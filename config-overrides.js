/* config-overrides.js */

// const { injectBabelPlugin } = require('react-app-rewired');
const { getBabelLoader } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
    // config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config);
    const babelLoader = getBabelLoader(config.module.rules);
    if (babelLoader) {
        babelLoader.options = {
            babelrc: true,
            cacheDirectory: true,
        };
    }

    //do stuff with the webpack config...
    config = rewireLess.withLoaderOptions({
        javascriptEnabled: true,
    })(config, env);

    return config;
};
