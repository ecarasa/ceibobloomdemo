var CONFIG = {
    FRONTEND_URL: 'http://syl.ceibo.digital',
    API_URL: 'http://syl.ceibo.digital:3333/api/v1/',
    NEOVIZ_URL: 'bolt://syl.ceibo.digital:7687',
    LOGO: `assets/logo-bloom.png`,
    LOGO_TRANSPARENT_PLACEHOLDER: `http://syl.ceibo.digital/assets/logo-larti-products.png`
};

window.CONFIG = CONFIG;