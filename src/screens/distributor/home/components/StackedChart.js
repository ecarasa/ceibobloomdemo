import React from 'react';
import _ from 'lodash';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import CHARTCONFIG from 'constants/chartConfig';

const COLOR = {
    success: 'rgba(139,195,74,.7)',
    info: 'rgba(1,188,212,.7)',
    text: '#3D4051',
    gray: '#EDF0F1',
};

const StackedChart = props => {
    const data = props.data;
    let bar2 = {};

    bar2.option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
            },
        },
        legend: {
            data: [
                'Potencial clientes',
                'Clientes Presupuesto',
                'Ventas clientes',
                'Clientes venta actual',
            ],
            textStyle: {
                color: CHARTCONFIG.color.text,
            },
        },
        calculable: true,
        xAxis: [
            {
                type: 'category',
                data: _.map(data.ranges, (i) => `${parseFloat(i.from).toFixed(1)} - ${parseFloat(i.to).toFixed(1)}`),
                axisLabel: {
                    textStyle: {
                        color: CHARTCONFIG.color.text,
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: CHARTCONFIG.color.splitLine,
                    },
                },
            },
        ],
        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    textStyle: {
                        color: CHARTCONFIG.color.text,
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: CHARTCONFIG.color.splitLine,
                    },
                },
                splitArea: {
                    show: true,
                    areaStyle: {
                        color: CHARTCONFIG.color.splitArea,
                    },
                },
            },
            {
                type: 'value',
                axisLabel: {
                    textStyle: {
                        color: CHARTCONFIG.color.text,
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: CHARTCONFIG.color.splitLine,
                    },
                },
                splitArea: {
                    show: true,
                    areaStyle: {
                        color: CHARTCONFIG.color.splitArea,
                    },
                },
            },
        ],
        series: [
            {
                name: 'Potencial clientes',
                type: 'line',
                stack: 'estimated_clients',
                data: _.map(data.estimated, (i) => i.clients),
                yAxisIndex: 1
            },
            {
                name: 'Ventas clientes',
                type: 'line',
                stack: 'real_clients',
                data: _.map(data.real, (i) => i.clients),
                yAxisIndex: 1
            },
            {
                name: 'Clientes Presupuesto',
                type: 'bar',
                stack: 'Estimado',
                data: _.map(data.estimated, (i) => i.sum),
            },
            {
                name: 'Clientes venta actual',
                type: 'bar',
                stack: 'Real',
                data: _.map(data.real, (i) => i.sum),
                /*markLine: {
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                type: 'dashed',
                            },
                        },
                    },
                    data: [[{ type: 'min' }, { type: 'max' }]],
                },*/
            },
            /*{
                name: 'Clientes real',
                type: 'bar',
                stack: 'real',
                data: _.map(data.real, (i) => i.clients),
            },*/
            /*{
                name: 'Affiliate',
                type: 'bar',
                stack: 'Ads',
                data: [220, 182, 191, 234, 290, 330, 310],
            },
            {
                name: 'Video Ads',
                type: 'bar',
                stack: 'Ads',
                data: [150, 232, 201, 154, 190, 330, 410],
            },
            {
                name: 'Search',
                type: 'bar',
                data: [862, 1018, 964, 1026, 1679, 1600, 1570],
                markLine: {
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                type: 'dashed',
                            },
                        },
                    },
                    data: [[{ type: 'min' }, { type: 'max' }]],
                },
            },
            {
                name: 'Baidu',
                type: 'bar',
                barWidth: 5,
                stack: 'Search',
                data: [620, 732, 701, 734, 1090, 1130, 1120],
            },
            {
                name: 'Google',
                type: 'bar',
                stack: 'Search',
                data: [120, 132, 101, 134, 290, 230, 220],
            },
            {
                name: 'Bing',
                type: 'bar',
                stack: 'Search',
                data: [60, 72, 71, 74, 190, 130, 110],
            },
            {
                name: 'Others',
                type: 'bar',
                stack: 'Search',
                data: [62, 82, 91, 84, 109, 110, 120],
            },*/
        ],
    };

    return <ReactEcharts option={bar2.option} theme={'macarons'} />;
};

export default StackedChart;
