import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { observable, action, computed, intercept, observe } from 'mobx'
import { observer } from 'mobx-react';
import { List, Avatar, Tag, Empty, Spin, Select, Carousel, Card, Checkbox, Radio } from 'antd';
import _ from 'lodash';
import { RightOutlined, LineChartOutlined, ShoppingCartOutlined, UsergroupAddOutlined, RocketOutlined } from '@ant-design/icons';
import * as d3 from 'd3';
import * as venn from 'venn.js'

// Neovis
import NeoVis from 'third-party/neovis/neovis'

// Services
import * as Services from 'services/'

// Components
import ModalClientDetails from 'screens/distributor/modals/modal_client_details'
import MainChart from './components/MainChart'
import Dashboard from './components/Dashboard'
import ProjectTable from './components/ProjectTable'
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard'

import DEMO from 'constants/demoData';
import moment from 'moment';
import withContext from 'context/withContext';

// Styles
import './index.scss';

const CheckboxGroup = Checkbox.Group;
const RadioGroup = Radio.Group;
const { Option } = Select;

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            filtros: [],
            acopios: [],
            relaciones: [],
            tiposGrafo: [],
            configQuery: {
                relacion: ['insumo', 'importeVenta']
            },
            configQueriesStack: [],
            clientId: null,
            clickedNodeId: null,
            selectedNodes: [],
            isEnabledMultipleSelection: false
        };

        // References
        this.visRef = React.createRef();
    }

    componentDidMount() {
        this.fetchGrafoConfig();

        // Get acopios
        Services.Acopio.getAcopios({}, (data) => {
            if(data.success) {
                this.setState({ acopios: data.acopios, isLoading: false });
            } else {
                this.setState({ isLoading: false });
            }
        }, (err) => {
            this.setState({ isLoading: false });
        });
    }

    fetchGrafoConfig = (addQueryToStack = true) => {
        let { configQuery, configQueriesStack } = this.state;
        Services.Analytics.trackAudit({ action : `Filtro de grafo`, screen: 'grafo', payload: configQuery });

        if(!configQueriesStack)
            configQueriesStack = [];
        if(addQueryToStack)
            configQueriesStack.push(configQuery);

        this.setState({
            isLoading : true,
            selectedNodes: [],
            configQueriesStack
        });
        Services.Neoviz.getConfig(configQuery, (data) => {
            var viz = null;
            var config = {
                container_id: "viz",
                server_url: window.CONFIG.NEOVIZ_URL,
                server_user: "neo4j",
                server_password: "ceibo"
            };
            let extendedConfig = data.neovizConfig;
            config = Object.assign(config, extendedConfig);

            // this.viz = new window.NeoVis.default(config);
            this.viz = new NeoVis(config);
            this.viz.render();

            this.viz._events.register('completed', (e) => {
                var options = {
                    nodes:{
                        color: '#ff0000',
                        fixed: false,
                        font: '19px Montserrat #222',
                        scaling: {
                            label: true
                        },
                        shadow: false,
                        opacity: 1
                    }
                };
                this.viz._network.setOptions(options);

                setTimeout(() => {
                    if(this.viz && this.viz._network)
                        this.viz._network.stopSimulation()

                    // this.viz._network.selectNodes([994])
                    this.setState({ isLoading : false });
                }, 1500);
            });

            this.viz._events.register('error', (e) => {
                this.setState({ isLoading : false });
            });

            const clickedEdge = (i) => {
                if(this.viz && this.viz._network)
                    this.viz._network.stopSimulation()
            }

            const clickedNode = (i) => {
                let { clickedNodeId, selectedNodes, isEnabledMultipleSelection } = this.state;

                if(i.nodeId != clickedNodeId) {
                    const connectedNodes = this.viz._network.getConnectedNodes(i.nodeId);
                    // const connectedEdges = this.viz._network.getConnectedEdges(i.nodeId);
                    const nodes = this.viz._nodes;

                    const groupNodes = _.filter(Object.keys(nodes), (i) => connectedNodes.indexOf(nodes[i].id) > -1 && nodes[i].group == 'Rubro');
                    // console.log("Group nodes", groupNodes);
                    this.viz._network.selectNodes(groupNodes);
                    // this.viz._network.selectEdges(connectedEdges);

                    this.setState({ clickedNodeId : i.nodeId });

                    //  Track new click on node
                    const isF12 = (i.node.group == 'vABSecundario' || i.node.group == 1 || i.node.group == 0) && i.node.title.indexOf('id:') > -1;
                    console.log(i);
                    if(isF12 && isEnabledMultipleSelection){
                        if(_.some(selectedNodes, (m) => m.label == i.node.label)) {
                            _.remove(selectedNodes, (m) => m.label == i.node.label)
                        } else {
                            Services.Client.isClientExists(i.node.label, (data) => {
                                selectedNodes.push({
                                    exists: data.exists,
                                    label: i.node.label
                                });

                                this.setState({ selectedNodes });
                            });
                        }
                    }

                    return;
                }

                if(i.node.group == 'Rubro') // Si es rubro omitir
                    return;

                if(i.node.group == 'vABSecundario' && i.node.title.indexOf('id:') == -1) {
                    let multipleAbs = i.node.title.match(/<li>(.*?)<\/li>/g);
                    if(multipleAbs != null && multipleAbs.length > 0) {
                        for(var idx in multipleAbs) {
                            multipleAbs[idx] = multipleAbs[idx].replace('<li>', '').replace('</li>', '');
                        }
                    }

                    if(multipleAbs == null || multipleAbs.length == 0) {
                        const lastIndexOf = i.node.title.lastIndexOf('</strong>');
                        if(lastIndexOf > -1) {
                            let subString = i.node.title.substring(lastIndexOf, i.node.title.length);
                            if(subString) {
                                subString = subString.replace('</strong>', '').replace(' ', '').replace('<br>', '');
                                multipleAbs = [subString];
                            }
                        }
                    }

                    const { configQuery } = this.state;
                    const filteringByMultipleAbs = multipleAbs != null && multipleAbs.length > 0;

                    if(filteringByMultipleAbs) {
                        configQuery.relacion = multipleAbs;
                        configQuery.tipoGrafo = 'zoomRubros';
                        this.setState({ configQuery }, this.fetchGrafoConfig)
                    } else {
                        Services.Analytics.trackAudit({ action : `Ver F12`, screen: 'grafo', payload: { cliente: i.node.label } });
                        if(i && i.node && i.node.label && configQuery.tipoGrafo != 'zoomRubros') {
                            this.setState({
                                clientId: i.node.label
                            });
                        }
                    }
                } else {
                    if(i.node.title.indexOf('id:') > -1 && i.node.title.indexOf('rubro_desc') == -1) {
                        Services.Analytics.trackAudit({ action : `Ver F12`, screen: 'grafo', payload: { cliente: i.node.label } });
                        if(i && i.node && i.node.label) {
                            this.setState({
                                clientId: i.node.label
                            });
                        }
                    }
                }

                /*if(i.node.title.indexOf('id:') > -1 && i.node.title.indexOf('rubro_desc') == -1) {
                    Services.Analytics.trackAudit({ action : `Ver F12`, screen: 'grafo', payload: { cliente: i.node.label } });
                    if(i && i.node && i.node.label) {
                        this.setState({ clientId: i.node.label });
                    }
                } else {
                    const multipleAbs = i.node.title.match(/<li>(.*?)<\/li>/g);
                    if(multipleAbs != null && multipleAbs.length > 0) {
                        for(var idx in multipleAbs) {
                            multipleAbs[idx] = multipleAbs[idx].replace('<li>', '').replace('</li>', '');
                        }
                    }

                    const { configQuery } = this.state;
                    const filteringByMultipleAbs = multipleAbs != null && multipleAbs.length > 0;
                    if(filteringByMultipleAbs) {
                        console.log(`Filtering by multiple ABS`, multipleAbs);
                        configQuery.relacion = multipleAbs;
                        configQuery.tipoGrafo = 'zoomRubros';
                        this.setState({ configQuery }, this.fetchGrafoConfig)
                    } else {
                        Services.Analytics.trackAudit({ action : `Ver F12`, screen: 'grafo', payload: { cliente: i.node.label } });
                        if(i && i.node && i.node.label && configQuery.tipoGrafo != 'zoomRubros') {
                            this.setState({ clientId: i.node.label });
                        }
                    }
                }*/

                this.viz._network.stopSimulation();
            }

            this.viz._events.register('clickNode', clickedNode);
            this.viz._events.register('clickEdge', clickedEdge);

            if(data.filtros) this.setState({ filtros : data.filtros });
            if(data.relaciones) this.setState({ relaciones : data.relaciones });
            if(data.tiposGrafo) this.setState({ tiposGrafo : data.tiposGrafo });
            if(data.config) this.setState({ configQuery : data.config });
        }, (err) => {
            // Do nothing
            this.setState({ isLoading : false });
        });
    }

    componentWillReceiveProps(nextProps, nextState) {

    }

    render() {

        const {
            configQuery,
            configQueriesStack,
            acopios
        } = this.state;

        // Make filters and relationships
        const filters = [];
        _.map(this.state.filtros, (e, index) => {

            // Convert the label to acopio's name
            if(index == 'planta') {
                var labels = [];
                for(var idx in e){
                    const acopio = _.find(acopios, (i) => i.neo4j_key == e[idx]);
                    if(acopio != null) {
                        labels.push(acopio.name);
                    }
                }

                filters.push({
                    label: index,
                    values: e,
                    labels
                });
            } else {
                filters.push({
                    label: index,
                    values: e
                });
            }
        });

        const relations = [];
        _.map(this.state.relaciones, (e, index) => {
            relations.push({
                label: _.capitalize(e),
                value: e
            })
        });

        const tiposGrafo = [];
        _.map(this.state.tiposGrafo, (e, index) => {
            tiposGrafo.push({
                label: _.capitalize(e),
                value: e
            })
        });

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">

                        <ModalClientDetails
                            visible={this.state.clientId}
                            clientId={this.state.clientId}
                            acopioId={this.state.configQuery && this.state.configQuery.planta ? this.state.configQuery.planta : null}
                            onClose={() => {
                                this.setState({ clientId : null });
                            }}
                        />

                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                                        <div className="box box-transparent" style={{ minWidth: '15%' }}>
                                            <div className="box-body">
                                                <Card title="Filtros" style={{ width: '100%' }}>
                                                    { filters && filters.map((e) => {

                                                        const checkboxGroup = [];
                                                        return (
                                                            <div>
                                                                <h5 style={{ fontWeight: 700, marginTop: 15 }}>{_.capitalize(e.label)}</h5>
                                                                <RadioGroup
                                                                    options={_.map(e.values, (i, index) => {
                                                                        return {
                                                                            label: e.labels && e.labels[index] != null ? e.labels[index] : i,
                                                                            value: i
                                                                        }
                                                                    })}
                                                                    style={{ display: 'flex', flexDirection: 'column' }}
                                                                    checked={configQuery[e.label]}
                                                                    value={configQuery[e.label]}
                                                                    onChange={(evt) => {
                                                                        configQuery[e.label] = evt.target.value;
                                                                        this.setState({ configQuery }, this.fetchGrafoConfig);
                                                                    }}
                                                                />
                                                            </div>
                                                        );
                                                    })}
                                                </Card>

                                                { false && 
                                                    <Card title="Configuración" style={{ width: '100%', marginTop: 10 }}>
                                                        <div>
                                                            <Checkbox
                                                                onChange={() => {
                                                                    this.setState({ isEnabledMultipleSelection: !this.state.isEnabledMultipleSelection });
                                                                }}>
                                                                { this.state.isEnabledMultipleSelection ? 'Selección múltiple habilitada' : 'Selección múltiple deshabilitada' }
                                                            </Checkbox>
                                                        </div>

                                                        { this.state.selectedNodes && this.state.selectedNodes.length > 0 != null &&
                                                            <QueueAnim type="bottom" className="ui-animate">
                                                                <div style={{ display: 'flex', flexDirection: 'column', maxWidth: '80%', alignItems: 'flex-start' }}>
                                                                    { this.state.selectedNodes.map((e, index) => {
                                                                        return (
                                                                            <Tag
                                                                                style={{ marginTop: 10 }}
                                                                                color={e.exists ? 'success' : 'warning'}
                                                                                closable
                                                                                onClose={i => {
                                                                                    i.preventDefault();
                                                                                    let { selectedNodes } = this.state;
                                                                                    _.remove(selectedNodes, (m) => m.label == e.label);
                                                                                    this.setState({ selectedNodes });
                                                                                }}>
                                                                                { e.label }
                                                                            </Tag>
                                                                        );
                                                                    })}
                                                                </div>
                                                            </QueueAnim>
                                                        }
                                                    </Card>
                                                }
                                            </div>
                                        </div>

                                        <div
                                            id={"viz"}
                                            ref={this.visRef}
                                            style={{
                                                display: 'flex',
                                                flex: 1,
                                                height: window.innerHeight / 1.3,
                                                backgroundColor: `transparent`
                                            }}
                                        />

                                        { configQueriesStack != null && configQueriesStack.length > 1 &&
                                            <a
                                                onClick={() => {
                                                    if(configQueriesStack.length > 1) {
                                                        // Remove the last one (which is the currently one)
                                                        configQueriesStack.pop();

                                                        // Set the last - 1 because it's the previous query
                                                        let lastQuery = configQueriesStack[configQueriesStack.length - 1];

                                                        this.setState({ configQuery : lastQuery, configQueriesStack }, () => {
                                                            this.fetchGrafoConfig(false);
                                                        });
                                                    }
                                                }}
                                                style={{ position: 'absolute', top: 10, left: '20%', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', cursor: 'pointer' }}>
                                                <img
                                                    style={{ width: 20, marginBottom: 2 }}
                                                    src="assets/images-upl/icons/back.png"
                                                />
                                                Volver atrás
                                            </a>
                                        }

                                        { this.state.configQuery.tipoGrafo != 'zoomRubros' &&
                                            <div style={{ flexDirection: 'column' }}>
                                                <div className="box box-transparent mb-4" style={{ minWidth: '15%' }}>
                                                    <div className="box-body">
                                                        <Card title="Tipos de grafo" style={{ width: '100%' }}>
                                                            <div>
                                                                <RadioGroup
                                                                    options={tiposGrafo}
                                                                    style={{ display: 'flex', flexDirection: 'column' }}
                                                                    checked={configQuery.tipoGrafo}
                                                                    value={configQuery.tipoGrafo}
                                                                    onChange={(evt) => {
                                                                        configQuery.tipoGrafo = evt.target.value;
                                                                        this.setState({ configQuery }, this.fetchGrafoConfig);
                                                                    }}
                                                                />
                                                            </div>
                                                        </Card>
                                                    </div>
                                                </div>

                                                { this.state.configQuery.tipoGrafo != 'agrupadoPorRubro' &&
                                                    <div className="box box-transparent" style={{ minWidth: '15%' }}>
                                                        <div className="box-body">
                                                            <Card title="Relaciones" style={{ width: '100%' }}>
                                                                { relations && relations.map((e) => {
                                                                    const checkboxGroup = [];
                                                                    return (
                                                                        <div>
                                                                            <CheckboxGroup
                                                                                options={[e]}
                                                                                style={{ display: 'flex', flexDirection: 'column' }}
                                                                                value={configQuery.relacion}
                                                                                onChange={(m) => {
                                                                                    if(e) {
                                                                                        if(configQuery.relacion.indexOf(e.value) > -1) {

                                                                                            // If it's just one relation to remove
                                                                                            if(configQuery.relacion.length == 1)
                                                                                                return;

                                                                                            configQuery.relacion.splice(configQuery.relacion.indexOf(e.value), 1);
                                                                                        } else {
                                                                                            configQuery.relacion.push(e.value);
                                                                                        }

                                                                                        this.setState({ configQuery }, this.fetchGrafoConfig);
                                                                                    }
                                                                                }}
                                                                            />
                                                                        </div>
                                                                    );
                                                                })}
                                                            </Card>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        }
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);