import React from 'react';
import queryString from 'query-string';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { List, Button, Avatar, Tag, Empty, Spin, Select, Input, message } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import DEMO from 'constants/demoData';
import QueueAnim from 'rc-queue-anim';

import { observer } from 'mobx-react';
import { CheckOutlined } from '@ant-design/icons';
import * as Services from 'services'
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            notifications: [],
            isLoading: true,
            users: [],

            notification: {
                notificationText: null,
                users: [],
                to: null
            }
        };
    }

    componentDidMount() {
        this.getNotifications()

        const params = queryString.parse(this.props.location.search);
        if(params.userId != null) {
            const { notification } = this.state;
            notification.to = params.userId;
            this.setState({ notification }, this.loadUsers);
        }

        /*if(params.userIds != null) {
            const { notification } = this.state;
            notification.to = params.userIds.join(',');
            this.setState({ notification }, this.loadUsers);
        }*/
    }

    getNotifications = () => {
        Services.Notification.getNotificationsCreatedByMe((data) => {
            if(data.success) {
                this.setState({ notifications: data.notifications, isLoading: false });
            }
        }, (err) => {
            this.setState({ isLoading : false });
        });
        /*Services.Notification.getMyNotifications((data) => {
            if(data.success) {
                this.setState({ notifications: data.notifications, isLoading: false });
            }
        }, (err) => {
            this.setState({ isLoading: false });
        });*/
    }

    onSendNotification = () => {
        const { notification } = this.state;
        if(!notification.text || !notification.text.length) {
            message.error('La notificación debe tener un texto');
            return
        } else if(!notification.to || !notification.to.length) {
            message.error('Selecciona un productor para enviar esta notificación');
            return
        }

        this.setState({ isLoading : true });

        Services.Notification.create({
            text: notification.text,
            to: notification.to,
        }, (data) => {
            if(data.success) {
                message.success('La notificación ya se envió al productor seleccionado.');
                this.setState({ users : [] });
                this.getNotifications()
                Services.Analytics.trackAudit({ action : `Notificación creada`, screen: 'alerts', payload: {} });
            } else {
                message.error(data.message);
            }

            this.setState({ isLoading : false });
        }, (err) => {
            message.error(Services.Constants.Errors.General);
            this.setState({ isLoading : false });
        })
    }

    loadUsers = () => {
        this.setState({ isLoading: true });
        Services.Analytics.trackAudit({ action : `Click en crear notificación`, screen: 'alerts', payload: {} });
        Services.User.getUsersByRoleSlug('all', (data) => {
            if(data.success) {
                let users = data.users.map((e) => {
                    return {
                        value: e.id,
                        label: e.profile.name,
                        email: e.email
                    }
                });

                this.setState({ users });
            }

            this.setState({ isLoading: false });
        }, (err) => {
            // Do nothing
        });
    }

    render() {
        const { notifications, notification } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Alertas dirigidas</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Button
                                        type={'primary'}
                                        icon={<SearchOutlined />}
                                        onClick={this.loadUsers}
                                        style={{ marginBottom: 10 }}>Crear nueva alerta</Button>

                                        <div style={{ display: 'flex', flexDirection: 'column', marginLeft: 0, marginBottom: 10, marginTop: 5 }}>
                                            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                                <div style={{ backgroundColor: '#01BCD4', width: 25, height: 10, borderRadius: 5 }}></div>
                                                <span style={{ display: 'block', marginLeft: 10 }}>Alerta leída</span>
                                            </div>

                                            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                                <div style={{ backgroundColor: '#66BB6A', width: 25, height: 10, borderRadius: 5 }}></div>
                                                <span style={{ display: 'block', marginLeft: 10 }}>Alerta no leída</span>
                                            </div>
                                        </div>

                                    { this.state.users && this.state.users.length > 0 &&
                                        <QueueAnim type="bottom" className="ui-animate">
                                            <div key="2" className="container-fluid chapter" style={{ width: '100%', padding: 0 }}>
                                                <article className="article">
                                                    <div className="box box-default" style={{ marginTop: 10 }}>
                                                        <div className="box-header">Generar una nueva alerta</div>
                                                        <div className="box-body">
                                                            <Input
                                                                name={'text'}
                                                                value={notification.text}
                                                                style={{ marginTop: 10 }}
                                                                placeholder={'Ingresa el texto de la notificación'}
                                                                onChange={(e) => {
                                                                    notification.text = e.target.value;
                                                                    this.setState({ notification });
                                                                }}
                                                            />

                                                            <Select
                                                                showSearch
                                                                placeholder="Escoge un usuario de esta lista"
                                                                optionFilterProp="children"
                                                                defaultValue={notification.to != null ? parseInt(notification.to) : null}
                                                                onChange={(e) => {
                                                                    notification.to = [e];
                                                                    this.setState({ notification })
                                                                }}
                                                                filterOption={(input, option) =>
                                                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                                }
                                                                style={{ width: '100%', marginTop: 10 }}>
                                                                { this.state.users && this.state.users.map((e) => {
                                                                    return (
                                                                        <Option value={e.value}>{ e.label } - {e.email}</Option>
                                                                    );
                                                                })}
                                                            </Select>

                                                            <Button ghost onClick={this.onSendNotification} style={{ marginTop: 10 }} type="primary">Enviar notificación</Button>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        </QueueAnim>
                                    }
                                    <ListAlerts
                                        notifications={notifications}
                                    />
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

const ListAlerts = (props) => {
    if(!props.notifications || !props.notifications.length) {
        return (
            <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={'Aún no generaste alertas dirigidas'}
            />
        );
    }

    return (
        <div className="box box-default">
            <div className="box-body">

                <List
                    itemLayout="horizontal"
                    dataSource={props.notifications}
                    renderItem={(item, index) => (
                        <List.Item>
                            <div className="list-style-v1">
                                <div className="list-item">
                                    { <div
                                        className={`icon-btn icon-btn-round mr-3 ${item.seen ? 'bg-info' : 'bg-success'} text-body-reverse`}>
                                        <LegacyIcon type={'mail'} style={{ color: 'white' }} />
                                    </div> }
                                    <div className="list-item__body">
                                        <div className="list-item__title">Alerta dirigida</div>
                                        <div className="list-item__desc" dangerouslySetInnerHTML={{ __html: item.text_formatted }}></div>
                                        { item.to != null && item.to.email &&
                                            <div className="list-item__datetime">Enviada a { item.to.email }</div>
                                        }
                                        <div className="list-item__datetime">
                                            {moment(item.created_at).fromNow()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </List.Item>
                    )}
                />
            </div>
        </div>
    );
};

export default withContext(Section);