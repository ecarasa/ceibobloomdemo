import React from 'react';
import QueueAnim from 'rc-queue-anim';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, message } from 'antd';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            profile: null,
        };

        this.form = null;
    }

    componentDidMount() {
        Services.User.getMe((data) => {
            if(data.success) {
                this.form.setFieldsValue(data.user.profile);
                this.setState({ profile: data.user });
            }
        });
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });

        let data = this.form.getFieldsValue();
        Services.User.updateMe(data, (response) => {
            this.setState({ isSaving : false });
            this.props.stores.user.refresh();
            message.success('Tu perfil fue modificado con éxito');
        }, (err) => {
            message.error('Hubo un problema al modificar tu perfil. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        })
    }

    render() {
        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Tu perfil</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Form
                                        layout={'vertical'}
                                        onFinish={this.onSubmit}
                                        ref={(e) => { this.form = e; }}>
                                        <Form.Item
                                            name={['name']}
                                            rules={[{ required: true }]}
                                            label="Tu nombre">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name={['whatsapp_number']}
                                            rules={[{ required: true }]}
                                            label="Tu número de whatsapp">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item>
                                            <Button
                                                loading={this.state.isSaving}
                                                type="primary"
                                                className="btn-cta"
                                                htmlType="submit">
                                                Guardar
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
