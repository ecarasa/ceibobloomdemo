import React from 'react';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import QueueAnim from 'rc-queue-anim';
import Highlighter from 'react-highlight-words';
import { observable, action, computed, intercept, observe } from 'mobx';
import { observer } from 'mobx-react';
import {
    List,
    Avatar,
    Tag,
    Empty,
    Spin,
    Select,
    Carousel,
    Table,
    Progress,
    Popover,
    Button,
    Divider,
    Input,
    Space
} from 'antd';
import _ from 'lodash';
import {
    RightOutlined,
    LineChartOutlined,
    ShoppingCartOutlined,
    UsergroupAddOutlined,
    UserSwitchOutlined,
    ClockCircleOutlined,
    RocketOutlined,
    PhoneOutlined,
    IdcardOutlined,
    AlertOutlined,
    SearchOutlined,
    DollarCircleOutlined,
    PhoneFilled,
    PhoneTwoTone
} from '@ant-design/icons';
import * as d3 from 'd3';
import * as venn from 'venn.js';

// Services
import * as Services from 'services/';

// Components
import ModalClientDetails from 'screens/distributor/modals/modal_client_details';
import HistoricalRealChart from './components/HistoricalRealChart';
import MainChart from './components/MainChart';
import Dashboard from './components/Dashboard';
import ProjectTable from './components/ProjectTable';
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';
import DEMO from 'constants/demoData';
import moment from 'moment';
import withContext from 'context/withContext';

// Styles
import './index.scss';

// Utils
import { formatPrice } from 'utils/numbers';

const { Option } = Select;

// Function to insert element at specific index
const insert = (arr, index, newItem) => [
    ...arr.slice(0, index),
    newItem,
    ...arr.slice(index),
];

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isLoadingIndicators: true,
            filters: {
                tipo: 'insumos',
                tipo_cliente: 'leales',
            },
            searchText: null,
            indicators: {},
            acopios: [],
            currentAcopio: null,
            clients: [],
            filteredClients: [],
            showPopoverActions: false,
            showDetailsClient: false,
            clientId: null,
            rowIndexId: null,
            columns: [],
        };
    }

    getColumnSearchProps = (label, dataIndex) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Buscar por ${label}`}
                    value={selectedKeys[0]}
                    onChange={e =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        this.handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() =>
                            this.handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}>
                        Buscar
                    </Button>
                    <Button
                        onClick={() => this.handleReset(clearFilters)}
                        size="small"
                        style={{ width: 90 }}>
                        Resetear
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => (
            <SearchOutlined
                style={{ color: filtered ? '#8e519a' : undefined }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                !text || text.length == 0 ? '-' : text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({ searchText: '' });
    };

    getColumns = () => {
        const { filters } = this.state;

        // Default columns
        let columns = [
            {
                title: 'Cod. Cliente',
                dataIndex: 'cliente_principal',
                key: 'cliente_principal',
                width: '15%',
                sorter: {
                    compare: (a, b) =>
                        parseInt(a.cliente_principal) -
                        parseInt(b.cliente_principal),
                    multiple: 3,
                },
                render: (text, record) => {
                    console.log("el texto es", text, text.length);
                    if (!text || text == '' || text.length == 0)
                        return '-';
                    return text;
                },
                ...this.getColumnSearchProps('AB Primario', 'cliente_principal'),
            },
            {
                title: 'Cliente',
                dataIndex: 'cliente',
                key: 'cliente',
                width: '15%',
                sorter: {
                    compare: (a, b) =>
                        parseInt(a.cliente) - parseInt(b.cliente),
                    multiple: 3,
                },
                ...this.getColumnSearchProps('AB Secundario', 'cliente'),
            },
            {
                title: 'Avance',
                dataIndex: 'avance',
                key: 'avance',
                sorter: {
                    compare: (a, b) =>
                        parseFloat(a.avance) - parseFloat(b.avance),
                    multiple: 3,
                },
                render: (text, record, index) => {
                    const isExceded =
                        (parseFloat(record.avance) * 100).toFixed(1) > 100;
                    return (
                        <Progress
                            percent={(parseFloat(record.avance) * 100).toFixed(
                                1
                            )}
                            strokeColor={
                                !isExceded
                                    ? {
                                          '0%': '#108ee9',
                                          '100%': '#87d068',
                                      }
                                    : {
                                          '0%': '#108ee9',
                                          '100%': '#FF4B2B',
                                      }
                            }
                            format={(percent, successPercent) => {
                                return `${(
                                    parseFloat(record.avance) * 100
                                ).toFixed(1)}%`;
                            }}
                            status="active"
                            showInfo={true}
                        />
                    );
                },
            },
        ];

        // Specific columns for each
        if (filters.tipo_cliente == 'leales') {
            columns = insert(columns, columns.length, {
                title: 'Total estimado',
                dataIndex: 'total_estimado',
                key: 'total_estimado',
                width: '15%',
                render: (text, record) => {
                    if(filters.tipo == 'productos')
                        return `${parseFloat(text).toFixed(2)} ton.`

                    return `${formatPrice(text)}`
                },
                sorter: {
                    compare: (a, b) =>
                        parseFloat(a.total_estimado) -
                        parseFloat(b.total_estimado),
                    multiple: 3,
                },
            });
        } else if (filters.tipo_cliente == 'nuevos') {
            columns.splice(
                _.findIndex(columns, i => i.dataIndex == 'avance'),
                1
            );
            if (filters.tipo == 'productos') {
                columns = insert(columns, columns.length, {
                    title: 'Total producción',
                    dataIndex: 'importe_total',
                    key: 'importe_total',
                    render: (text, record) => `${parseFloat(text).toFixed(1)} ton.`,
                    sorter: {
                        compare: (a, b) =>
                            parseFloat(a.importe_total) -
                            parseFloat(b.importe_total),
                        multiple: 3,
                    },
                });
            } else {
                columns = insert(columns, columns.length, {
                    title: 'Importe total',
                    dataIndex: 'importe_total',
                    key: 'importe_total',
                    render: (text, record) => `${formatPrice(text)}`,
                    sorter: {
                        compare: (a, b) =>
                            parseFloat(a.importe_total) -
                            parseFloat(b.importe_total),
                        multiple: 3,
                    },
                });
            }
        } else if (filters.tipo_cliente == 'pendientes') {
            columns.splice(
                _.findIndex(columns, i => i.dataIndex == 'avance'),
                1
            );
            columns = insert(columns, columns.length, {
                title: 'Total estimado',
                dataIndex: 'total_estimado',
                key: 'total_estimado',
                render: (text, record) => {
                    if(filters.tipo == 'productos')
                        return `${parseFloat(text).toFixed(2)} ton.`

                    return `${formatPrice(text)}`
                },
                sorter: {
                    compare: (a, b) =>
                        parseFloat(a.total_estimado) -
                        parseFloat(b.total_estimado),
                    multiple: 3,
                },
            });
            columns = insert(columns, columns.length, {
                title: 'Estado',
                dataIndex: 'estado',
                key: 'estado',
                sorter: {
                    compare: (a, b) =>
                        parseFloat(a.estado) - parseFloat(b.estado),
                    multiple: 3,
                },
                render: (text, record) => {
                    let phones = [];
                    let color = record.estado == 1 ? '#66BB6A' : (record.estado == 2 ? '#ffc000' : '#ff4200');
                    for (var i = 0; i < record.estado; i++) {
                        phones.push(
                            <div
                                style={{
                                    marginRight: 5,
                                    display: 'inline-block',
                                }}>
                                <PhoneFilled style={{ color }} />
                            </div>
                        );
                    }

                    return phones;
                },
            });
        }

        // Set actions
        columns = insert(columns, columns.length, {
            title: 'Acciones',
            width: '15%',
            render: (text, record, index) => {
                return (
                    <Popover
                        content={this.renderActions.bind(this, record)}
                        title={'Escoge una acción'}
                        trigger="click"
                        visible={
                            this.state.rowIndexId == index &&
                            this.state.clientId == record.cliente &&
                            this.state.showPopoverActions == true
                        }
                        onVisibleChange={e => {
                            // Do nothing
                            if (e == false) {
                                this.setState({
                                    rowIndexId: null,
                                    clientId: null,
                                    showPopoverActions: false,
                                });
                            }
                        }}>
                        <Button
                            onClick={e => {
                                e.preventDefault();
                                this.setState({
                                    rowIndexId: index,
                                    clientId: record.cliente,
                                    showPopoverActions: true,
                                    showDetailsClient: false,
                                });
                            }}
                            type="primary">
                            Acciones
                        </Button>
                    </Popover>
                );
                /*return (
                    <a
                        onClick={(e) => {
                            e.preventDefault();
                            this.setState({ clientId : record.cliente, showDetailsClient: true })
                        }}
                        href="#">Ver más detalles</a>
                );*/
            },
        });

        return columns;
    };

    renderActions = record => {
        let buttons = [];

        // Details
        buttons.push(
            <a
                onClick={() => {
                    Services.Analytics.trackAudit({ action : `Presupuesto`, screen: 'clients', payload: { cliente: record.cliente } });

                    this.setState({
                        showDetailsClient: true,
                        showPopoverActions: false,
                    });
                }}
                style={{ fontWeight: 400, fontSize: 14 }}>
                <IdcardOutlined style={{ marginRight: 0 }} /> &nbsp; Presupuesto
            </a>
        );
        buttons.push(<Divider style={{ marginTop: 5, marginBottom: 5 }} />);

        if (record.user && record.user.id) {
            buttons.push(
                <a
                    onClick={() => {
                        Services.Analytics.trackAudit({ action : `Crear notificacion`, screen: 'clients', payload: { cliente : record.cliente } });

                        this.props.history.push({
                            pathname: '/dashboard/alerts/managed',
                            search: `?userId=${record.user.id}`,
                        });
                    }}
                    style={{ fontWeight: 400, fontSize: 14 }}>
                    <AlertOutlined style={{ marginRight: 0 }} /> &nbsp; Crear
                    notificación
                </a>
            );
            buttons.push(<Divider style={{ marginTop: 5, marginBottom: 5 }} />);

            buttons.push(
                <a
                    onClick={() => {
                        Services.Analytics.trackAudit({ action : `Crear oferta`, screen: 'clients', payload: { cliente: record.cliente } });

                        this.props.history.push({
                            pathname: '/dashboard/offers/new',
                            search: `?userId=${record.user.id}`,
                        });
                    }}
                    style={{ fontWeight: 400, fontSize: 14 }}>
                    <DollarCircleOutlined style={{ marginRight: 0 }} /> &nbsp; Crear
                    oferta
                </a>
            );
            buttons.push(<Divider style={{ marginTop: 5, marginBottom: 5 }} />);

            if (record.user.profile && record.user.profile.whatsapp_number) {
                buttons.push(
                    <a
                        href={`tel://${record.user.profile.whatsapp_number}`}
                        style={{ fontWeight: 400, fontSize: 14 }}>
                        <PhoneOutlined style={{ marginRight: 0 }} /> &nbsp;
                        Llamar
                    </a>
                );
                buttons.push(
                    <Divider style={{ marginTop: 5, marginBottom: 5 }} />
                );
            }

            if (
                record.user_principal &&
                record.user_principal.profile &&
                record.user_principal.profile.whatsapp_number
            ) {
                // Check if those users are different
                if (record.user_principal.id != record.user.id) {
                    buttons.push(
                        <a
                            href={`tel://${record.user_principal.profile.whatsapp_number}`}
                            style={{ fontWeight: 400, fontSize: 14 }}>
                            <PhoneOutlined style={{ marginRight: 0 }} /> &nbsp;
                            Llamar cliente primario
                        </a>
                    );
                    buttons.push(
                        <Divider style={{ marginTop: 5, marginBottom: 5 }} />
                    );
                }
            }
        }

        return <div>{buttons}</div>;
    };

    componentDidMount() {
        this.setState({ columns: this.getColumns() });

        let user = this.props.stores.user.getUser();
        if (user != null && user.acopios && user.acopios.length > 0) {
            const filters = this.state.filters;

            // Check URL params
            const params = queryString.parse(this.props.location.search);
            if (params.tipo) {
                filters.tipo = params.tipo;

                // Get top indicators
                this.setState({ isLoadingIndicators: true });
                Services.Client.getIndicators(
                    params.tipo,
                    data => {
                        if (data.success) {
                            this.setState({
                                indicators: data.indicators,
                                isLoadingIndicators: false,
                            });
                        }
                    },
                    err => {
                        // Do nothing
                    }
                );
            }

            this.setState(
                {
                    acopios: user.acopios,
                    filters: filters,
                    isLoading: false,
                },
                this.fetchClients
            );
        }
    }

    fetchClients = () => {
        const { filters } = this.state;

        this.setState({
            clients: [],
            filteredClients: [],
            isLoading: true,
            columns: this.getColumns(),
        });
        Services.Client.getClientsByFilter(
            filters,
            data => {
                if (data.success) {
                    this.setState({
                        clients: data.clients,
                        filteredClients: data.clients,
                        currentAcopio: data.acopio,
                    });
                }
                this.setState({ isLoading: false });
            },
            err => {
                this.setState({ isLoading: false });
            }
        );
    };

    render() {
        const {
            currentAcopio,
            filters,
            acopios,
            clients,
            filteredClients,
        } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <ModalClientDetails
                            visible={this.state.showDetailsClient}
                            clientId={this.state.clientId}
                            onClose={() => {
                                this.setState({
                                    rowIndexId: null,
                                    clientId: null,
                                    showDetailsClient: false,
                                    showPopoverActions: false,
                                });
                            }}
                        />

                        <h2 className="article-title">
                            {currentAcopio != null
                                ? `${currentAcopio.name} - `
                                : null}{' '}
                            {filters.tipo == 'productos'
                                ? `Clientes por granos`
                                : `Clientes por insumos`}
                        </h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Spin
                                        spinning={
                                            this.state.isLoadingIndicators
                                        }>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="row">
                                                    {!this.state
                                                        .isLoadingIndicators &&
                                                        this.state.indicators &&
                                                        Object.keys(
                                                            this.state
                                                                .indicators
                                                        ).length > 0 &&
                                                        Object.keys(
                                                            this.state
                                                                .indicators
                                                        ).map(e => {
                                                            return (
                                                                <div className="col-xl-3 mb-4">
                                                                    <div className="number-card-v1">
                                                                        <div className="card-top">
                                                                            <span>
                                                                                {
                                                                                    this
                                                                                        .state
                                                                                        .indicators[
                                                                                        e
                                                                                    ]
                                                                                        .length
                                                                                }
                                                                            </span>
                                                                        </div>
                                                                        <div className="card-info">
                                                                            <span>
                                                                                Clientes{' '}
                                                                                {
                                                                                    this
                                                                                        .state
                                                                                        .indicators[
                                                                                        e
                                                                                    ]
                                                                                        .label
                                                                                }
                                                                            </span>
                                                                        </div>
                                                                        <div className="card-bottom">
                                                                            {this
                                                                                .state
                                                                                .indicators[
                                                                                e
                                                                            ]
                                                                                .label ==
                                                                                'leales' && (
                                                                                <UserSwitchOutlined className="text-success" />
                                                                            )}

                                                                            {this
                                                                                .state
                                                                                .indicators[
                                                                                e
                                                                            ]
                                                                                .label ==
                                                                                'nuevos' && (
                                                                                <UsergroupAddOutlined className="text-success" />
                                                                            )}

                                                                            {this
                                                                                .state
                                                                                .indicators[
                                                                                e
                                                                            ]
                                                                                .label ==
                                                                                'pendientes' && (
                                                                                <ClockCircleOutlined className="text-success" />
                                                                            )}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            );
                                                        })}
                                                </div>
                                            </div>
                                        </div>
                                    </Spin>

                                    <div className="row">
                                        <div className="col-12">
                                            <h5 style={{ fontWeight: 600 }}>
                                                Filtrar por tipo de cliente
                                            </h5>
                                        </div>
                                        <div className="col-12">
                                            <div
                                                style={{
                                                    width: 'auto',
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    marginBottom: 10,
                                                }}>
                                                <Select
                                                    value={filters.tipo_cliente}
                                                    onChange={value => {
                                                        filters.tipo_cliente = value;
                                                        Services.Analytics.trackAudit({ action : `Filtrar por clientes`, screen: 'clients', payload: { tipo: filters.tipo_cliente } });
                                                        this.setState(
                                                            { filters },
                                                            this.fetchClients
                                                        );
                                                    }}
                                                    placeholder={
                                                        'Selecciona un tipo de cliente'
                                                    }
                                                    defaultValue={
                                                        filters.tipo_cliente
                                                    }
                                                    style={{
                                                        marginRight: 10,
                                                        width: 250,
                                                        marginBottom: 10,
                                                    }}>
                                                    <Option value={'leales'}>
                                                        Clientes leales
                                                    </Option>
                                                    <Option value={'nuevos'}>
                                                        Clientes nuevos
                                                    </Option>
                                                    <Option
                                                        value={'pendientes'}>
                                                        Clientes pendientes
                                                    </Option>
                                                </Select>
                                            </div>
                                        </div>
                                    </div>

                                    {false && acopios && acopios.length > 0 && (
                                        <div className="row">
                                            <div className="col-12">
                                                <h5 style={{ fontWeight: 600 }}>
                                                    Filtrar por acopio
                                                </h5>
                                            </div>
                                            <div className="col-12">
                                                <div
                                                    style={{
                                                        width: 'auto',
                                                        display: 'flex',
                                                        flexDirection: 'row',
                                                        marginBottom: 10,
                                                    }}>
                                                    <Select
                                                        value={
                                                            this.state.acopio_id
                                                        }
                                                        onChange={value => {
                                                            this.setState(
                                                                {
                                                                    acopio_id: value,
                                                                },
                                                                this
                                                                    .getInfoForCurrentAcopio
                                                            );
                                                        }}
                                                        placeholder={
                                                            'Selecciona un acopio'
                                                        }
                                                        defaultValue={
                                                            this.state.acopio_id
                                                        }
                                                        style={{
                                                            marginRight: 10,
                                                            width: 250,
                                                        }}>
                                                        {acopios.map(e => {
                                                            return (
                                                                <Option
                                                                    value={
                                                                        e.id
                                                                    }>
                                                                    {e.name}
                                                                </Option>
                                                            );
                                                        })}
                                                    </Select>
                                                </div>
                                            </div>
                                        </div>
                                    )}

                                    <div className="row">
                                        <div className="col-12">
                                            <Table
                                                dataSource={this.state.clients}
                                                columns={this.state.columns}
                                                size={'small'}
                                                showSorterTooltip={false}
                                            />
                                        </div>
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withRouter(withContext(Section));
