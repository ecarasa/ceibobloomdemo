import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

const COLOR = {
    success: 'rgba(139,195,74,.7)',
    info: 'rgba(1,188,212,.7)',
    text: '#3D4051',
    gray: '#EDF0F1',
};

const HistoricalRealChart = (props) => {
    let combo = {};
    combo.option = {
        legend: {
            show: true,
            x: 'right',
            y: 'top',
            data: ['Histórico', 'Real'],
        },
        grid: {
            x: 40,
            y: 60,
            x2: 40,
            y2: 30,
            borderWidth: 0,
        },
        tooltip: {
            show: true,
            trigger: 'axis',
            axisPointer: {
                lineStyle: {
                    color: COLOR.gray,
                },
            },
        },
        xAxis: [
            {
                type: 'category',
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
                axisLabel: {
                    textStyle: {
                        color: '#607685',
                    },
                },
                splitLine: {
                    show: false,
                    lineStyle: {
                        color: '#f3f3f3',
                    },
                },
                data: props.xData
            },
        ],
        yAxis: [
            {
                type: 'value',
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
                axisLabel: {
                    textStyle: {
                        color: '#607685',
                    },
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#f3f3f3',
                    },
                },
            },
        ],
        series: [
            {
                name: 'Histórico',
                type: 'line',
                animation: false,
                smooth: true,
                itemStyle: {
                    normal: {
                        color: COLOR.success,
                        areaStyle: {
                            color: COLOR.success,
                            type: 'default',
                        },
                    },
                },
                data: props.historicalData,
                symbol: 'none',
                legendHoverLink: false,
                z: 3,
            },
            {
                name: 'Real',
                type: 'line',
                animation: false,
                smooth: true,
                itemStyle: {
                    normal: {
                        color: COLOR.info,
                        areaStyle: {
                            color: COLOR.info,
                            type: 'default',
                        },
                    },
                },
                data: props.realData,
                symbol: 'none',
                legendHoverLink: false,
                z: 4,
            },
        ],
    };

    return (
        <ReactEcharts
            option={combo.option}
            theme={'macarons'}
            style={{ height: '450px' }}
        />
    );
};

export default HistoricalRealChart;