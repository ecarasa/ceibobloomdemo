import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter } from 'react-router-dom';
import { observable, action, computed, intercept, observe } from 'mobx'
import { observer } from 'mobx-react';
import { Button, Divider, List, Avatar, Tag, Empty, Spin, Select, Carousel, Modal, Card, Progress, Tabs, Statistic, Table, Drawer, Popover } from 'antd';
import _ from 'lodash';
import {
    ArrowUpOutlined,
    RightOutlined,
    LineChartOutlined,
    ShoppingCartOutlined,
    UsergroupAddOutlined,
    RocketOutlined,
    LikeOutlined,
    AlertOutlined,
    IdcardOutlined,
    PhoneOutlined,
    UnorderedListOutlined,
    DollarCircleOutlined,
    CheckSquareOutlined,
    CheckCircleOutlined,
    InfoCircleOutlined
} from '@ant-design/icons';
import * as d3 from 'd3';
import * as venn from 'venn.js'

// Services
import * as Services from 'services/'

// Components
import DEMO from 'constants/demoData';
import moment from 'moment';
import withContext from 'context/withContext';

// Styles
import './modals.scss';

// Utils
import { formatPrice, formatTn } from 'utils/numbers';

const { TabPane } = Tabs;
const { Option } = Select;

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            isLoading: true,
            showActions: false,
            client: null,
            isMinimalData: false,
            errorMessage: null,
            isShowingMontoVentaPorRubro: false
        };
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps, nextState) {
        if(nextProps) {
            this.setState({ visible : nextProps.visible })

            if(nextProps.clientId != null && this.state.client == null) {
                this.setState({ isLoading : true });

                if(nextProps.acopioId != null) {
                    Services.Client.getClientByIdAndAcopio(nextProps.clientId, nextProps.acopioId, (data) => {
                        if(data.success) {
                            this.setState({
                                client : data.client,
                                isMinimalData: _.defaultTo(data.isMinimalData, false)
                            });
                        } else {
                            this.setState({ errorMessage : data.message, client: data.client });
                        }

                        this.setState({ isLoading : false });
                    }, (err) => {
                        // Do nothing
                        this.setState({ isLoading : true });
                    });
                } else {
                    Services.Client.getClientById(nextProps.clientId, (data) => {
                        if(data.success) {
                            this.setState({
                                client : data.client,
                                isMinimalData: _.defaultTo(data.isMinimalData, false)
                            });
                        } else {
                            this.setState({ errorMessage : data.message, client: data.client });
                        }

                        this.setState({ isLoading : false });
                    }, (err) => {
                        // Do nothing
                        this.setState({ isLoading : true });
                    });
                }
            }
        }
    }

    render() {

        const {
            client,
            errorMessage,
            visible
        } = this.state;

        if(!visible)
            return null;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <Drawer
                        width={'50%'}
                        placement="right"
                        closable={true}
                        onClose={() => {
                            if(this.props.onClose)
                                this.props.onClose()

                            this.setState({
                                client: null,
                                visible: false,
                                errorMessage: null,
                                isShowingMontoVentaPorRubro: false
                            });
                        }}
                        visible={this.state.visible}>
                        { errorMessage == null && (client == null || this.state.isLoading) &&
                            <div style={{ width: '100%', height: 50, textAlign: 'center' }}>
                                <Spin />
                                <p>Cargando detalles del cliente</p>
                            </div>
                        }

                        { errorMessage != null && this.renderClientNotFound() }

                        { errorMessage == null && client != null && !this.state.isMinimalData && this.renderClient() }
                        { errorMessage == null && client != null && this.state.isMinimalData && this.renderClientMinimal() }
                    </Drawer>
                </div>
            </QueueAnim>
        );
    }

    renderClientNotFound = () => {
        const {
            client,
            errorMessage
        } = this.state;

        return (
            <div className="client-details">
                { client && client.data && client.data.length > 0 &&
                    <Popover
                        content={this.renderActions.bind(this, client.data[0])}
                        title={'Escoge una acción'}
                        trigger="click"
                        visible={this.state.showActions}
                        placement="bottom"
                        onVisibleChange={e => {
                            // Do nothing
                            if (e == false) {
                                this.setState({
                                    showActions: false
                                });
                            }
                        }}>
                        <Button
                            onClick={e => {
                                e.preventDefault();
                                this.setState({
                                    showActions: true
                                });
                            }}
                            icon={<UnorderedListOutlined />}
                            type="primary"
                            style={{ position: 'absolute', top : 10, left: 25, zIndex: 99999 }}>
                            Acciones
                        </Button>
                    </Popover>
                }

                <div className="row">
                    <div className="col-12">
                        <article class="profile-card-v1">
                            <h4>{ errorMessage }</h4>
                        </article>
                    </div>
                </div>
            </div>
        );
    }

    renderClient = () => {
        const {
            client,
            isShowingMontoVentaPorRubro
        } = this.state;

        let isClientFound = true;
        let clientData = null;
        let products = [];
        let ventaInsumosHistoricoDataSource = [];
        let ventaInsumosEstimada = null;
        if(client != null && client.data && client.data.length > 0) {
            clientData = client.data[0]
            products = _.uniqBy(client.data, 'producto');

            // New version
            const ventaInsumos = clientData.venta_insumos;
            for(var idx in ventaInsumos.name) {
                const year = ventaInsumos.name[idx];
                const value = ventaInsumos.value[idx];
                if(year != 'venta_insumos_estimada') {
                    ventaInsumosHistoricoDataSource.push({
                        year: year.replace('venta_insumos_', ''),
                        quantity: value
                    });
                } else {
                    ventaInsumosEstimada = value;
                }

                ventaInsumosHistoricoDataSource = _.orderBy(ventaInsumosHistoricoDataSource, ['year'], ['asc']);
            }

            /*const keys = ['venta_insumos_2016', 'venta_insumos_2017', 'venta_insumos_2018', 'venta_insumos_2019', 'venta_insumos_2020', 'venta_insumos_2021', 'venta_insumos_2022'];
            for(var idx in Object.keys(clientData)) {
                const key = Object.keys(clientData)[idx];
                console.log("Key", key);
                console.log("Value", clientData[key]);
                if(keys.indexOf(key) > -1){
                    ventaInsumosHistoricoDataSource.push({
                        year: key.replace('venta_insumos_', ''),
                        quantity: clientData[key]
                    });
                }
            }*/
        } else {
            return (
                <div className="client-details">
                    <div className="row">
                        <div className="col-12">
                            <article class="profile-card-v1">
                                <h4>No encontramos información disponible para el cliente solicitado.</h4>
                            </article>
                        </div>
                    </div>
                </div>
            );
        }

        // Compras historicas
        let historicosCompra = [];
        _.map(products, (i) => {
            if(i.historico) {
                for(var idx in i.historico) {
                    const historico = i.historico[idx];
                    historicosCompra.push({
                        producto: i.producto,
                        ...historico
                    })
                }
            }
        });

        historicosCompra = _.chain(historicosCompra)
            .groupBy('cosecha')
            .map((historico) => {
                return {
                    cosecha: historico[0].cosecha,
                    produccion_maiz: _.defaultTo(_.find(historico, (m) => m.producto == 'maiz'), { produccion : -1 }).produccion,
                    produccion_soja: _.defaultTo(_.find(historico, (m) => m.producto == 'soja'), { produccion : -1 }).produccion
                }
            })
            .value()

        return (
            <div className="client-details">
                <Popover
                    content={this.renderActions.bind(this, clientData)}
                    title={'Escoge una acción'}
                    trigger="click"
                    visible={this.state.showActions}
                    placement="bottom"
                    onVisibleChange={e => {
                        // Do nothing
                        if (e == false) {
                            this.setState({
                                showActions: false
                            });
                        }
                    }}>
                    <Button
                        onClick={e => {
                            e.preventDefault();
                            this.setState({
                                showActions: true
                            });
                        }}
                        icon={<UnorderedListOutlined />}
                        type="primary"
                        style={{ position: 'absolute', top : 10, left: 25, zIndex: 99999 }}>
                        Acciones
                    </Button>
                </Popover>

                <div className="row">
                    <div className="col-12">
                        <article class="profile-card-v1">
                            <h4>{ clientData && clientData.user && clientData.user.profile && clientData.user.profile.name ? clientData.user.profile.name : clientData.adress_book_secundario }</h4>
                            <p className="featured-font no-margin">{ clientData.categoria_cliente }</p>
                            <div class="row">
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Clientes</span> <span className="value">{ clientData.adress_book_principal }</span></p>
                                </div>
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Zona</span> <span className="value">{ clientData.zona }</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Cosecha</span> <span className="value">{ clientData.cosecha }</span></p>
                                </div>
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Planta</span> <span className="value">{ clientData.planta }</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Clientes</span> <span className="value">{ clientData.adress_book_secundario }</span></p>
                                </div>
                            </div>
                        </article>

                        <div class="row" style={{ marginTop: 20 }}>
                            <div class="col-xl-6 mb-12 mt-2">
                                <Card title="Granos">
                                    <h5 className="title mt-1 text-center">Acopio histórico</h5>
                                    <Table
                                        size={'small'}
                                        pagination={false}
                                        dataSource={historicosCompra}
                                        columns={[
                                            {
                                                title: 'Cosecha',
                                                dataIndex: 'cosecha',
                                                key: 'cosecha',
                                                render: (text) => {
                                                    return <span style={{ fontWeight: 600 }}>{text}</span>
                                                }
                                            },
                                            {
                                                title: 'Maíz',
                                                dataIndex: 'produccion_maiz',
                                                key: 'produccion_maiz',
                                                render: (text, record) => {
                                                    if(text == '-1')
                                                        return '-'
                                                    return `${formatTn(text)} ton.`;
                                                }
                                            },
                                            {
                                                title: 'Soja',
                                                dataIndex: 'produccion_soja',
                                                key: 'produccion_soja',
                                                render: (text, record) => {
                                                    if(text == '-1')
                                                        return '-'
                                                    return `${formatTn(text)} ton.`;
                                                }
                                            },
                                        ]}
                                    />

                                    <h5 className="title mt-3 text-center">Potencial de granos</h5>
                                    <Tabs
                                        defaultActiveKey="0"
                                        onChange={(index) => console.log(index)}>
                                        { products.map((product, index) => {
                                            return (
                                                <TabPane
                                                    className="pane-details"
                                                    tab={_.capitalize(product.producto)} key={index}>
                                                    <div className="item-text-pane">
                                                        <p className="no-margin unit">{ _.defaultTo(product.siembra_ha, '-') } ha</p><p className="no-margin">de siembra</p>
                                                    </div>
                                                    <div className="item-text-pane">
                                                        <p className="no-margin unit">{ _.defaultTo(product.rines_estimados_qq, '-') } qq</p><p className="no-margin">de rendición</p>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                            <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Captura de acopio de granos</span>
                                                            <PercentComponent
                                                                status={'active'}
                                                                className="mt-1"
                                                                style={{ maxWidth: '95%' }}
                                                                percentage={parseFloat(product.porcentaje_recupero) * 100}
                                                            />
                                                        </div>
                                                        { product.produccion_estimada_ton != null &&
                                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                <Statistic title="Producción estimada" value={product.produccion_estimada_ton + " ton."} prefix={<LikeOutlined />} />
                                                            </div>
                                                        }
                                                    </div>

                                                    <Divider />

                                                    <div className="text-center">
                                                        <Statistic
                                                            title="Acopio estimado"
                                                            value={product.produccion_estimada_ton * (product.porcentaje_recupero)}
                                                            precision={2}
                                                            valueStyle={{ color: '#3f8600' }}
                                                            prefix={<ArrowUpOutlined />}
                                                            suffix="tn"
                                                        />
                                                    </div>

                                                    { clientData.avanceProduccionAbSecundario != null && clientData.avanceProduccionAbSecundario.length > 0 &&
                                                        <Divider />
                                                    }

                                                    { clientData.avanceProduccionAbSecundario != null && clientData.avanceProduccionAbSecundario.length > 0 &&
                                                        <div className="row">
                                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                <Statistic
                                                                    title="Acopio real"
                                                                    value={parseFloat(clientData.avanceProduccionAbSecundario[0].produccion).toFixed(2) + " ton."}
                                                                    valueStyle={{ color: clientData.avanceProduccionAbSecundario[0].produccion == 0 ? 'red' : 'black' }}
                                                                    prefix={<CheckCircleOutlined />}
                                                                />
                                                            </div>
                                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Porcentaje de avance</span>
                                                                <PercentComponent
                                                                    status={'active'}
                                                                    className="mt-1"
                                                                    style={{ maxWidth: '95%' }}
                                                                    percentage={(parseFloat(clientData.avanceProduccionAbSecundario[0].avance) * 100).toFixed(2)}
                                                                />
                                                            </div>
                                                        </div>
                                                    }
                                                </TabPane>
                                            );
                                        })}
                                    </Tabs>
                                </Card>
                            </div>
                            <div class="col-xl-6 mb-12 mt-2">
                                <Card title="Insumos">
                                    <h5 className="title mt-1 text-center">Ventas históricas</h5>
                                    <Table
                                        size={'small'}
                                        pagination={false}
                                        dataSource={ventaInsumosHistoricoDataSource}
                                        columns={[
                                            {
                                                title: 'Año',
                                                dataIndex: 'year',
                                                key: 'year',
                                            },
                                            {
                                                title: 'Cantidad',
                                                dataIndex: 'quantity',
                                                key: 'quantity',
                                                render: (text, record) => {
                                                    if(text == 'null' || text == null)
                                                        return '-';
                                                    return `${formatPrice(text)}`
                                                }
                                            }
                                        ]}
                                    />

                                    <h5 className="title mt-3 text-center">Potencial de insumos</h5>
                                    <Tabs
                                        defaultActiveKey="0"
                                        onChange={(index) => console.log(index)}>
                                        { products.map((product, index) => {
                                            return (
                                                <TabPane
                                                    className="pane-details"
                                                    tab={_.capitalize(product.producto)} key={index}>
                                                    <div className="item-text-pane">
                                                        <p className="no-margin unit">{ formatPrice(product.inversion_usd_ha) }</p><p className="no-margin">Inversión USD/HA</p>
                                                    </div>
                                                </TabPane>
                                            );
                                        })}
                                    </Tabs>

                                    <div className="row">
                                        <div className="col-xl-12 col-md-12 mt-2 text-center">
                                            <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Captura de inversión</span>
                                            <PercentComponent
                                                status={'active'}
                                                className="mt-1"
                                                style={{ maxWidth: '95%' }}
                                                percentage={parseFloat(clientData.captura_de_inversion) * 100}
                                            />
                                        </div>
                                        <div className="col-xl-12 col-md-12 mt-2 text-center">
                                            <Statistic
                                                title="Inversión total"
                                                value={`${formatPrice(clientData.inversion_total_usd)}`}
                                                prefix={<LikeOutlined />}
                                            />
                                        </div>
                                    </div>

                                    <Divider />

                                    <div className="text-center">
                                        <Statistic
                                            title="Venta estimada"
                                            value={formatPrice(_.defaultTo(ventaInsumosEstimada, 0))}
                                            precision={2}
                                            valueStyle={{ color: '#3f8600' }}
                                            prefix={<ArrowUpOutlined />}
                                        />
                                    </div>

                                    { clientData.avanceInsumosAbSecundario != null && clientData.avanceInsumosAbSecundario.length > 0 &&
                                        <Divider />
                                    }

                                    { clientData.avanceInsumosAbSecundario != null && clientData.avanceInsumosAbSecundario.length > 0 &&
                                        <div className="row">
                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                <Statistic
                                                    title="Venta real"
                                                    value={formatPrice(clientData.avanceInsumosAbSecundario[0].insumos)}
                                                    valueStyle={{ color: clientData.avanceInsumosAbSecundario[0].insumos == 0 ? 'red' : 'black' }}
                                                    prefix={<CheckCircleOutlined />}
                                                />

                                                <div
                                                    onClick={() => {
                                                        this.setState({ isShowingMontoVentaPorRubro: !this.state.isShowingMontoVentaPorRubro });
                                                    }}
                                                    style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 0, cursor: 'pointer' }}>
                                                    <InfoCircleOutlined style={{ fontSize: 16, marginRight: 5, color: '#8e519a' }} />
                                                    <p style={{ textDecoration: 'underline', fontWeight: 600, margin: 0, color: '#8e519a' }}>Visualizar venta por rubro</p>
                                                </div>

                                                { isShowingMontoVentaPorRubro  &&
                                                    <div style={{ marginTop: 10 }}>
                                                        { clientData.montoVentaPorRubro.map((e) => {
                                                            const field = e;
                                                            return (
                                                                <div style={{ width: '100%', flexDirection: 'column' }}>
                                                                    <div style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                                        <p style={{ display: 'flex', flex: 0.5, fontSize: 13, fontWeight: 300, margin: 0, padding: 2 }}>{ field.rubro.properties.rubro_desc }</p>
                                                                        <p style={{ display: 'flex', flex: 0, fontSize: 13, fontWeight: 500, margin: 0, padding: 2, color: 'rgba(24,144,255,1)' }}>{ formatPrice(field.venta) }</p>
                                                                    </div>
                                                                    <div style={{ width: '100%', height: 2, backgroundColor: 'rgba(0,0,0,0.05)', marginTop: 5, marginBottom: 5 }}></div>
                                                                </div>
                                                            );
                                                        })}
                                                    </div>
                                                }
                                            </div>
                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Porcentaje de avance</span>
                                                <PercentComponent
                                                    status={'active'}
                                                    className="mt-1"
                                                    style={{ maxWidth: '95%' }}
                                                    percentage={(parseFloat(clientData.avanceInsumosAbSecundario[0].avance) * 100).toFixed(2)}
                                                />
                                            </div>
                                        </div>
                                    }
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderClientMinimal = () => {
        const {
            client
        } = this.state;

        let isClientFound = true;
        let clientData = null;
        let products = [];
        let ventaInsumosHistoricoDataSource = [];

        if(client != null && client.data && client.data.length > 0) {
            clientData = client.data[0]
            products = _.uniqBy(client.data, 'producto');

            const keys = ['venta_insumos_2016', 'venta_insumos_2017', 'venta_insumos_2018', 'venta_insumos_2019', 'venta_insumos_2020', 'venta_insumos_2021', 'venta_insumos_2022'];
            for(var idx in Object.keys(clientData)) {
                const key = Object.keys(clientData)[idx];
                if(keys.indexOf(key) > -1){
                    ventaInsumosHistoricoDataSource.push({
                        year: key.replace('venta_insumos_', ''),
                        quantity: clientData[key]
                    });
                }
            }
        } else {
            return (
                <div className="client-details">
                    <div className="row">
                        <div className="col-12">
                            <article class="profile-card-v1">
                                <h4>No encontramos información disponible para el cliente solicitado.</h4>
                            </article>
                        </div>
                    </div>
                </div>
            );
        }

        // Compras historicas
        let historicosCompra = [];
        _.map(products, (i) => {
            if(i.historico) {
                for(var idx in i.historico) {
                    const historico = i.historico[idx];
                    historicosCompra.push({
                        producto: i.producto,
                        ...historico
                    })
                }
            }
        });

        historicosCompra = _.chain(historicosCompra)
            .groupBy('cosecha')
            .map((historico) => {
                return {
                    cosecha: historico[0].cosecha,
                    produccion_maiz: _.defaultTo(_.find(historico, (m) => m.producto == 'maiz'), { produccion : -1 }).produccion,
                    produccion_soja: _.defaultTo(_.find(historico, (m) => m.producto == 'soja'), { produccion : -1 }).produccion
                }
            })
            .value()

        return (
            <div className="client-details">
                <Popover
                    content={this.renderActions.bind(this, clientData)}
                    title={'Escoge una acción'}
                    trigger="click"
                    visible={this.state.showActions}
                    placement="bottom"
                    onVisibleChange={e => {
                        // Do nothing
                        if (e == false) {
                            this.setState({
                                showActions: false
                            });
                        }
                    }}>
                    <Button
                        onClick={e => {
                            e.preventDefault();
                            this.setState({
                                showActions: true
                            });
                        }}
                        icon={<UnorderedListOutlined />}
                        type="primary"
                        style={{ position: 'absolute', top : 10, left: 25, zIndex: 99999 }}>
                        Acciones
                    </Button>
                </Popover>

                <div className="row">
                    <div className="col-12">
                        <article class="profile-card-v1">
                            <h4>{ clientData && clientData.user && clientData.user.profile && clientData.user.profile.name ? clientData.user.profile.name : clientData.adress_book_secundario }</h4>
                            <p className="featured-font no-margin">{ clientData.categoria_cliente }</p>
                            <div class="row">
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Cod. Cliente</span> <span className="value">{ clientData['ab.id'] }</span></p>
                                </div>
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Cliente</span> <span className="value">{ clientData['ab2.id'] }</span></p>
                                </div>
                            </div>
                            {/*<div class="row">
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Total acopio</span> <span className="value">{ formatPrice(clientData.total_compra) }</span></p>
                                </div>
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Total acopio TN</span> <span className="value">{ clientData.total_compra_ton.toFixed(1) }</span></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <p className="label-value no-margin"><span className="label">Total venta</span> <span className="value">{ formatPrice(clientData.total_venta) }</span></p>
                                </div>
                            </div>*/}
                        </article>

                        <div class="row" style={{ marginTop: 20, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <div class="col-12 col-xl-6 col-md-6 mt-4 d-flex justify-content-center align-items-center text-center">
                                <Statistic
                                    title="Total Acopio"
                                    value={formatPrice(clientData.total_compra)}
                                    valueStyle={{ color: '#3f8600' }}
                                    prefix={<ArrowUpOutlined />}
                                />
                            </div>

                            <div class="col-12 col-xl-6 col-md-6 mt-4 d-flex justify-content-center align-items-center text-center">
                                <Statistic
                                    title="Total TN Acopio"
                                    value={formatTn(clientData.total_compra_ton) + ' ton.'}
                                    valueStyle={{ color: '#3f8600' }}
                                    prefix={<ArrowUpOutlined />}
                                />
                            </div>

                            <div class="col-12 col-xl-6 col-md-6 mt-4 d-flex justify-content-center align-items-center text-center">
                                <Statistic
                                    title="Total Venta"
                                    value={formatPrice(clientData.total_venta)}
                                    prefix={<LikeOutlined />}
                                />
                            </div>
                        </div>

                        { false &&
                            <div class="row" style={{ marginTop: 20 }}>
                                <div class="col-xl-6 mb-12 mt-2">
                                    <Card title="Granos">
                                        <h5 className="title mt-1 text-center">Acopio histórico</h5>
                                        <Table
                                            size={'small'}
                                            pagination={false}
                                            dataSource={historicosCompra}
                                            columns={[
                                                {
                                                    title: 'Cosecha',
                                                    dataIndex: 'cosecha',
                                                    key: 'cosecha',
                                                    render: (text) => {
                                                        return <span style={{ fontWeight: 600 }}>{text}</span>
                                                    }
                                                },
                                                {
                                                    title: 'Maíz',
                                                    dataIndex: 'produccion_maiz',
                                                    key: 'produccion_maiz',
                                                    render: (text, record) => {
                                                        if(text == '-1')
                                                            return '-'
                                                        return `${formatTn(text)} ton.`;
                                                    }
                                                },
                                                {
                                                    title: 'Soja',
                                                    dataIndex: 'produccion_soja',
                                                    key: 'produccion_soja',
                                                    render: (text, record) => {
                                                        if(text == '-1')
                                                            return '-'
                                                        return `${formatTn(text)} ton.`;
                                                    }
                                                },
                                            ]}
                                        />

                                        <h5 className="title mt-3 text-center">Potencial de granos</h5>
                                        <Tabs
                                            defaultActiveKey="0"
                                            onChange={(index) => console.log(index)}>
                                            { products.map((product, index) => {
                                                return (
                                                    <TabPane
                                                        className="pane-details"
                                                        tab={_.capitalize(product.producto)} key={index}>
                                                        <div className="item-text-pane">
                                                            <p className="no-margin unit">{ _.defaultTo(product.siembra_ha, '-') } ha</p><p className="no-margin">de siembra</p>
                                                        </div>
                                                        <div className="item-text-pane">
                                                            <p className="no-margin unit">{ _.defaultTo(product.rines_estimados_qq, '-') } qq</p><p className="no-margin">de rendición</p>
                                                        </div>

                                                        <div className="row">
                                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Captura de compra de granos</span>
                                                                <PercentComponent
                                                                    status={'active'}
                                                                    className="mt-1"
                                                                    style={{ maxWidth: '95%' }}
                                                                    percentage={parseFloat(product.porcentaje_recupero) * 100}
                                                                />
                                                            </div>
                                                            { product.produccion_estimada_ton != null &&
                                                                <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                    <Statistic title="Producción estimada" value={product.produccion_estimada_ton + " ton."} prefix={<LikeOutlined />} />
                                                                </div>
                                                            }
                                                        </div>

                                                        <Divider />

                                                        <div className="text-center">
                                                            <Statistic
                                                                title="Acopio estimado"
                                                                value={product.produccion_estimada_ton * (product.porcentaje_recupero)}
                                                                precision={2}
                                                                valueStyle={{ color: '#3f8600' }}
                                                                prefix={<ArrowUpOutlined />}
                                                                suffix="tn"
                                                            />
                                                        </div>

                                                        { clientData.avanceProduccionAbSecundario != null && clientData.avanceProduccionAbSecundario.length > 0 &&
                                                            <Divider />
                                                        }

                                                        { clientData.avanceProduccionAbSecundario != null && clientData.avanceProduccionAbSecundario.length > 0 &&
                                                            <div className="row">
                                                                <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                    <Statistic
                                                                        title="Acopio real"
                                                                        value={parseFloat(clientData.avanceProduccionAbSecundario[0].produccion).toFixed(2) + " ton."}
                                                                        valueStyle={{ color: clientData.avanceProduccionAbSecundario[0].produccion == 0 ? 'red' : 'black' }}
                                                                        prefix={<CheckCircleOutlined />}
                                                                    />
                                                                </div>
                                                                <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                                    <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Porcentaje de avance</span>
                                                                    <PercentComponent
                                                                        status={'active'}
                                                                        className="mt-1"
                                                                        style={{ maxWidth: '95%' }}
                                                                        percentage={(parseFloat(clientData.avanceProduccionAbSecundario[0].avance) * 100).toFixed(2)}
                                                                    />
                                                                </div>
                                                            </div>
                                                        }
                                                    </TabPane>
                                                );
                                            })}
                                        </Tabs>
                                    </Card>
                                </div>
                                <div class="col-xl-6 mb-12 mt-2">
                                    <Card title="Insumos">
                                        <h5 className="title mt-1 text-center">Ventas históricas</h5>
                                        <Table
                                            size={'small'}
                                            pagination={false}
                                            dataSource={ventaInsumosHistoricoDataSource}
                                            columns={[
                                                {
                                                    title: 'Año',
                                                    dataIndex: 'year',
                                                    key: 'year',
                                                },
                                                {
                                                    title: 'Cantidad',
                                                    dataIndex: 'quantity',
                                                    key: 'quantity',
                                                    render: (text, record) => {
                                                        if(text == 'null' || text == null)
                                                            return '-';
                                                        return `${formatPrice(text)}`
                                                    }
                                                }
                                            ]}
                                        />

                                        <h5 className="title mt-3 text-center">Potencial de insumos</h5>
                                        <Tabs
                                            defaultActiveKey="0"
                                            onChange={(index) => console.log(index)}>
                                            { products.map((product, index) => {
                                                return (
                                                    <TabPane
                                                        className="pane-details"
                                                        tab={_.capitalize(product.producto)} key={index}>
                                                        <div className="item-text-pane">
                                                            <p className="no-margin unit">{ formatPrice(product.inversion_usd_ha) }</p><p className="no-margin">Inversión USD/HA</p>
                                                        </div>
                                                    </TabPane>
                                                );
                                            })}
                                        </Tabs>

                                        <div className="row">
                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Captura de inversión</span>
                                                <PercentComponent
                                                    status={'active'}
                                                    className="mt-1"
                                                    style={{ maxWidth: '95%' }}
                                                    percentage={parseFloat(clientData.captura_de_inversion) * 100}
                                                />
                                            </div>
                                            <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                <Statistic
                                                    title="Inversión total"
                                                    value={`${formatPrice(clientData.inversion_total_usd)}`}
                                                    prefix={<LikeOutlined />}
                                                />
                                            </div>
                                        </div>

                                        <Divider />

                                        <div className="text-center">
                                            <Statistic
                                                title="Venta estimada"
                                                value={formatPrice(clientData.venta_insumos_estimada)}
                                                precision={2}
                                                valueStyle={{ color: '#3f8600' }}
                                                prefix={<ArrowUpOutlined />}
                                            />
                                        </div>

                                        { clientData.avanceInsumosAbSecundario != null && clientData.avanceInsumosAbSecundario.length > 0 &&
                                            <Divider />
                                        }

                                        { clientData.avanceInsumosAbSecundario != null && clientData.avanceInsumosAbSecundario.length > 0 &&
                                            <div className="row">
                                                <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                    <Statistic
                                                        title="Venta real"
                                                        value={formatPrice(clientData.avanceInsumosAbSecundario[0].insumos)}
                                                        valueStyle={{ color: clientData.avanceInsumosAbSecundario[0].insumos == 0 ? 'red' : 'black' }}
                                                        prefix={<CheckCircleOutlined />}
                                                    />
                                                </div>
                                                <div className="col-xl-12 col-md-12 mt-2 text-center">
                                                    <span style={{ display: 'block', width: '100%', color: 'rgba(0, 0, 0, 0.45)' }}>Porcentaje de avance</span>
                                                    <PercentComponent
                                                        status={'active'}
                                                        className="mt-1"
                                                        style={{ maxWidth: '95%' }}
                                                        percentage={(parseFloat(clientData.avanceInsumosAbSecundario[0].avance) * 100).toFixed(2)}
                                                    />
                                                </div>
                                            </div>
                                        }
                                    </Card>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }

    renderActions = record => {
        let buttons = [];

        if (record.user && record.user.id) {
            buttons.push(
                <a
                    onClick={() => {
                        this.props.history.push({
                            pathname: '/dashboard/alerts/managed',
                            search: `?userId=${record.user.id}`,
                        });
                    }}
                    style={{ fontWeight: 400, fontSize: 14 }}>
                    <AlertOutlined style={{ marginRight: 0 }} /> &nbsp; Crear
                    notificación
                </a>
            );
            buttons.push(<Divider style={{ marginTop: 5, marginBottom: 5 }} />);

            buttons.push(
                <a
                    onClick={() => {
                        this.props.history.push({
                            pathname: '/dashboard/offers/new',
                            search: `?userId=${record.user.id}`,
                        });
                    }}
                    style={{ fontWeight: 400, fontSize: 14 }}>
                    <DollarCircleOutlined style={{ marginRight: 0 }} /> &nbsp; Crear
                    oferta
                </a>
            );
            buttons.push(<Divider style={{ marginTop: 5, marginBottom: 5 }} />);

            if (record.user.profile && record.user.profile.whatsapp_number) {
                buttons.push(
                    <a
                        href={`tel://${record.user.profile.whatsapp_number}`}
                        style={{ fontWeight: 400, fontSize: 14 }}>
                        <PhoneOutlined style={{ marginRight: 0 }} /> &nbsp;
                        Llamar
                    </a>
                );
                buttons.push(
                    <Divider style={{ marginTop: 5, marginBottom: 5 }} />
                );
            }

            if (
                record.user_principal &&
                record.user_principal.profile &&
                record.user_principal.profile.whatsapp_number
            ) {
                // Check if those users are different
                if (record.user_principal.id != record.user.id) {
                    buttons.push(
                        <a
                            href={`tel://${record.user_principal.profile.whatsapp_number}`}
                            style={{ fontWeight: 400, fontSize: 14 }}>
                            <PhoneOutlined style={{ marginRight: 0 }} /> &nbsp;
                            Llamar cliente primario
                        </a>
                    );
                    buttons.push(
                        <Divider style={{ marginTop: 5, marginBottom: 5 }} />
                    );
                }
            }
        }

        if(buttons.length > 0) {
            return (
                <div>
                    {buttons}
                </div>
            )
        }

        return (
            <span>No hay acciones disponibles</span>
        );
    };
}

const PercentComponent = (props) => {
    const isExceded = props.percentage > 100;
    return (
        <Progress
            percent={props.percentage}
            strokeColor={
                !isExceded
                    ? {
                          '0%': '#108ee9',
                          '100%': '#87d068',
                      }
                    : {
                          '0%': '#108ee9',
                          '100%': '#FF4B2B',
                      }
            }
            format={(percent, successPercent) => {
                return `${(parseFloat(props.percentage)).toFixed(1)}%`;
            }}
            status="active"
            showInfo={true}
            {...props}
        />
    );
}

export default withRouter(withContext(Section));