import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Components
import OfferCard from 'components/UPL/marketplace/OfferCard'

// Modules
import _ from 'lodash';
import { List, Avatar, Tag, Empty, Spin, Button, message, Popconfirm, Select, Checkbox } from 'antd';
import DEMO from 'constants/demoData';
import * as Icon from '@ant-design/icons';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

const CheckboxGroup = Checkbox.Group;
const plainOptions = ['Ofertas', 'Productos'];
const defaultCheckedList = ['Ofertas', 'Productos'];

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            productors: [],
            offers: [],
            initialOffers: [],
            filters: {
                checkedList: defaultCheckedList,
                indeterminate: true,
                checkAll: false,
                productor_email: -1
            }
        };
    }

    componentDidMount() {
        this.getMyOffers();
    }

    getMyOffers = () => {
        this.setState({ isLoading: true });
        Services.Product.getProductsCreatedByUserId('me', (response) => {
            this.setState({ isLoading: false });

            if(response.success) {

                // Get productors to filter
                let productors = [];
                _.forEach(response.products, (n) => {
                    if(n) {
                        _.forEach(n.targeted, (i) => {
                            if(i)
                                productors.push(i.user);
                        });
                    }
                });
                productors = _.uniqBy(productors, (i) => i.id);

                this.setState({
                    offers : response.products,
                    initialOffers: response.products,
                    productors
                });
            } else {
                message.error(response.message);
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    onFilter = () => {
        let filteredOffers = [];
        if(this.state.filters.productor_email == -1) {
            filteredOffers = this.state.initialOffers;
        } else {
            filteredOffers = _.filter(this.state.initialOffers, (e) => {
                return _.some(e.targeted, (i) => i && i.user && i.user.email == this.state.filters.productor_email)
            });
        }

        if(this.state.filters.checkedList.length == 1) {
            if(this.state.filters.checkedList[0] == 'Ofertas'){
                filteredOffers = _.filter(filteredOffers, (i) => i.is_offer == true);
            } else {
                filteredOffers = _.filter(filteredOffers, (i) => i.is_offer == false);
            }
        } else if(this.state.filters.checkedList.length == 0) {
            filteredOffers = [];
        }

        this.setState({ offers: filteredOffers })
    }

    onDeleteProduct = (id) => {
        this.setState({ isLoading: true });
        Services.Product.deleteById(id, (response) => {
            if(response.success){
                message.success('El producto se eliminó correctamente');
                this.getMyOffers();
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    render() {
        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Mis ofertas</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div
                                        className="col-12"
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            flexWrap: 'wrap',
                                        }}>

                                        { this.state.productors && this.state.productors.length > 0 &&
                                            <div class="col-12" style={{ paddingLeft: '1.5em' }}>
                                                <div style={{ width: 'auto', display: 'flex', flexDirection:'row', marginBottom: 10 }}>
                                                    <Select
                                                        value={this.state.filters.productor_email}
                                                        onChange={(value) => {
                                                            let filters = this.state.filters;
                                                            filters.productor_email = value;
                                                            this.setState({ filters }, this.onFilter);
                                                        }}
                                                        placeholder={'Filtrar por productor'}
                                                        defaultValue={this.state.filters.productor_email}
                                                        style={{ marginRight: 10 }}>
                                                        <Option value={-1}>Todos los productores</Option>
                                                        { this.state.productors.map((e) => {
                                                            return (
                                                                <Option value={e.email}>{ e.email }</Option>
                                                            );
                                                        })}
                                                    </Select>
                                                    { /* <Button onClick={this.handleTableChange} icon={<Icon.SearchOutlined />}>Filtrar</Button> */ }
                                                </div>

                                                <div style={{ marginBottom: 20 }}>
                                                    <CheckboxGroup
                                                        options={plainOptions}
                                                        value={this.state.filters.checkedList}
                                                        onChange={(e) => {
                                                            let filters = this.state.filters;
                                                            filters.checkedList = e;
                                                            this.setState({ filters }, this.onFilter);
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        }

                                        { this.state.offers.length == 0 &&
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                <Empty
                                                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                    description={'No hay resultados para ese criterio de búsqueda'}
                                                />
                                            </div>
                                        }

                                        { this.state.offers.map((offer, i) => {
                                            return (
                                                <OfferCard
                                                    key={i}
                                                    index={i}
                                                    offer={offer}
                                                    footer={
                                                        <div>
                                                            <div style={{ marginTop: 10 }}>
                                                                <Checkbox
                                                                    checked={offer.enabled}
                                                                    onChange={() => {
                                                                        this.setState({ isLoading : true });
                                                                        Services.Product.toggleEnable(offer.id, (response) => {
                                                                            this.setState({ isLoading : false });

                                                                            if(response.success) {
                                                                                offer.enabled = response.product.enabled;
                                                                                message.success('Se cambió el estado del producto');

                                                                                let offers = this.state.offers;
                                                                                offers[i] = offer;
                                                                                this.setState({ offers })
                                                                            } else {
                                                                                message.error(response.message);
                                                                            }
                                                                        }, (err) => {
                                                                            message.error(Services.Constants.Errors.General);
                                                                            this.setState({isLoading : false });
                                                                        });
                                                                    }}>
                                                                    Producto { offer.enabled ? 'habilitado' : 'deshabilitado' }
                                                                </Checkbox>
                                                            </div>

                                                            <Button
                                                                type="button"
                                                                className="ant-btn ant-btn-primary"
                                                                onClick={() => {
                                                                    this.props.history.push(`/dashboard/offers/${offer.id}/edit`)
                                                                }}
                                                                style={{ width: '100%', marginTop: 20, height: 40, borderRadius: 3 }}>
                                                                <span>Editar producto</span>
                                                            </Button>

                                                            <Popconfirm
                                                                title="¿Estás seguro que quieres borrar este producto?"
                                                                onConfirm={this.onDeleteProduct.bind(this, offer.id)}
                                                                onCancel={() => {
                                                                    // Do nothing
                                                                }}
                                                                okText="Sí, estoy seguro"
                                                                cancelText="Cancelar">
                                                                <Button
                                                                    type="button"
                                                                    className="ant-btn ant-btn-danger"
                                                                    style={{ width: '100%', marginTop: 10, height: 40, borderRadius: 3 }}>
                                                                    <span>Eliminar</span>
                                                                </Button>
                                                            </Popconfirm>
                                                        </div>
                                                    }
                                                />
                                            );
                                        })}
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
