import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Components
import OfferCard from 'components/UPL/marketplace/OfferCard'

// Modules
import _ from 'lodash';
import queryString from 'query-string';
import { List, Avatar, Tag, Empty, Spin, Button, message, Form, Input, Checkbox, Select, Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            offer: {},
            productors: [],
            fileList: []
        };
    }

    componentDidMount() {
        Services.Analytics.trackAudit({ action : `Click en crear oferta`, screen: 'offers', payload: {} });

        Services.User.getUsersByRoleSlug('productor', (data) => {
            if(data.success) {
                let productors = data.users.map((e) => {
                    return {
                        value: e.id,
                        label: e.profile.name,
                        email: e.email
                    }
                });

                this.setState({ productors });
            }
        }, (err) => {
            // Do nothing
        });

        const params = queryString.parse(this.props.location.search);
        if(params.userId != null) {
            const { offer } = this.state;
            offer.productor_ids = [params.userId];
            this.setState({ offer });
        }

        if(this.props.match.params.id) {
            Services.Product.getById(this.props.match.params.id, (data) => {
                if(data.success) {
                    let offer = data.product;
                    // Set productor ids
                    if(data.product.targeted != null && data.product.targeted.length > 0) {
                        offer.productor_ids = data.product.targeted.map((e) => e.user_id);
                    }

                    if(offer.image != null) {
                        this.setState({
                            fileList: [{
                                uid: -1,
                                name: 'uploaded.jpg',
                                status: 'done',
                                url: offer.image
                            }]
                        })
                    }

                    this.setState({ offer });
                    this.form.setFieldsValue(data.product);
                } else {
                    message.error(data.message);
                }
            }, (err) => {
                // Do nothing
            });
        }
    }

    onSubmit = () => {

        const _saveProduct = (data) => {
            Services.Product.create(data, (response) => {
                this.setState({ isSaving : false });

                if(response.success) {
                    message.success('El producto fue creado y dirigido a los productores especificados');
                    this.props.history.push('/dashboard/offers/list/me');
                    Services.Analytics.trackAudit({ action : `Creada oferta`, screen: 'offers', payload: {} });
                } else {
                    message.error(response.message);
                }
            }, (err) => {
                message.error(Services.Constants.Errors.General);
                this.setState({ isSaving : false });
            });
        }

        this.setState({ isSaving : true });

        let data = this.form.getFieldsValue();

        if(this.state.fileList.length > 0 && this.state.fileList[0].status != 'done') {
            Services.Firebase.media.upload(this.state.fileList[0].originFileObj, (response) => {
                if(response.downloadURL)
                    data.image = response.downloadURL;

                _saveProduct(data);
            }, (err) => {
                _saveProduct(data);
            }, {
                container: 'offers'
            });
        } else {
            _saveProduct(data);
        }
    }

    handlePreviewMedia = async file => {
        if (!file.url && !file.preview) {
          file.preview = await getBase64(file.originFileObj);
        }

         this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    };

    handleChangeMedia = ({ fileList }) => {
        if(fileList && fileList.length > 1) {
            this.setState({ fileList: [fileList[fileList.length - 1]] });
        } else {
            this.setState({ fileList: fileList });
        }
    };

    render() {

        const { fileList, offer } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">{ this.props.match.params.id ? 'Editar oferta' : 'Crear oferta' }</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div
                                        className="col-12">
                                        <Form
                                            layout={'vertical'}
                                            onFinish={this.onSubmit}
                                            ref={(e) => { this.form = e; }}>
                                            { this.props.match.params.id != null &&
                                                <Form.Item
                                                    name={['id']}
                                                    style={{ display: 'none' }}>
                                                    <Input
                                                        type={'hidden'}
                                                        value={this.props.match.params.id}
                                                    />
                                                </Form.Item>
                                            }
                                            <Form.Item
                                                name={['name']}
                                                rules={[{ required: true }]}
                                                label="Nombre del producto">
                                                <Input />
                                            </Form.Item>
                                            <Form.Item
                                                name={['price']}
                                                rules={[{ required: true }]}
                                                label="Precio del producto">
                                                <Input
                                                    type={'number'}
                                                    addonBefore={"$"}
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                name={['is_offer']}
                                                valuePropName={"checked"}
                                                label={'Es una oferta'}>
                                                <Checkbox
                                                    valuePropName="checked"
                                                    onChange={() => {
                                                        console.log(this.form.getFieldsValue());
                                                    }}
                                                    style={{ lineHeight: '32px' }}>
                                                    Selecciona esta opción si es una oferta
                                                </Checkbox>
                                            </Form.Item>
                                            <Upload
                                                accept={".png, .jpeg, .jpg, .JPG, .JPEG"}
                                                listType="picture-card"
                                                fileList={fileList}
                                                onPreview={this.handlePreviewMedia}
                                                onChange={this.handleChangeMedia}
                                                onRemove={(file) => {
                                                }}
                                                beforeUpload={ (file) => {
                                                    this.setState(state => ({
                                                        fileList: [...state.fileList, file],
                                                    }));
                                                    return false;
                                                }}>
                                                {fileList.length >= 8 ? null : (
                                                    <div>
                                                        <PlusOutlined />
                                                        <div className="ant-upload-text">{ fileList.length > 0 ? 'Reemplazar imagen' : 'Subir imagen (opcional)' }</div>
                                                    </div>
                                                )}
                                            </Upload>
                                            <Form.Item
                                                name={['productor_ids']}
                                                label={'Productores a los que se le mostrará esta oferta'}>
                                                <Select
                                                    showSearch
                                                    style={{ width: '100%' }}
                                                    placeholder="Toca para escoger productores"
                                                    optionFilterProp="children"
                                                    mode="multiple"
                                                    defaultValue={offer.productor_ids != null ? _.map(offer.productor_ids, (i) => parseInt(i)) : []}
                                                    filterOption={(input, option) =>
                                                        input && option && option.children && option.children.toLowerCase() && option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }>
                                                    { this.state.productors && this.state.productors.map((e) => {
                                                        return (
                                                            <Option value={e.value}>{ e.label } - {e.email}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Form.Item>
                                            {/*<Form.Item
                                                name={['is_global']}
                                                label={'Todos los productores'}>
                                                <Checkbox
                                                    value="is_global"
                                                    style={{ lineHeight: '32px' }}>
                                                    Selecciona esta opción si la oferta irá dirigida a todos los productores
                                                </Checkbox>
                                            </Form.Item>*/}
                                            <Form.Item>
                                                <Button
                                                    loading={this.state.isSaving}
                                                    type="primary"
                                                    className="btn-cta"
                                                    htmlType="submit">
                                                    Guardar
                                                </Button>
                                            </Form.Item>
                                        </Form>
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
