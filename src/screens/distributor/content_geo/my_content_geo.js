import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard'

// Modules
import _ from 'lodash';
import { List, Avatar, Tag, Empty, Spin, Button, message, Popconfirm, Select, Checkbox } from 'antd';
import * as Icon from '@ant-design/icons';
import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            initialContents: [],
            contents: [],
            productors: [],
            filters: {}
        };
    }

    componentDidMount() {
        this.getMyContents();
    }

    getMyContents = () => {
        this.setState({ isLoading: true });
        Services.ContentGeo.getContentCreatedByUserId('me', (response) => {
            this.setState({ isLoading: false });

            if(response.success) {
                // Get productors to filter
                let productors = [];
                _.forEach(response.contents, (n) => {
                    if(n) {
                        _.forEach(n.targeted, (i) => {
                            if(i)
                                productors.push(i.user);
                        });
                    }
                });
                productors = _.uniqBy(productors, (i) => i.id);

                this.setState({ productors, contents: response.contents, initialContents: response.contents })
            } else {
                message.error(response.message);
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    onFilter = () => {
        if(this.state.filters.productor_email == -1) {
            this.setState({ contents : this.state.initialContents });
        } else {
            let contents = _.filter(this.state.initialContents, (e) => {
                console.log(e);
                console.log(this.state.filters.productor_email);
                return _.some(e.targeted, (i) => i && i.user && i.user.email == this.state.filters.productor_email)
            });
            this.setState({ contents })
            // this.setState({ contents : _.filter(this.state.initialContents, (e) => _.some(e.targeted, (i) => i && i.user && i.user.email == this.state.filters.productor_email)) });
        }
    }

    onDeleteContent = (id) => {
        this.setState({ isLoading: true });
        Services.ContentGeo.deleteById(id, (response) => {
            if(response.success){
                message.success('El contenido se eliminó correctamente');
                this.getMyContents();
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    render() {
        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Mi contenido georeferenciado</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    { this.state.productors && this.state.productors.length > 0 &&
                                        <div class="col-12">
                                            <div style={{ width: 'auto', display: 'flex', flexDirection:'row', marginBottom: 10 }}>
                                                <Select
                                                    value={this.state.filters.productor_email}
                                                    onChange={(value) => {
                                                        let filters = this.state.filters;
                                                        filters.productor_email = value;
                                                        this.setState({ filters }, this.onFilter);
                                                    }}
                                                    placeholder={'Filtrar por productor'}
                                                    defaultValue={-1}
                                                    style={{ marginRight: 10 }}>
                                                    <Option value={-1}>Todos los productores</Option>
                                                    { this.state.productors.map((e) => {
                                                        return (
                                                            <Option value={e.email}>{ e.email }</Option>
                                                        );
                                                    })}
                                                </Select>
                                                { /* <Button onClick={this.handleTableChange} icon={<Icon.SearchOutlined />}>Filtrar</Button> */ }
                                            </div>
                                        </div>
                                    }
                                    <div
                                        className="col-12"
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            flexWrap: 'wrap',
                                        }}>

                                        { this.state.contents && this.state.contents.length == 0 &&
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                <Empty
                                                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                    description={'No tenés contenido georeferenciado creado'}
                                                />
                                            </div>
                                        }

                                        { this.state.contents && this.state.contents.length > 0 && this.state.contents.map((content, i) => {
                                            return (
                                                <ContentGeoReferencedCard
                                                    key={i}
                                                    content={content}
                                                    footer={
                                                        <div style={{ width: '100%' }}>
                                                            <div style={{ marginTop: 10 }}>
                                                                <Checkbox
                                                                    checked={content.enabled}
                                                                    onChange={() => {
                                                                        this.setState({ isLoading : true });
                                                                        Services.ContentGeo.toggleEnable(content.id, (response) => {
                                                                            this.setState({ isLoading : false });

                                                                            if(response.success) {
                                                                                content.enabled = response.content.enabled;
                                                                                message.success('Se cambió el estado del contenido');

                                                                                let contents = this.state.contents;
                                                                                contents[i] = content;
                                                                                this.setState({ contents })
                                                                            } else {
                                                                                message.error(response.message);
                                                                            }
                                                                        }, (err) => {
                                                                            message.error(Services.Constants.Errors.General);
                                                                            this.setState({isLoading : false });
                                                                        });
                                                                    }}>
                                                                    Contenido { content.enabled ? 'habilitado' : 'deshabilitado' }
                                                                </Checkbox>
                                                            </div>

                                                            <Button
                                                                type="button"
                                                                className="ant-btn ant-btn-primary"
                                                                onClick={() => {
                                                                    this.props.history.push(`/dashboard/content/edit/${content.id}`)
                                                                }}
                                                                style={{ width: '100%', marginTop: 20, height: 40, borderRadius: 3 }}>
                                                                <span>Editar contenido</span>
                                                            </Button>

                                                            <Popconfirm
                                                                title="¿Estás seguro que quieres borrar este contenido?"
                                                                onConfirm={this.onDeleteContent.bind(this, content.id)}
                                                                onCancel={() => {
                                                                    // Do nothing
                                                                }}
                                                                okText="Sí, estoy seguro"
                                                                cancelText="Cancelar">
                                                                <Button
                                                                    type="button"
                                                                    className="ant-btn ant-btn-danger"
                                                                    style={{ width: '100%', marginTop: 10, height: 40, borderRadius: 3 }}>
                                                                    <span>Eliminar</span>
                                                                </Button>
                                                            </Popconfirm>
                                                        </div>
                                                    }
                                                />
                                            );
                                        })}
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
