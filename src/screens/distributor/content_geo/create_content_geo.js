import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Components
import OfferCard from 'components/UPL/marketplace/OfferCard'

// Modules
import _ from 'lodash';
import { List, Avatar, Tag, Empty, Spin, Button, message, Form, Input, Checkbox, Select, Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;
const { TextArea } = Input;

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            offer: {},
            productors: [],
            fileList: []
        };
    }

    componentDidMount() {
        Services.Analytics.trackAudit({ action : `Click en crear contenido georeferenciado`, screen: 'content_geo', payload: {} });

        Services.User.getUsersByRoleSlug('productor', (data) => {
            if(data.success) {
                let productors = data.users.map((e) => {
                    return {
                        value: e.id,
                        label: e.profile.name,
                        email: e.email
                    }
                });

                this.setState({ productors });
            }
        }, (err) => {
            // Do nothing
        });

        if(this.props.match.params.id) {
            Services.ContentGeo.getById(this.props.match.params.id, (data) => {
                if(data.success) {
                    let content = data.content;
                    // Set productor ids
                    if(data.content.targeted != null && data.content.targeted.length > 0) {
                        content.productor_ids = data.content.targeted.map((e) => e.user_id);
                    }

                    if(content.image != null) {
                        this.setState({
                            fileList: [{
                                uid: -1,
                                name: 'uploaded.jpg',
                                status: 'done',
                                url: content.image
                            }]
                        })
                    }

                    this.setState({ content });
                    this.form.setFieldsValue(data.content);
                } else {
                    message.error(data.message);
                }
            }, (err) => {
                // Do nothing
            });
        }
    }

    onSubmit = () => {
        const _saveContent = (data) => {
            Services.ContentGeo.create(data, (response) => {
                this.setState({ isSaving : false });

                if(response.success) {
                    message.success('El contenido fue creado y dirigido a los productores especificados');
                    this.props.history.push('/dashboard/content/list/me');

                    Services.Analytics.trackAudit({ action : `Creado contenido georeferenciado`, screen: 'content_geo', payload: {} });
                } else {
                    message.error(response.message);
                }
            }, (err) => {
                message.error(Services.Constants.Errors.General);
                this.setState({ isSaving : false });
            });
        }

        this.setState({ isSaving : true });
        let data = this.form.getFieldsValue();
        if(this.state.fileList.length > 0 && this.state.fileList[0].status != 'done') {
            Services.Firebase.media.upload(this.state.fileList[0].originFileObj, (response) => {
                if(response.downloadURL)
                    data.image = response.downloadURL;

                _saveContent(data);
            }, (err) => {
                _saveContent(data);
            }, {
                container: 'georeferenced_contents'
            });
        } else {
            _saveContent(data);
        }
    }

    handlePreviewMedia = async file => {
        if (!file.url && !file.preview) {
          file.preview = await getBase64(file.originFileObj);
        }

         this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    };

    handleChangeMedia = ({ fileList }) => {
        if(fileList && fileList.length > 1) {
            this.setState({ fileList: [fileList[fileList.length - 1]] });
        } else {
            this.setState({ fileList: fileList });
        }
    };

    render() {
        const { fileList } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">{ !this.props.match.params.id ? 'Crear contenido' : 'Editar contenido' }</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div
                                        className="col-12">
                                        <Form
                                            layout={'vertical'}
                                            onFinish={this.onSubmit}
                                            ref={(e) => { this.form = e; }}>
                                            { this.props.match.params.id != null &&
                                                <Form.Item
                                                    style={{ display: 'none' }}
                                                    name={['id']}>
                                                    <Input
                                                        type={'hidden'}
                                                        value={this.props.match.params.id}
                                                    />
                                                </Form.Item>
                                            }
                                            <Form.Item
                                                name={['name']}
                                                rules={[{ required: true }]}
                                                label="Nombre del contenido">
                                                <Input />
                                            </Form.Item>

                                            <Form.Item
                                                name={['text']}
                                                rules={[{ required: true }]}
                                                label="Escribe el texto del contenido">
                                                <TextArea rows={5} />
                                            </Form.Item>
                                            <Form.Item
                                                name={['link_to']}
                                                rules={[{ required: false }]}
                                                label="Link del contenido (opcional)">
                                                <Input />
                                            </Form.Item>
                                            <Upload
                                                accept={".png, .jpeg, .jpg, .JPG, .JPEG"}
                                                listType="picture-card"
                                                fileList={fileList}
                                                onPreview={this.handlePreviewMedia}
                                                onChange={this.handleChangeMedia}
                                                onRemove={(file) => {
                                                }}
                                                beforeUpload={ (file) => {
                                                    this.setState(state => ({
                                                        fileList: [...state.fileList, file],
                                                    }));
                                                    return false;
                                                }}>
                                                {fileList.length >= 8 ? null : (
                                                    <div>
                                                        <PlusOutlined />
                                                        <div className="ant-upload-text">{ fileList.length > 0 ? 'Reemplazar imagen' : 'Subir imagen (opcional)' }</div>
                                                    </div>
                                                )}
                                            </Upload>
                                            <Form.Item
                                                name={['productor_ids']}
                                                label={'Productores a los que se le mostrará este contenido'}>
                                                <Select
                                                    showSearch
                                                    style={{ width: '100%' }}
                                                    placeholder="Toca para escoger productores"
                                                    optionFilterProp="children"
                                                    mode="multiple"
                                                    defaultValue={this.state.offer.productor_ids}
                                                    filterOption={(input, option) =>
                                                        option && option.children && option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }>
                                                    { this.state.productors && this.state.productors.map((e) => {
                                                        return (
                                                            <Option value={e.value}>{ e.label } - {e.email}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Form.Item>
                                            <Form.Item>
                                                <Button
                                                    loading={this.state.isSaving}
                                                    type="primary"
                                                    className="btn-cta"
                                                    htmlType="submit">
                                                    Guardar
                                                </Button>
                                            </Form.Item>
                                        </Form>
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
