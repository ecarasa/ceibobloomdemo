import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter } from 'react-router-dom';
import { observable, action, computed, intercept, observe } from 'mobx'
import { observer } from 'mobx-react';
import { List, Avatar, Tag, Empty, Spin, Select, Carousel, Progress } from 'antd';
import _ from 'lodash';
import { RightOutlined, LineChartOutlined, ShoppingCartOutlined, UsergroupAddOutlined, RocketOutlined, SyncOutlined } from '@ant-design/icons';
import * as d3 from 'd3';
import * as venn from 'venn.js'

// Services
import * as Services from 'services/'

// Components
import HistoricalRealChart from './components/HistoricalRealChart';
import MainChart from './components/MainChart'
import Dashboard from './components/Dashboard'
import ProjectTable from './components/ProjectTable'
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard'

import DEMO from 'constants/demoData';
import moment from 'moment';
import withContext from 'context/withContext';

// Styles
import './index.scss';

// Utils
import { formatPrice } from 'utils/numbers'

const { Option } = Select;

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            isLoadingChart: true,
            acopios: [],
            acopio_id: null,
            indicators: {},
            venn: {},
            currentCompraProductoTitle: '',
            currentVentaRubroTitle: '',
            historicalRealChartData: null,
            historicalRealChartTitle: 'Insumos'
        };

        // Variables
        this.autoPlayCarouselInterval = null;

        // References
        this.carousel = React.createRef();
        this.carouselVenta = React.createRef();
    }

    componentDidMount() {

        let user = this.props.stores.user.getUser();
        Services.Acopio.getAcopios({}, (data) => {
            if(data.success) {
                this.setState({
                    acopios : data.acopios,
                    acopio_id: data.acopios != null ? data.acopios[data.acopios.length > this.props.index ? this.props.index : 0].id : null,
                    isLoading: false
                }, this.getInfoForCurrentAcopio);
            }
        }, (err) => {
            // Do nothing
        });
        /*if(user != null && user.acopios && user.acopios.length > 0) {
            this.setState({
                acopios : user.acopios,
                acopio_id: user.current_acopio_id != null ? user.current_acopio_id : user.acopios[0].id,
                isLoading: false
            }, this.getInfoForCurrentAcopio);
        }*/

        /* Doing a manually autoplay for carousels since there is a bug on react-slick when there are multiple carousels on the same component */
        /*this.autoPlayCarouselInterval = setInterval(() => {
            if(this.state.indicators != null) {
                if(this.carousel)
                    this.carousel.next();

                if(this.carouselVenta)
                    this.carouselVenta.next();
            }
        }, 4000);*/
    }

    fetchHistoricalVsReal = () => {
        this.setState({ isLoadingChart : true });
        Services.Statistics.getHistoricoReal({
            acopioId: this.state.acopio_id
        }, (data) => {
            if(data.success) {
                this.setState({ historicalRealChartData: data.graphData });
            }

            this.setState({ isLoadingChart : false });
        }, (err) => {
            this.setState({ isLoadingChart : false });
        });
    }

    getInfoForCurrentAcopio = () => {
        this.setState({ isLoading: true });

        // Update chars
        // this.fetchHistoricalVsReal();

        if(this.props.onChangeAcopio){
            console.log("Cambiair acopio", this.state.acopio_id);
            this.props.onChangeAcopio(this.state.acopio_id);
        }

        Services.Statistics.getHomeIndicatorsWithData({
            acopioId: this.state.acopio_id
        }, (data) => {
            if(data.success) {
                this.setState({
                    indicators: data.indicators,
                    currentCompraProductoTitle: (data.indicators && data.indicators.compraProductos && data.indicators.compraProductos.length > 0 ? data.indicators.compraProductos[0].producto : null),
                    venn: data.venn
                }, () => {
                    /*if(this.carousel) this.carousel.next();
                    if(this.carouselVenta) this.carouselVenta.next();*/

                    // If received the venn, then draw it
                    /*if(data.venn && data.venn.dataset) {
                        // var sets = [{sets: ['A'], size: 50, label: 'Compra'}, {sets: ['B'], size: 12, label: 'Venta'}, {sets: ['A','B'], size: 2}];
                        let sets = data.venn.dataset;
                        let innerWidth = window.innerWidth;
                        let widthGraphic = window.innerWidth / 4;
                        let heightGraphic = 300;
                        if(innerWidth < 1200)
                            widthGraphic = window.innerWidth / 1.5;

                        let chart = venn.VennDiagram().height(heightGraphic).width(widthGraphic)

                        d3.select("#venn")
                            .datum(sets)
                            .call(chart)

                        d3.selectAll("#venn .venn-circle path")
                        .style("fill-opacity", .8);

                        d3.selectAll("#venn text").style("fill", "white");
                    }*/
                });
            }

            this.setState({ isLoading: false });
        }, (err) => {
            this.setState({ isLoading: false });
        });
    }

    componentWillReceiveProps(nextProps, nextState) {
        // Do nothing
    }

    render() {

        const {
            acopios,
            indicators
        } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">{ this.state.acopio_id != null ? _.find(acopios, (i) => i.id == this.state.acopio_id).name : 'Inicio - Distribuidor' }</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin
                                    indicator={<SyncOutlined style={{ fontSize: 24 }} spin />}
                                    spinning={this.state.isLoading}>
                                    { acopios && acopios.length > 0 &&
                                        <div className="row">
                                            <div className="col-12">
                                                <h5 style={{ fontWeight: 600 }}>Selecciona un acopio</h5>
                                            </div>
                                            <div className="col-12">
                                                <div style={{ width: 'auto', display: 'flex', flexDirection:'row', marginBottom: 10 }}>
                                                    <Select
                                                        value={this.state.acopio_id}
                                                        onChange={(value) => {
                                                            this.setState({ acopio_id: value }, this.getInfoForCurrentAcopio);
                                                        }}
                                                        placeholder={'Selecciona un acopio'}
                                                        defaultValue={this.state.acopio_id}
                                                        style={{ marginRight: 10, width: 250 }}>
                                                        { acopios.map((e) => {
                                                            const isSelected = this.props.selectedAcopios != null && this.props.selectedAcopios.indexOf(e.id) > -1;
                                                            return (
                                                                <Option disabled={isSelected} value={e.id}>{ e.name }</Option>
                                                            );
                                                        })}
                                                    </Select>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="row">
                                                <div className="col-6 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            { this.state.currentCompraProductoTitle == 'Maiz' &&
                                                                <img
                                                                    style={{ width: 35 }}
                                                                    src="assets/images-upl/icons/icon_maiz.svg"
                                                                />
                                                            }

                                                            { this.state.currentCompraProductoTitle == 'Trigo' &&
                                                                <img
                                                                    style={{ width: 35 }}
                                                                    src="assets/images-upl/icons/icon_trigo.svg"
                                                                />
                                                            }

                                                            { this.state.currentCompraProductoTitle == 'Soja' &&
                                                                <img
                                                                    style={{ width: 35 }}
                                                                    src="assets/images-upl/icons/icon_soja.svg"
                                                                />
                                                            }
                                                        </div>
                                                        <div className="card-info">
                                                            <span>Acopio <b>{ this.state.currentCompraProductoTitle }</b></span>
                                                        </div>
                                                        <div className="card-bottom" style={{ display: 'flex', flexDirection: 'column', paddingBottom: 0 }}>
                                                            <Carousel
                                                                effect={'fade'}
                                                                ref={(e) => { this.carousel = e; }}
                                                                afterChange={(e) => {
                                                                    const producto = indicators && indicators.compraProductos ? indicators.compraProductos[e] : null;
                                                                    if(producto) {
                                                                        this.setState({ currentCompraProductoTitle : producto.producto })
                                                                    }
                                                                }}
                                                                dots={false}>
                                                                { indicators && indicators.compraProductos && indicators.compraProductos.map((e, key) => {
                                                                    return (
                                                                        <div>
                                                                            <h1 className="primary" style={{ fontSize: 16, textAlign: 'center', marginBottom: 0 }}><span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ formatPrice(e.monto) }</span></h1>
                                                                            <p style={{ fontSize: 14, textAlign: 'center', marginBottom: 0 }}>de <span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ e.transaccionesQty }</span> transacciones</p>
                                                                            <Progress
                                                                                style={{ maxWidth: '85%', width: '85%' }}
                                                                                percent={(parseFloat(indicators.avanceProduccionCosecha) * 100).toFixed(1)}
                                                                                strokeColor={{
                                                                                    '0%': '#108ee9',
                                                                                    '100%': '#87d068',
                                                                                }}
                                                                                format={(percent, successPercent) => {
                                                                                    return `${(parseFloat(indicators.avanceProduccionCosecha) * 100).toFixed(1)}% av.`;
                                                                                }}
                                                                                status="active"
                                                                                showInfo={true}
                                                                            />
                                                                        </div>
                                                                    );
                                                                })}
                                                            </Carousel>

                                                            <div
                                                                onClick={() => {
                                                                    if(this.carousel)
                                                                        this.carousel.next();
                                                                    /*clearInterval(this.autoPlayCarouselInterval);*/
                                                                }}
                                                                className="overlay-next">
                                                                <RightOutlined
                                                                    style={{ position: 'absolute', right: 0, top: '45%', color: 'rgba(24,144,255,1)', fontSize: 30 }}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-6 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <span>
                                                                { indicators.nuevosClientesCompra }
                                                            </span>
                                                        </div>
                                                        <div className="card-info">
                                                            <span>CLIENTES ACTIVOS ACOPIOS</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ flexDirection: 'column' }}>
                                                            <UsergroupAddOutlined className="text-success" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-6 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <LineChartOutlined className="text-primary" />
                                                        </div>
                                                        <div className="card-info">
                                                            <span>VENTA DE INSUMOS</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ display: 'flex', flexDirection: 'column', paddingBottom: 0 }}>
                                                            <Carousel
                                                                effect={'fade'}
                                                                ref={(e) => { this.carouselVenta = e; }}
                                                                after={(e) => { }}
                                                                dots={false}>
                                                                { indicators && indicators.ventaProductos && indicators.ventaProductos.map((e, key) => {
                                                                    return (
                                                                        <div>
                                                                            <h1 className="primary" style={{ fontSize: 16, textAlign: 'center', marginBottom: 0 }}><span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ formatPrice(e.monto) }</span></h1>
                                                                            <p style={{ fontSize: 14, textAlign: 'center', marginBottom: 0 }}>de <span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ e.transaccionesQty }</span> transacciones</p>
                                                                            <Progress
                                                                                style={{ maxWidth: '85%', width: '85%' }}
                                                                                percent={(parseFloat(indicators.avanceInsumosCosecha) * 100).toFixed(1)}
                                                                                strokeColor={{
                                                                                    '0%': '#108ee9',
                                                                                    '100%': '#87d068',
                                                                                }}
                                                                                format={(percent, successPercent) => {
                                                                                    return `${(parseFloat(indicators.avanceInsumosCosecha) * 100).toFixed(1)}% av.`;
                                                                                }}
                                                                                status="active"
                                                                                showInfo={true}
                                                                            />
                                                                        </div>
                                                                    );
                                                                })}
                                                            </Carousel>

                                                            { false && <div
                                                                onClick={() => {
                                                                    if(this.carouselVenta)
                                                                        this.carouselVenta.next();
                                                                    /*clearInterval(this.autoPlayCarouselInterval);*/
                                                                }}
                                                                className="overlay-next">
                                                                <RightOutlined
                                                                    style={{ position: 'absolute', right: 0, top: '45%', color: 'rgba(24,144,255,1)', fontSize: 30 }}
                                                                />
                                                            </div> }
                                                        </div>
                                                    </div>
                                                </div>

                                                <div
                                                    onClick={() => {
                                                        this.props.history.push({
                                                            pathname: '/dashboard/clients/list',
                                                            search: '?tipo=insumos'
                                                        });
                                                    }}
                                                    className="col-6 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <span>
                                                                { indicators.nuevosClientesVenta }
                                                            </span>
                                                        </div>
                                                        <div className="card-info">
                                                            <span>CLIENTES ACTIVOS INSUMOS</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ flexDirection: 'column' }}>
                                                            <UsergroupAddOutlined className="text-success" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    { false &&
                                        <div className="row">
                                            <div className="col-12 mb-8">
                                                { this.state.isLoadingChart == true ?
                                                    <Spin />
                                                    :
                                                    <div className="box box-default mb-4">
                                                        <div className="box-body">
                                                            <h4>{ this.state.historicalRealChartTitle }</h4>
                                                            <h5>Histórico vs Real</h5>
                                                            <Carousel
                                                                effect={'fade'}
                                                                ref={(e) => { this.carouselGraphics = e; }}
                                                                afterChange={(e) => {
                                                                    if(e == 0)
                                                                        this.setState({ historicalRealChartTitle : 'Ventas de insumos' });
                                                                    else if(e == 1)
                                                                        this.setState({ historicalRealChartTitle : 'TN Acopios' });
                                                                }}
                                                                dots={false}>
                                                                <div>
                                                                    <HistoricalRealChart
                                                                        xData={this.state.historicalRealChartData.venta.xData}
                                                                        realData={this.state.historicalRealChartData.venta.serie_real}
                                                                        historicalData={this.state.historicalRealChartData.venta.serie_historico}
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <HistoricalRealChart
                                                                        xData={this.state.historicalRealChartData.compra.xData}
                                                                        realData={this.state.historicalRealChartData.compra.serie_real}
                                                                        historicalData={this.state.historicalRealChartData.compra.serie_historico}
                                                                    />
                                                                </div>
                                                            </Carousel>

                                                            <div
                                                                onClick={() => {
                                                                    if(this.carouselGraphics)
                                                                        this.carouselGraphics.next();
                                                                }}
                                                                className="overlay-next">
                                                                <RightOutlined
                                                                    style={{ position: 'absolute', right: 0, top: '45%', color: 'rgba(24,144,255,1)', fontSize: 30 }}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                            { false &&
                                                <div className="col-12 mb-4 venn-container">

                                                    <h4 style={{ textAlign: 'center' }}>Acopio y Ventas</h4>
                                                    { this.state.venn != null && this.state.venn.quantities &&
                                                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                                                            <div style={{ display: 'inline-block', textAlign: 'center' }}>
                                                                <div style={{ display: 'inline-block', backgroundColor: 'rgb(31, 119, 180)', width: 25, height: 10, borderRadius: 5 }}></div>
                                                                <span style={{ display: 'inline-block', marginLeft: 10, width: 120, textAlign: 'center' }}>Compras: <b>{ this.state.venn.quantities.compras }</b></span>
                                                            </div>

                                                            <div style={{ display: 'inline-block', textAlign: 'center' }}>
                                                                <div style={{ display: 'inline-block', backgroundColor: 'rgb(255, 127, 14)', width: 25, height: 10, borderRadius: 5 }}></div>
                                                                <span style={{ display: 'inline-block', marginLeft: 10, width: 120, textAlign: 'center' }}>Ventas: <b>{ this.state.venn.quantities.ventas }</b></span>
                                                            </div>
                                                        </div>
                                                    }

                                                    <div id="venn" />

                                                    <p style={{ fontSize: 16, textDecorationLine: 'underline', padding: 0, marginTop: 0, textAlign: 'center' }}>Ver más detalles</p>

                                                    {/*<ContentGeoReferencedCard
                                                        article={{
                                                            id: 2,
                                                            link: 'https://google.com.ar',
                                                            date: 'today',
                                                            content: 'Contenido georeferenciado aquí',
                                                            image: 'https://picsum.photos/200/200'
                                                        }}
                                                    />*/}
                                                </div>
                                            }
                                        </div>
                                    }
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withRouter(withContext(Section));