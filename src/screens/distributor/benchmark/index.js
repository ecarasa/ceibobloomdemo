import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter } from 'react-router-dom';
import { observable, action, computed, intercept, observe, toJS } from 'mobx'
import { observer } from 'mobx-react';
import { List, Avatar, Tag, Empty, Spin, Select, Carousel } from 'antd';
import _ from 'lodash';
import { RightOutlined, LineChartOutlined, ShoppingCartOutlined, UsergroupAddOutlined, RocketOutlined, SyncOutlined } from '@ant-design/icons';
import * as d3 from 'd3';
import * as venn from 'venn.js'

// Services
import * as Services from 'services/'

// Components
import Acopio from './acopio';
import HistoricalRealChart from './components/HistoricalRealChart';
import MainChart from './components/MainChart'
import Dashboard from './components/Dashboard'
import ProjectTable from './components/ProjectTable'
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard'
import DEMO from 'constants/demoData';
import moment from 'moment';
import withContext from 'context/withContext';

// Styles
import './index.scss';

// Utils
import { formatPrice } from 'utils/numbers'

const { Option } = Select;

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            isLoadingChart: true,
            acopios: [],
            acopio_id_1: null,
            acopio_id_2: null,
            indicators: {},
            venn: {},
            currentCompraProductoTitle: '',
            currentVentaRubroTitle: '',
            historicalRealChartData: null,
            historicalRealChartTitle: 'Insumos'
        };
    }

    componentDidMount() {
        // Do nothing
        let user = this.props.stores.user.getUser();
        Services.Acopio.getAcopios({}, (data) => {
            if(data.success) {
                this.setState({ acopios : data.acopios });
            }
        });
    }

    fetchMergeHistoricalVsReal = () => {
        const {
            acopio_id_1,
            acopio_id_2,
            acopios
        } = this.state;

        if(!acopio_id_1 || !acopio_id_2)
            return null;

        this.setState({ isLoadingChart : true });
        Services.Statistics.getHistoricoReal({
            merge: {
                acopio_id_1: this.state.acopio_id_1,
                acopio_id_2: this.state.acopio_id_2
            }
        }, (data) => {
            if(data.success) {
                this.setState({ historicalRealChartData: data.graphData });
            }

            let acopiosFiltered = _.filter(acopios, (i) => [this.state.acopio_id_1, this.state.acopio_id_2].indexOf(i.id) > -1);
            if(acopiosFiltered)
                Services.Analytics.trackAudit({ action : `Filtro por benchmark`, screen: 'benchmark', payload: { acopios : _.map(acopiosFiltered, i => i.neo4j_key) } });

            this.setState({ isLoadingChart : false });
        }, (err) => {
            this.setState({ isLoadingChart : false });
        });
    }

    componentWillReceiveProps(nextProps, nextState) {
        // Do nothing
    }

    render() {

        const {
            acopio_id_1,
            acopio_id_2,
            acopios,
            indicators
        } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Benchmark</h2>
                        <div className="row">
                            <div className="col-6" style={{ paddingLeft: 0, paddingRight: 0 }}>
                                <Acopio
                                    onChangeAcopio={(i) => {
                                        this.setState({ acopio_id_1 : i }, this.fetchMergeHistoricalVsReal);
                                    }}
                                    selectedAcopios={[acopio_id_1, acopio_id_2]}
                                    index={0}
                                />
                            </div>
                            <div className="col-6" style={{ paddingLeft: 0, paddingRight: 0 }}>
                                <Acopio
                                    onChangeAcopio={(i) => {
                                        this.setState({ acopio_id_2 : i }, this.fetchMergeHistoricalVsReal);
                                    }}
                                    selectedAcopios={[acopio_id_1, acopio_id_2]}
                                    index={1}
                                />
                            </div>
                        </div>

                        { this.state.isLoadingChart == true ?
                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', paddingTop: 20, paddingBottom: 20 }}>
                                <Spin />
                            </div>
                            :
                            (this.state.historicalRealChartData &&
                                <div className="row">
                                    <div className="col-12">
                                        <div className="box box-default mb-4">
                                            <div className="box-body">
                                                <h4>{ this.state.historicalRealChartTitle }</h4>
                                                <h5 style={{ marginBottom: 10 }}>Histórico vs Real</h5>
                                                <Carousel
                                                    effect={'fade'}
                                                    ref={(e) => { this.carouselGraphics = e; }}
                                                    afterChange={(e) => {
                                                        if(e == 0)
                                                            this.setState({ historicalRealChartTitle : 'Ventas de insumos' });
                                                        else if(e == 1)
                                                            this.setState({ historicalRealChartTitle : 'TN Acopios' });
                                                    }}
                                                    dots={false}>
                                                    <div>
                                                        <HistoricalRealChart
                                                            xData={this.state.historicalRealChartData.venta.xData}
                                                            realData={this.state.historicalRealChartData.venta.serie_real}
                                                            historicalData={this.state.historicalRealChartData.venta.serie_historico}
                                                        />
                                                    </div>
                                                    <div>
                                                        <HistoricalRealChart
                                                            xData={this.state.historicalRealChartData.compra.xData}
                                                            realData={this.state.historicalRealChartData.compra.serie_real}
                                                            historicalData={this.state.historicalRealChartData.compra.serie_historico}
                                                        />
                                                    </div>
                                                </Carousel>

                                                <div
                                                    onClick={() => {
                                                        if(this.carouselGraphics)
                                                            this.carouselGraphics.next();
                                                    }}
                                                    className="overlay-next">
                                                    <RightOutlined
                                                        style={{ position: 'absolute', right: 0, top: '45%', color: 'rgba(24,144,255,1)', fontSize: 30 }}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withRouter(withContext(Section));