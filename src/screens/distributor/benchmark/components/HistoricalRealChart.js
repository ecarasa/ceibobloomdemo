import React from 'react';
import _ from 'lodash';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

const COLOR = {
    success: 'rgba(139,195,74,.7)',
    info: 'rgba(1,188,212,.7)',
    text: '#3D4051',
    gray: '#EDF0F1',
};

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

const HistoricalRealChart = (props) => {
    let combo = {};
    let series = [];

    let realData = props.realData;
    let historicalData = props.historicalData;

    if(Array.isArray(realData)) {
        for(var idx in realData) {
            const data = realData[idx];
            const randomColor = getRandomColor();
            series.push({
                name: `Real ${data.label}`,
                type: 'line',
                animation: true,
                smooth: true,
                itemStyle: {
                    normal: {
                        areaStyle: {
                            type: 'default',
                        },
                    },
                },
                data: _.map(data.values, (i) => parseFloat(i * 100).toFixed(2)),
                symbol: 'none',
                legendHoverLink: false,
                z: 3
            })
        }
    }

    if(Array.isArray(historicalData)) {
        for(var idx in historicalData) {
            const data = historicalData[idx];
            const randomColor = getRandomColor();
            series.push({
                name: `Histórico ${data.label}`,
                type: 'line',
                animation: true,
                smooth: true,
                itemStyle: {
                    normal: {
                        areaStyle: {
                            type: 'default',
                        },
                    },
                },
                data: _.map(data.values, (i) => parseFloat(i * 100).toFixed(2)),
                symbol: 'none',
                legendHoverLink: false,
                z: 3
            })
        }
    }

    /*realData = _.map(props.realData, (e) => {
        return parseFloat(parseFloat(e) * 100).toFixed(2);
    });

    let historicalData = props.historicalData;
    historicalData = _.map(props.historicalData, (e) => {
        return parseFloat(parseFloat(e) * 100).toFixed(2);
    });*/

    let acopios = _.map(props.realData, (i) => `Real ${i.label}`);
    acopios = acopios.concat(_.map(props.historicalData, (i) => `Histórico ${i.label}`));

    let arr = [];
    let maxYAxis = 0;
    for(var idx in series) {
        const serie = series[idx];
        let maxByData = _.maxBy(_.map(serie.data, (i) => parseFloat(i)));
        maxYAxis = Math.max(maxYAxis, maxByData)
    }

    combo.option = {
        legend: {
            show: true,
            x: 'right',
            y: 'top',
            data: acopios,
            padding: 0,
        },
        grid: {
            x: 40,
            y: 60,
            x2: 40,
            y2: 30,
            borderWidth: 0,
        },
        tooltip: {
            show: true,
            trigger: 'axis',
            axisPointer: {
                lineStyle: {
                    color: COLOR.gray,
                },
            },
            formatter: (series) => {
                const month = series[0].axisValue;

                let strVal = "";
                strVal += month;

                for(var idx in series) {
                    strVal += `<br/>${series[idx].marker}${series[idx].seriesName}: ${series[idx].value}%`;
                }

                return strVal;
                /*console.log(this);
                var s = '<b>'+ this.x +'</b>';

                for(var idx )
                $.each(this.points, function(i, point) {
                    s += '<br/><span style="color:' + point.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y;
                });

                return s;*/
            },
        },
        xAxis: [
            {
                type: 'category',
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
                axisLabel: {
                    textStyle: {
                        color: '#607685',
                    },
                },
                splitLine: {
                    show: false,
                    lineStyle: {
                        color: '#f3f3f3',
                    },
                },
                data: props.xData
            },
        ],
        yAxis: [
            {
                max: _.defaultTo(parseInt(maxYAxis), 0),
                scale: false,
                type: 'value',
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
                axisLabel: {
                    textStyle: {
                        color: '#607685',
                    },
                    formatter: (value, index) => {
                        return `${value}%`;
                    }
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#f3f3f3',
                    },
                },
            },
        ],
        series: series
    };

    return (
        <ReactEcharts
            option={combo.option}
            theme={'macarons'}
            style={{ height: '450px' }}
        />
    );
};

export default HistoricalRealChart;