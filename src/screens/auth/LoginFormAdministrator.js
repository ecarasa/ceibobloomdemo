import React from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Input, Button, Checkbox, notification, message } from 'antd';
import { withRouter } from 'react-router-dom';
import APPCONFIG from 'constants/appConfig';
import ROUTES from 'constants/routes';
import ROLES from 'constants/roles';
import DEMO from 'constants/demoData';

import withContext from 'context/withContext';
import * as Services from 'services'

const FormItem = Form.Item;

class LoginFormAdministrator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const hide = message.loading('Ingresando al sistema', 0);
                const { username, password } = this.state;
                Services.Auth.login({ username, password, role_slug: ROLES.administrator_slug }, (data) => {
                    if(data.success) {
                        Services.Base.SetToken(data.token);
                        this.props.stores.user.setUser(data.user);

                        setTimeout(() => {
                            this.props.history.push(ROUTES.home_administrator);
                        }, 500);

                        Services.Analytics.trackAudit({ action : 'Usuario logueado', screen: 'login', payload: {} });
                    } else {
                        notification.open({
                            type: 'error',
                            message: 'Error',
                            description: data.message
                        });
                    }

                    hide()
                });
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        let { username, password } = this.state;

        return (
            <section className="form-v1-container">
                <h1 style={{ color: 'white', margin: 0 }}>UPL WEB APP</h1>
                <h5 style={{ color: 'white', marginBottom: 30 }}>Acceso para <span style={{ fontWeight: 600 }}>Administrador</span></h5>

                <Form onSubmit={this.handleSubmit} className="form">
                    <FormItem style={{ marginBottom : 5 }}>
                        {getFieldDecorator('username', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Ingresa tu nombre de usuario o email',
                                },
                            ],
                        })(
                            <Input
                                size="large"
                                prefix={
                                    <UserOutlined style={{ fontSize: 13 }} />
                                }
                                onChange={(e) => {
                                    username = e.target.value;
                                    this.setState({ username })
                                }}
                                placeholder="Nombre de usuario o email"
                            />
                        )}
                    </FormItem>
                    <FormItem style={{ marginBottom : 0 }}>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Ingresa tu password',
                                },
                            ],
                        })(
                            <Input
                                size="large"
                                prefix={
                                    <LockOutlined style={{ fontSize: 13 }} />
                                }
                                onChange={(e) => {
                                    password = e.target.value;
                                    this.setState({ password })
                                }}
                                type="password"
                                placeholder="Password"
                            />
                        )}
                    </FormItem>
                    {/*<FormItem className="form-v1__remember">
                        {getFieldDecorator('login2-remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>Remember me</Checkbox>)}
                    </FormItem>*/}
                    <FormItem>
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="btn-cta btn-block">
                            Ingresar
                        </Button>
                    </FormItem>
                </Form>
                {/*<p className="additional-info">
                    Don't have an account yet? <a href={DEMO.signUp}>Sign up</a>
                </p>*/}
                {/*<p className="additional-info">
                    Olvidaste tu contraseña?{' '}
                    <a href={DEMO.forgotPassword}>Reiniciar</a>
                </p>*/}
            </section>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(withRouter(LoginFormAdministrator));

export default withContext(WrappedNormalLoginForm);