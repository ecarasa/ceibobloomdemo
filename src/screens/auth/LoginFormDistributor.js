import React from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Input, Button, Checkbox, notification, message } from 'antd';
import { withRouter } from 'react-router-dom';
import APPCONFIG from 'constants/appConfig';
import ROUTES from 'constants/routes';
import ROLES from 'constants/roles';

import withContext from 'context/withContext';
import * as Services from 'services'

const FormItem = Form.Item;

class LoginFormDistributor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    componentDidMount() {
        // Check if the user is already logged in
        if (Services.Base.GetToken() != null) {
            this.props.history.push(ROUTES.home_distributor);
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const hide = message.loading('Ingresando al sistema', 0);
                const { username, password } = this.state;
                Services.Auth.login({ username, password, role_slug: ROLES.distributor_slug }, (data) => {
                    if (data.success) {
                        Services.Base.SetToken(data.token);
                        this.props.stores.user.setUser(data.user);

                        const redirectTo = this.props.location.state != null && this.props.location.state.from != null ? this.props.location.state.from : null;
                        setTimeout(() => {
                            if (redirectTo != null)
                                this.props.history.push(redirectTo);
                            else
                                this.props.history.push(ROUTES.home_distributor);
                        }, 500);

                        Services.Analytics.trackAudit({ action: 'Usuario logueado', screen: 'login', payload: {} });
                    } else {
                        notification.open({
                            type: 'error',
                            message: 'Error',
                            description: data.message
                        });
                    }

                    hide()
                });
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        let { username, password } = this.state;

        return (
            <section className="form-v1-container">
                <div style={{ textAlign: 'center' }}>
                    <img src={`${window.CONFIG.LOGO}`} style={{ maxWidth: 240, marginBottom: 20 }} />
                </div>

                <h4 style={{ color: 'black', marginBottom: 30, textAlign: 'center' }}>Ingresar</h4>

                <Form onSubmit={this.handleSubmit} className="form">
                    <FormItem style={{ marginBottom: 5 }}>
                        {getFieldDecorator('username', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Ingresa tu nombre de usuario o email',
                                },
                            ],
                        })(
                            <Input
                                size="large"
                                prefix={
                                    <UserOutlined style={{ fontSize: 13 }} />
                                }
                                onChange={(e) => {
                                    username = e.target.value;
                                    this.setState({ username })
                                }}
                                placeholder="E-mail"
                            />
                        )}
                    </FormItem>
                    <FormItem style={{ marginBottom: 0 }}>
                        {getFieldDecorator('password', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Ingresa tu password',
                                },
                            ],
                        })(
                            <Input
                                size="large"
                                prefix={
                                    <LockOutlined style={{ fontSize: 13 }} />
                                }
                                onChange={(e) => {
                                    password = e.target.value;
                                    this.setState({ password })
                                }}
                                type="password"
                                placeholder="Contraseña"
                            />
                        )}
                    </FormItem>
                    {/*<FormItem className="form-v1__remember">
                        {getFieldDecorator('login2-remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>Remember me</Checkbox>)}
                    </FormItem>*/}
                    <FormItem>
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="btn-cta btn-block">
                            Ingresar
                        </Button>
                    </FormItem>
                </Form>

                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <p style={{ marginLeft: 0, color: 'white', fontWeight: 800, fontSize: 13 }}>
                        Powered by <img src="/assets/logo-upl.png" style={{ maxWidth: 50, marginLeft: 10 }} />
                    </p>
                </div>
                {/*<p className="additional-info">
                    Don't have an account yet? <a href={DEMO.signUp}>Sign up</a>
                </p>
                <p className="additional-info" style={{ color: 'white' }}>
                    Olvidaste tu contraseña?{' '}
                    <a href={"/#/auth/forgot-password"} style={{ color: 'white' }}>Recuperar contraseña</a>
                </p>*/}
            </section>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(withRouter(LoginFormDistributor));

export default withContext(WrappedNormalLoginForm);