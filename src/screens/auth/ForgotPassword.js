import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { LockOutlined, UserOutlined, MailOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Input, Button, Checkbox, notification, message } from 'antd';
import { withRouter } from 'react-router-dom';
import APPCONFIG from 'constants/appConfig';
import ROUTES from 'constants/routes';
import ROLES from 'constants/roles';
import DEMO from 'constants/demoData';

import withContext from 'context/withContext';
import * as Services from 'services'

const FormItem = Form.Item;

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    componentDidMount() {
        // Check if the user is already logged in
        if(Services.Base.GetToken() != null) {
            this.props.history.push(ROUTES.home_distributor);
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const email = values.email;
                const hide = message.loading('Enviando email de recuperación', 0);
                Services.Auth.requestForgotPassword({ email }, (data) => {
                    if(data.success) {
                        hide();
                        message.success('Revisa tu dirección de correo electrónico');
                        this.props.history.goBack()

                        Services.Analytics.trackAudit({ action : 'Cambio de password', screen: 'forgot_password', payload: {} });
                    }
                }, (err) => {
                    // Do nothing
                    hide();
                });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        let { username, password } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <section className="cover cover-page">
                        <div className="cover-bg-img" style={{backgroundImage: 'url(assets/images-demo/covers/bench-accounting-49909-unsplash-lg.jpg)'}}></div>
                            <div className="cover-form-container">
                                <div className="col-12 col-md-8 col-lg-6 col-xl-4">
                                    <section className="form-v1-container">
                                        <h2>¿Olvidaste tu contraseña?</h2>
                                        <p className="additional-info col-lg-10 mx-lg-auto mb-3">Escribe tu dirección de correo electrónico y te enviaremos instrucciones para reestablecer tu contraseña.</p>
                                        <Form onSubmit={this.handleSubmit} className="form-v1">
                                            <FormItem className="mb-3">
                                                {getFieldDecorator('email', {
                                                rules: [
                                                    { type: 'email', message: 'Escribe una dirección de correo electrónico válida' },
                                                    { required: true, message: 'Escribe una dirección de correo electrónico válida' }
                                                ],
                                                })(
                                                    <Input size="large" prefix={<MailOutlined style={{ fontSize: 13 }} />} placeholder="Email" />
                                                )}
                                            </FormItem>
                                            <FormItem>
                                                <Button type="primary" htmlType="submit" className="btn-cta btn-block">
                                                    Enviar instrucciones
                                                </Button>
                                            </FormItem>
                                        </Form>
                                    </section>
                                </div>
                            </div>
                    </section>
                </div>
            </QueueAnim>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(withRouter(ForgotPassword));

export default withContext(WrappedNormalLoginForm);