import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { LockOutlined, UserOutlined, MailOutlined } from '@ant-design/icons';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Input, Button, Checkbox, notification, message } from 'antd';
import { withRouter } from 'react-router-dom';
import APPCONFIG from 'constants/appConfig';
import ROUTES from 'constants/routes';
import ROLES from 'constants/roles';
import DEMO from 'constants/demoData';

import withContext from 'context/withContext';
import * as Services from 'services'

const FormItem = Form.Item;

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    componentDidMount() {

    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                if(values.password != values.password_confirmation) {
                    message.error('La confirmación de la contraseña debe ser la misma que tu contraseña');
                    return;
                }

                const hide = message.loading('Reestableciendo tu contraseña', 0);
                const token = this.props.match.params.token;
                Services.Auth.postForgotPassword({ password: values.password, password_confirmation: values.password_confirmation, token }, (data) => {
                    if(data.success) {
                        hide();
                        message.success('Tu contraseña fue reestablecida con éxito');
                        this.props.history.push('/#/auth/login/productor');
                    }
                }, (err) => {
                    // Do nothing
                    hide();
                });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        let { username, password } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <section className="cover cover-page">
                        <div className="cover-bg-img" style={{backgroundImage: 'url(assets/images-demo/covers/bench-accounting-49909-unsplash-lg.jpg)'}}></div>
                            <div className="cover-form-container">
                                <div className="col-12 col-md-8 col-lg-6 col-xl-4">
                                    <section className="form-v1-container">
                                        <h2>Reestablecer mi contraseña</h2>
                                        <p className="additional-info col-lg-10 mx-lg-auto mb-3">Escribe tu nueva contraseña y la confirmación; luego presiona sobre el botón reestablecer.</p>
                                        <Form onSubmit={this.handleSubmit} className="form-v1">
                                            <FormItem className="mb-3">
                                                {getFieldDecorator('password', {
                                                rules: [
                                                    { required: true }
                                                ],
                                                })(
                                                    <Input size="large" type={'password'} prefix={<LockOutlined style={{ fontSize: 13 }} />} placeholder="Tu contraseña" />
                                                )}
                                            </FormItem>

                                            <FormItem className="mb-3">
                                                {getFieldDecorator('password_confirmation', {
                                                rules: [
                                                    { required: true }
                                                ],
                                                })(
                                                    <Input size="large" type={'password'} prefix={<LockOutlined style={{ fontSize: 13 }} />} placeholder="Confirma tu contraseña" />
                                                )}
                                            </FormItem>
                                            <FormItem>
                                                <Button type="primary" htmlType="submit" className="btn-cta btn-block">
                                                    Reestablecer
                                                </Button>
                                            </FormItem>
                                        </Form>
                                    </section>
                                </div>
                            </div>
                    </section>
                </div>
            </QueueAnim>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(withRouter(ForgotPassword));

export default withContext(WrappedNormalLoginForm);