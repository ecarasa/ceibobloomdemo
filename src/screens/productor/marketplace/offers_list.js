import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { observer } from 'mobx-react';
import { connect } from 'react-redux';

// Components
import OfferCard from 'components/UPL/marketplace/OfferCard'

// Modules
import _ from 'lodash';
import { List, Avatar, Tag, Empty, Spin, Button, message } from 'antd';
import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            offers: [],
            cart: null
        };
    }

    componentDidMount() {
        this.getProducts()
        this.getMyCart()
    }

    getMyCart = () => {
        Services.Cart.getMyCart((data) => {
            if(data.success) {
                this.props.stores.cart.setCart(data.cart);
                this.setState({
                    cart: data.cart
                });
            }
        }, (err) => {
            // Do nothing
        });
    }

    getProducts = () => {
        this.setState({ isLoading: true });
        Services.Product.getProductsTargetedForUserId('me', (response) => {
            this.setState({ isLoading: false });

            if(response.success) {
                this.setState({ offers : response.products })
            } else {
                message.error(response.message);
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    addToCart = (id) => {
        let offers = this.state.offers;
        let offerIndex = _.findIndex(offers, (e) => e.id == id);
        offers[offerIndex].isLoading = true;
        this.setState({ offers });

        Services.Cart.addItemToCart({ id: this.state.cart.id, product_id: id }, (data) => {
            if(data.success) {
                message.success('Producto agregado al carrito de compras');

                offers[offerIndex].isLoading = false;
                this.setState({ offers });

                this.getMyCart();

                Services.Analytics.trackAudit({ action : `Producto agregado al carrito`, screen: 'offers', payload: {} });
            }
        }, (err) => {
            // Do nothing
        });
    }

    render() {

        let productsCart = [];
        if(this.props.stores.cart.getCart() != null && this.props.stores.cart.getCart().products && this.props.stores.cart.getCart().products.length > 0) {
            productsCart = _.map(this.props.stores.cart.getCart().products, (i) => i.id);
        }

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Marketplace</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div
                                        className="col-12"
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            flexWrap: 'wrap',
                                        }}>

                                        { this.state.offers.length == 0 &&
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                <Empty
                                                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                    description={'No hay ofertas disponibles para mostrarte en este momento'}
                                                />
                                            </div>
                                        }

                                        {this.state.offers.map((offer, i) => {
                                            return (
                                                <OfferCard
                                                    key={i}
                                                    index={i}
                                                    offer={offer}
                                                    footer={
                                                        <div>
                                                            <Button
                                                                disabled={productsCart && productsCart.indexOf(offer.id) > -1}
                                                                type="button"
                                                                loading={offer.isLoading}
                                                                className="ant-btn ant-btn-primary"
                                                                onClick={this.addToCart.bind(this, offer.id)}
                                                                style={{ width: '100%', marginTop: 20, height: 40, borderRadius: 3 }}>
                                                                <span>Agregar al carrito</span>
                                                            </Button>
                                                        </div>
                                                    }
                                                />
                                            );
                                        })}
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
