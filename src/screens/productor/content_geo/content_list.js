import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard'

// Modules
import _ from 'lodash';
import { List, Avatar, Tag, Empty, Spin, Button, message, Popconfirm } from 'antd';
import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            contents: []
        };
    }

    componentDidMount() {
        this.getMyContents();
    }

    getMyContents = () => {
        this.setState({ isLoading: true });
        Services.ContentGeo.getContentTargetedForUserId('me', (response) => {
            this.setState({ isLoading: false });

            if(response.success) {
                this.setState({ contents : response.contents })
            } else {
                message.error(response.message);
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    onDeleteContent = (id) => {
        this.setState({ isLoading: true });
        Services.ContentGeo.deleteById(id, (response) => {
            if(response.success){
                message.success('El contenido se eliminó correctamente');
                this.getMyContents();
            }
        }, (err) => {
            this.setState({ isLoading: false });
            message.error(Services.Constants.Errors.General);
        });
    }

    render() {
        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Contenido georeferenciado</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div
                                        className="col-12"
                                        style={{
                                            display: 'flex',
                                            flexDirection: 'row',
                                            flexWrap: 'wrap',
                                        }}>

                                        { this.state.contents.length == 0 &&
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                <Empty
                                                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                                                    description={'Aún no hay contenido georeferenciado para tí.'}
                                                />
                                            </div>
                                        }

                                        { this.state.contents.map((content, i) => {
                                            return (
                                                <ContentGeoReferencedCard
                                                    key={i}
                                                    content={content}
                                                />
                                            );
                                        })}
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);