import React from 'react';
import { List, Avatar, Tag, Empty, Spin } from 'antd';

// Components
import PieChart from './components/PieChart';

import DEMO from 'constants/demoData';
import * as Services from 'services'
import moment from 'moment';
import withContext from 'context/withContext';

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ isLoading : false });
        }, 2000);
    }

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <Spin spinning={this.state.isLoading}>
                        <div className="row">
                            <div className="col-6">
                                <PieChart />
                            </div>
                            <div className="col-6">
                                <PieChart />
                            </div>
                        </div>
                    </Spin>
                </div>
            </div>
        );
    }
}

export default withContext(Section);