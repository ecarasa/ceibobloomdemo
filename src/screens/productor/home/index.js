import React from 'react';
import QueueAnim from 'rc-queue-anim';
import { withRouter } from 'react-router-dom';
import { observable, action, computed, intercept, observe } from 'mobx'
import { observer } from 'mobx-react';
import { List, Avatar, Tag, Empty, Spin, Select, Carousel } from 'antd';
import _ from 'lodash';
import { RightOutlined, LineChartOutlined, ShoppingCartOutlined, UsergroupAddOutlined, RocketOutlined, SyncOutlined } from '@ant-design/icons';
import * as d3 from 'd3';
import * as venn from 'venn.js'

// Services
import * as Services from 'services/'

// Components
import HistoricalRealChart from 'screens/distributor/home/components/HistoricalRealChart';
import MainChart from './components/MainChart'
import Dashboard from './components/Dashboard'
import ProjectTable from './components/ProjectTable'
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard'

import DEMO from 'constants/demoData';
import moment from 'moment';
import withContext from 'context/withContext';

// Styles
import './index.scss';

// Utils
import { formatPrice } from 'utils/numbers'

const { Option } = Select;

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            isLoadingChart: true,
            acopios: [],
            indicators: {},
            venn: {},
            currentCompraProductoTitle: '',
            currentVentaRubroTitle: '',
            historicalRealChartData: null,
            historicalRealChartTitle: 'Insumos'
        };

        // Variables
        this.autoPlayCarouselInterval = null;

        // References
        this.carousel = React.createRef();
        this.carouselVenta = React.createRef();
    }

    componentDidMount() {
        let user = this.props.stores.user.getUser();
        if(user != null && user.productors && user.productors.length > 0) {
            this.setState({
                productors: user.productors,
                productor_id: user.productors[0].id,
                isLoading: false
            }, () => {
                this.getInfoForProductor(user.productors[0].id)
            });
        } else {
            this.getInfoForProductor()
        }

        Services.Cart.getMyCart((data) => {
            if(data.success)
                this.props.stores.cart.setCart(data.cart);
        }, (err) => {});
    }

    fetchHistoricalVsReal = (productor_id = null) => {
        this.setState({ isLoadingChart : true });
        Services.Statistics.getHistoricoReal({ productorId: productor_id }, (data) => {
            if(data.success) {
                this.setState({ historicalRealChartData: data.graphData });
            }

            this.setState({ isLoadingChart : false });
        }, (err) => {
            this.setState({ isLoadingChart : false });
        });
    }

    getInfoForProductor = (productor_id = null) => {
        this.setState({ isLoading: true });

        // Update chars
        this.fetchHistoricalVsReal(productor_id);

        if(productor_id != null) {
            Services.Statistics.getHomeIndicatorsWithData({ productorId: productor_id }, (data) => {
                if(data.success) {
                    this.setState({
                        indicators: data.indicators,
                        currentCompraProductoTitle: (data.indicators && data.indicators.compraProductos && data.indicators.compraProductos.length > 0 ? data.indicators.compraProductos[0].producto : null),
                        venn: data.venn
                    });
                }

                this.setState({ isLoading: false });
            }, (err) => {
                this.setState({ isLoading: false });
            });
        } else {
            Services.Statistics.getHomeIndicators((data) => {
                if(data.success) {
                    this.setState({
                        indicators: data.indicators,
                        currentCompraProductoTitle: (data.indicators && data.indicators.compraProductos && data.indicators.compraProductos.length > 0 ? data.indicators.compraProductos[0].producto : null),
                        venn: data.venn
                    });
                }

                this.setState({ isLoading: false });
            }, (err) => {
                this.setState({ isLoading: false });
            });
        }
    }

    componentWillReceiveProps(nextProps, nextState) {
        // Do nothing
    }

    render() {

        const {
            acopios,
            productors,
            indicators
        } = this.state;

        const ventaMaiz = _.defaultTo(_.find(indicators.compraProductos, (i) => i.producto == 'Maiz'), { monto: 0, transaccionesQty: 0 });
        const ventaSoja = _.defaultTo(_.find(indicators.compraProductos, (i) => i.producto == 'Soja'), { monto: 0, transaccionesQty: 0 });
        const ventaTrigo = _.defaultTo(_.find(indicators.compraProductos, (i) => i.producto == 'Trigo'), { monto: 0, transaccionesQty: 0 });

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">{ this.state.acopio_id != null ? _.find(acopios, (i) => i.id == this.state.acopio_id).name : 'Inicio - Productor' }</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin
                                    indicator={<SyncOutlined style={{ fontSize: 24 }} spin />}
                                    spinning={this.state.isLoading}>
                                    { productors && productors.length > 0 &&
                                        <div className="row">
                                            <div className="col-12">
                                                <h5 style={{ fontWeight: 600 }}>Ver productor</h5>
                                            </div>
                                            <div className="col-12">
                                                <div style={{ width: 'auto', display: 'flex', flexDirection:'row', marginBottom: 10 }}>
                                                    <Select
                                                        value={this.state.productor_id}
                                                        onChange={(value) => {
                                                            this.setState({ productor_id: value }, () => {
                                                                this.getInfoForProductor(value)
                                                            });
                                                        }}
                                                        placeholder={'Selecciona un productor'}
                                                        defaultValue={this.state.productor_id}
                                                        style={{ marginRight: 10, width: 250 }}>
                                                        { productors.map((e) => {
                                                            return (
                                                                <Option value={e.id}>{ e.email }</Option>
                                                            );
                                                        })}
                                                    </Select>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="row">
                                                <div className="col-xl-3 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <img
                                                                style={{ width: 35 }}
                                                                src="assets/images-upl/icons/icon_maiz.svg"
                                                            />
                                                        </div>
                                                        <div className="card-info">
                                                            <span>Venta de maíz</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ display: 'flex', flexDirection: 'column', paddingBottom: 0 }}>
                                                            { ventaMaiz != null &&
                                                                <div>
                                                                    <h1 className="primary" style={{ fontSize: 16, textAlign: 'center' }}><span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ formatPrice(ventaMaiz.monto) }</span></h1>
                                                                    <p style={{ fontSize: 16, textAlign: 'center' }}>de <span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ ventaMaiz.transaccionesQty }</span> transacciones</p>
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xl-3 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <img
                                                                style={{ width: 35 }}
                                                                src="assets/images-upl/icons/icon_soja.svg"
                                                            />
                                                        </div>
                                                        <div className="card-info">
                                                            <span>Venta de soja</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ display: 'flex', flexDirection: 'column', paddingBottom: 0 }}>
                                                            { ventaSoja != null &&
                                                                <div>
                                                                    <h1 className="primary" style={{ fontSize: 16, textAlign: 'center' }}><span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ formatPrice(ventaSoja.monto) }</span></h1>
                                                                    <p style={{ fontSize: 16, textAlign: 'center' }}>de <span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ ventaSoja.transaccionesQty }</span> transacciones</p>
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xl-3 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <img
                                                                style={{ width: 35 }}
                                                                src="assets/images-upl/icons/icon_trigo.svg"
                                                            />
                                                        </div>
                                                        <div className="card-info">
                                                            <span>Venta de trigo</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ display: 'flex', flexDirection: 'column', paddingBottom: 0 }}>
                                                            { ventaTrigo != null &&
                                                                <div>
                                                                    <h1 className="primary" style={{ fontSize: 16, textAlign: 'center' }}><span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ formatPrice(ventaTrigo.monto) }</span></h1>
                                                                    <p style={{ fontSize: 16, textAlign: 'center' }}>de <span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ ventaTrigo.transaccionesQty }</span> transacciones</p>
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xl-3 mb-4">
                                                    <div className="number-card-v1">
                                                        <div className="card-top">
                                                            <LineChartOutlined className="text-primary" />
                                                        </div>
                                                        <div className="card-info">
                                                            <span>Compra de productos</span>
                                                        </div>
                                                        <div className="card-bottom" style={{ display: 'flex', flexDirection: 'column', paddingBottom: 0 }}>
                                                            { indicators && indicators.ventaProductos && indicators.ventaProductos.map((e, key) => {
                                                                return (
                                                                    <div>
                                                                        <h1 className="primary" style={{ fontSize: 16, textAlign: 'center' }}><span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ formatPrice(e.monto) }</span></h1>
                                                                        <p style={{ fontSize: 16, textAlign: 'center' }}>de <span style={{ color: 'rgba(24,144,255,1)', fontWeight: 600 }}>{ e.transaccionesQty }</span> transacciones</p>
                                                                    </div>
                                                                );
                                                            })}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xl-12 mb-12">
                                            { this.state.isLoadingChart == true ?
                                                <Spin />
                                                :
                                                (this.state.historicalRealChartData &&
                                                    <div className="box box-default mb-4">
                                                        <div className="box-body">
                                                            <h4>{ this.state.historicalRealChartTitle }</h4>
                                                            <h5>Histórico vs Real</h5>
                                                            <Carousel
                                                                effect={'fade'}
                                                                ref={(e) => { this.carouselGraphics = e; }}
                                                                afterChange={(e) => {
                                                                    if(e == 0)
                                                                        this.setState({ historicalRealChartTitle : 'Insumos' });
                                                                    else if(e == 1)
                                                                        this.setState({ historicalRealChartTitle : 'Productos' });
                                                                }}
                                                                dots={false}>
                                                                <div>
                                                                    <HistoricalRealChart
                                                                        xData={this.state.historicalRealChartData.venta.xData}
                                                                        realData={this.state.historicalRealChartData.venta.serie_real}
                                                                        historicalData={this.state.historicalRealChartData.venta.serie_historico}
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <HistoricalRealChart
                                                                        xData={this.state.historicalRealChartData.compra.xData}
                                                                        realData={this.state.historicalRealChartData.compra.serie_real}
                                                                        historicalData={this.state.historicalRealChartData.compra.serie_historico}
                                                                    />
                                                                </div>
                                                            </Carousel>

                                                            <div
                                                                onClick={() => {
                                                                    if(this.carouselGraphics)
                                                                        this.carouselGraphics.next();
                                                                }}
                                                                className="overlay-next">
                                                                <RightOutlined
                                                                    style={{ position: 'absolute', right: 0, top: '45%', color: 'rgba(24,144,255,1)', fontSize: 30 }}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withRouter(withContext(Section));