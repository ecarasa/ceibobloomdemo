import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, message, Table, Popconfirm } from 'antd';
import * as Icon from '@ant-design/icons';
import QueueAnim from 'rc-queue-anim';
import _ from 'lodash';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            data: [],
            pagination: {},
            pattern: null,
            loading: false,
        };

        this.form = null;
        this.TABLE_COLUMNS = [{
            title: 'Email',
            dataIndex: 'email',
            sorter: false,
            render: (text, record) => {
                return (
                    <a href={`/#/dashboard/admin/users/${record.id}/edit`}>{ text }</a>
                );
            }
        }, {
            title: 'Rol',
            dataIndex: 'role_slug',
            filters: [
                { text: 'Distribuidor', value: 'distributor' },
                { text: 'Productor', value: 'productor' },
                { text: 'Asesor de productores', value: 'asesor_productores' },
            ],
            render: (text, record) => {
                const color = text == 'productor' ? '#87d068' : '#108ee9';
                return (
                    <Tag color={color}>{ text }</Tag>
                );
            }
        }, {
            title: 'Nombre',
            render: (text, record) => record.profile.name || '-'
        }, {
            title: 'Fecha de creación',
            dataIndex: 'created_at',
            sorter: false,
            render: (text, record) => {
                return moment(text).format('YYYY-MM-DD')
            }
        }, {
            title: 'Acciones',
            render: (text, record) => {
                return (
                    <Popconfirm
                        title="¿Estás seguro que quieres borrar este usuario?"
                        onConfirm={this.onDeleteUserById.bind(this, record.id)}
                        okText="Eliminar usuario"
                        cancelText="Cancelar">
                        <a href="#">Eliminar</a>
                    </Popconfirm>
                );
            }
        }];
    }

    handleTableChange = (pagination = null, filters = {}, sorter = {}) => {
        const pager = { ...this.state.pagination };
        if(!pagination)
            pagination = this.state.pagination;

        if(pagination) {
            pager.current = pagination.current;
            this.setState({ pagination: pager });
        }

        this.fetch({
            results: pagination.pageSize,
            page: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
            pattern: this.state.pattern
        });
    }

    fetch = (params = {}) => {
        this.setState({ isLoading: true });
        Services.User.getUsers(params, (data) => {
            if(data.success) {
                this.setState({ data: data.users, isLoading: false });
            } else {
                this.setState({ isLoading: false });
            }
        }, (err) => {
            this.setState({ isLoading: false });
        });
    }

    componentDidMount() {
        this.fetch();
    }

    onDeleteUserById = (id) => {
        this.setState({ isLoading : true });
        Services.User.deleteById(id, (response) => {
            if(response.success) {
                message.success('El usuario se eliminó del sistema');
            }

            let index = _.findIndex(this.state.data, (e) => e.id == id);
            let data = [...this.state.data];
            data.splice(index, 1);

            this.setState({
                data,
                isLoading : false
            });
        }, (err) => {
            message.error('Hubo un problema al eliminar este usuario. Inténtalo nuevamente');
            this.setState({ isLoading : false });
        });
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });

        let data = this.form.getFieldsValue();
        Services.User.updateMe(data, (response) => {
            this.setState({ isSaving : false });
            this.props.stores.user.refresh();
            message.success('Tu perfil fue modificado con éxito');
        }, (err) => {
            message.error('Hubo un problema al modificar tu perfil. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        });
    }

    render() {

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Usuarios</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <div style={{ width: 'auto', display: 'flex', flexDirection:'row', marginBottom: 10 }}>
                                        <Input
                                            value={this.state.pattern}
                                            onChange={(evt) => {
                                                this.setState({ pattern : evt.target.value });
                                            }}
                                            placeholder={'Busca por email o nombre'}
                                            style={{ marginRight: 10 }}
                                        />
                                        <Button onClick={this.handleTableChange} icon={<Icon.SearchOutlined />}>Buscar</Button>
                                    </div>

                                    <Table
                                        columns={this.TABLE_COLUMNS}
                                        rowKey={record => record.id}
                                        dataSource={this.state.data}
                                        pagination={this.state.pagination}
                                        loading={this.state.isLoading}
                                        onChange={this.handleTableChange}
                                    />
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
