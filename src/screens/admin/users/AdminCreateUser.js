import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, Select, message } from 'antd';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            user: {},
            acopios: [],
            productors: [],
            roleSelected: null
        };

        this.form = null;
    }

    componentDidMount() {
        // Get acopios
        Services.Acopio.getAcopios({}, (data) => {
            if(data.success) {
                this.setState({ acopios : data.acopios });
            }
        }, (err) => {});

        // Get users by role slug
        Services.User.getUsersByRoleSlug('productor', (data) => {
            if(data.success) {
                this.setState({ productors : data.users });
            }
        }, (err) => {});
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });

        let data = this.form.getFieldsValue();
        Services.Auth.register(data, (response) => {
            this.setState({ isSaving : false });
            message.success('El usuario se creó exitosamente');

            this.props.history.push(`/dashboard/admin/users/list`)

            Services.Analytics.trackAudit({ action : `Usuario creado`, screen: 'users', payload: {} });
        }, (err) => {
            message.error('Hubo un problema al crear el usuario. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        });
    }

    render() {

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Crear usuario</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Form
                                        layout={'vertical'}
                                        onFinish={this.onSubmit}
                                        ref={(e) => { this.form = e; }}>
                                        <Form.Item
                                            name={['email']}
                                            rules={[{ required: true, type: 'email' }]}
                                            label="Email">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name={['password']}
                                            rules={[{ required: true }]}
                                            label="Password">
                                            <Input
                                                type={'password'}
                                            />
                                        </Form.Item>
                                        <Form.Item
                                            name={['role_id']}
                                            rules={[{ required: true }]}
                                            label={'Rol de usuario'}>
                                            <Select
                                                showSearch
                                                style={{ width: '100%' }}
                                                placeholder="Toca para escoger un rol"
                                                optionFilterProp="children"
                                                onChange={(e) => {
                                                    this.setState({ roleSelected : e });
                                                }}
                                                filterOption={(input, option) =>
                                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                }>
                                                <Option value={2}>Distribuidor</Option>
                                                <Option value={3}>Productor</Option>
                                                <Option value={4}>Asesor de productores</Option>
                                            </Select>
                                        </Form.Item>

                                        { this.state.roleSelected == 2 && this.state.acopios != null && this.state.acopios.length > 0 &&
                                            <Form.Item
                                                name={['acopio_ids']}
                                                rules={[{ required: true }]}
                                                label={'Asociar a acopios'}>
                                                <Select
                                                    showSearch
                                                    style={{ width: '100%' }}
                                                    placeholder="Toca para escoger acopios"
                                                    optionFilterProp="children"
                                                    mode={'multiple'}
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }>
                                                    { this.state.acopios.map((e) => {
                                                        return (
                                                            <Option value={e.id}>{e.name}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Form.Item>
                                        }

                                        { this.state.roleSelected == 4 && this.state.productors != null && this.state.productors.length > 0 &&
                                            <Form.Item
                                                name={['productor_ids']}
                                                rules={[{ required: true }]}
                                                label={'Ver los siguientes productores'}>
                                                <Select
                                                    showSearch
                                                    style={{ width: '100%' }}
                                                    placeholder="Toca para escoger productores"
                                                    optionFilterProp="children"
                                                    mode={'multiple'}
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }>
                                                    { this.state.productors.map((e) => {
                                                        return (
                                                            <Option value={e.id}>{e.email}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Form.Item>
                                        }

                                        <Form.Item>
                                            <Button
                                                loading={this.state.isSaving}
                                                type="primary"
                                                className="btn-cta"
                                                htmlType="submit">
                                                Guardar
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
