import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, Select, message } from 'antd';
import QueueAnim from 'rc-queue-anim';
import _ from 'lodash';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            user: {},
            acopios: [],
            productors: []
        };

        this.form = null;
    }

    componentDidMount() {
        const userId = this.props.match.params.id;
        this.setState({ isLoading : true });
        Services.User.getById(userId, (data) => {
            if(data.success) {
                this.form.setFieldsValue({
                    ...data.user,
                    acopio_ids: _.map(data.user.acopios, (i) => i.id)
                })
                this.setState({ user : data.user });

                if(data.user.role_slug == 'distributor') {
                    // Get acopios
                    Services.Acopio.getAcopios({}, (data) => {
                        if(data.success) {
                            this.setState({ acopios : data.acopios });
                        }
                    }, (err) => {});
                } else if(data.user.role_slug == 'asesor_productores') {
                    // Get users by role slug
                    Services.User.getUsersByRoleSlug('productor', (data) => {
                        if(data.success) {
                            this.setState({ productors : data.users });
                        }
                    }, (err) => {});
                }
            }
            this.setState({ isLoading : false });
        }, (err) => {
            this.setState({ isLoading : false });
        });
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });
        const userId = this.props.match.params.id;

        let data = this.form.getFieldsValue();
        Services.User.updateById(userId, data, (response) => {
            this.setState({ isSaving : false });
            message.success('El usuario se editó exitosamente');

            this.props.history.push(`/dashboard/admin/users/list`)
            Services.Analytics.trackAudit({ action : `Usuario editado`, screen: 'users', payload: {} });
        }, (err) => {
            message.error('Hubo un problema al editar el usuario. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        });
    }

    render() {

        const { user } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Editar usuario</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Form
                                        layout={'vertical'}
                                        onFinish={this.onSubmit}
                                        ref={(e) => { this.form = e; }}>
                                        <Form.Item
                                            name={['email']}
                                            rules={[{ required: true, type: 'email' }]}
                                            label="Email">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name={['profile', ['name']]}
                                            rules={[{ required: false }]}
                                            label={'Nombre'}>
                                            <Input />
                                        </Form.Item>
                                        { user.role_slug == 'productor' &&
                                            <Form.Item
                                                name={'neo4j_key'}
                                                rules={[{ required: false }]}
                                                label={'Neo4j Key AB Secundario'}>
                                                <Input />
                                            </Form.Item>
                                        }
                                        <Form.Item
                                            name={['profile', ['whatsapp_number']]}
                                            rules={[{ required: false }]}
                                            label={'Número de whatsapp'}>
                                            <Input />
                                        </Form.Item>
                                        { this.state.acopios != null && this.state.acopios.length > 0 &&
                                            <Form.Item
                                                name={['acopio_ids']}
                                                rules={[{ required: true }]}
                                                label={'Asociar a acopios'}>
                                                <Select
                                                    showSearch
                                                    style={{ width: '100%' }}
                                                    placeholder="Toca para escoger acopios"
                                                    optionFilterProp="children"
                                                    mode={'multiple'}
                                                    defaultValue={user && user.acopios && user.acopios.length > 0 ? _.map(user.acopios, (e) => e.id) : null}
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }>
                                                    { this.state.acopios.map((e) => {
                                                        return (
                                                            <Option value={e.id}>{e.name}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Form.Item>
                                        }

                                        { this.state.productors != null && this.state.productors.length > 0 &&
                                            <Form.Item
                                                name={['productor_ids']}
                                                rules={[{ required: true }]}
                                                label={'Asociar a productores'}>
                                                <Select
                                                    showSearch
                                                    style={{ width: '100%' }}
                                                    placeholder="Toca para escoger productores"
                                                    optionFilterProp="children"
                                                    mode={'multiple'}
                                                    defaultValue={user && user.productors && user.productors.length > 0 ? _.map(user.productors, (e) => e.id) : null}
                                                    filterOption={(input, option) =>
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }>
                                                    { this.state.productors.map((e) => {
                                                        return (
                                                            <Option value={e.id}>{e.email}</Option>
                                                        );
                                                    })}
                                                </Select>
                                            </Form.Item>
                                        }
                                        <Form.Item>
                                            <Button
                                                loading={this.state.isSaving}
                                                type="primary"
                                                className="btn-cta"
                                                htmlType="submit">
                                                Guardar
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
