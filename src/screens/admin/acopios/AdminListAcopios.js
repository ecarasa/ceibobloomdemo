import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, message, Table } from 'antd';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const TABLE_COLUMNS = [{
    title: 'Nombre',
    dataIndex: 'name',
    render: (text, record) => {
        return (
            <a href={`/#/dashboard/admin/acopios/${record.id}/edit`}>{ text }</a>
        );
    }
}, {
    title: 'NEO4J Key',
    dataIndex: 'neo4j_key'
}];

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            data: [],
            pagination: {},
            loading: false,
        };

        this.form = null;
    }

    handleTableChange = (pagination, filters, sorter) => {
        const pager = { ...this.state.pagination };
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
        this.fetch({
            results: pagination.pageSize,
            page: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            ...filters,
        });
    }

    fetch = (params = {}) => {
        this.setState({ isLoading: true });
        Services.Acopio.getAcopios(params, (data) => {
            if(data.success) {
                this.setState({ data: data.acopios, isLoading: false });
            } else {
                this.setState({ isLoading: false });
            }
        }, (err) => {
            this.setState({ isLoading: false });
        });
    }

    componentDidMount() {
        this.fetch();
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });

        let data = this.form.getFieldsValue();
        Services.User.updateMe(data, (response) => {
            this.setState({ isSaving : false });
            this.props.stores.user.refresh();
            message.success('Tu perfil fue modificado con éxito');
        }, (err) => {
            message.error('Hubo un problema al modificar tu perfil. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        })
    }

    render() {

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Acopios</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Table
                                        columns={TABLE_COLUMNS}
                                        rowKey={record => record.id}
                                        dataSource={this.state.data}
                                        pagination={this.state.pagination}
                                        loading={this.state.isLoading}
                                        onChange={this.handleTableChange}
                                    />
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
