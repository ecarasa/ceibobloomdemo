import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, Select, message } from 'antd';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            user: {},
        };

        this.form = null;
    }

    componentDidMount() {

    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });

        let data = this.form.getFieldsValue();
        Services.Acopio.create(data, (response) => {
            this.setState({ isSaving : false });
            message.success('El acopio se creó exitosamente');

            Services.Analytics.trackAudit({ action : `Acopio creado`, screen: 'acopios', payload: {} });
        }, (err) => {
            message.error('Hubo un problema al crear el acopio. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        });
    }

    render() {

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Crear acopio</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Form
                                        layout={'vertical'}
                                        onFinish={this.onSubmit}
                                        ref={(e) => { this.form = e; }}>
                                        <Form.Item
                                            name={['name']}
                                            rules={[{ required: true }]}
                                            label="Nombre">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name={['neo4j_key']}
                                            rules={[{ required: false }]}
                                            label="Key de neo4j">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item>
                                            <Button
                                                loading={this.state.isSaving}
                                                type="primary"
                                                className="btn-cta"
                                                htmlType="submit">
                                                Crear
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
