import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, Select, message } from 'antd';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

const { Option } = Select;

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            acopio: {},
        };

        this.form = null;
    }

    componentDidMount() {
        const acopioId = this.props.match.params.id;
        this.setState({ isLoading : true });
        Services.Acopio.getById(acopioId, (data) => {
            if(data.success) {
                this.form.setFieldsValue(data.acopio)
                this.setState({ acopio : data.acopio });
            }
            this.setState({ isLoading : false });
        }, (err) => {
            this.setState({ isLoading : false });
        });
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });
        const acopioId = this.props.match.params.id;

        let data = this.form.getFieldsValue();
        Services.Acopio.updateById(acopioId, data, (response) => {
            this.setState({ isSaving : false });
            message.success('El acopio se editó exitosamente');

            Services.Analytics.trackAudit({ action : `Acopio editado`, screen: 'acopios', payload: {} });
        }, (err) => {
            message.error('Hubo un problema al editar el acopio. Inténtalo nuevamente');
            this.setState({ isSaving : false });
        });
    }

    render() {

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Editar usuario</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Form
                                        layout={'vertical'}
                                        onFinish={this.onSubmit}
                                        ref={(e) => { this.form = e; }}>
                                        <Form.Item
                                            name={['name']}
                                            rules={[{ required: true }]}
                                            label="Nombre">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name={['neo4j_key']}
                                            rules={[{ required: false }]}
                                            label="Key de neo4j">
                                            <Input />
                                        </Form.Item>
                                        <Form.Item>
                                            <Button
                                                loading={this.state.isSaving}
                                                type="primary"
                                                className="btn-cta"
                                                htmlType="submit">
                                                Guardar
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);
