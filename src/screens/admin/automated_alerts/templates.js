import React from 'react';
import '@ant-design/compatible/assets/index.css';
import { Form, List, Avatar, Tag, Empty, Spin, Button, Input, message, Table, Alert, Checkbox } from 'antd';
import QueueAnim from 'rc-queue-anim';

// Components
import ContentGeoReferencedCard from 'components/UPL/content_geo/ContentGeoReferencedCard';

import DEMO from 'constants/demoData';
import * as Services from 'services';
import moment from 'moment';
import withContext from 'context/withContext';

class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSaving: false,
            data: [],
            pagination: {},
            loading: false,
            columns: []
        };

        this.form = null;
    }

    componentDidMount() {
        this.setState({ columns: this.getColumns() });
        this.getTemplates();
    }

    getTemplates = () => {
        this.setState({ isLoading : true });
        Services.AutomatedAlert.getTemplates((data) => {
            if(data.success) {
                this.setState({ data : data.templates });
            }

            this.setState({ isLoading : false });

            Services.Analytics.trackAudit({ action : `Ver alertas`, screen: 'alerts', payload: {} });
        }, (err) => {
            // Do nothing
            this.setState({ isLoading : false });
        });
    }

    getColumns = () => {
        return [{
            title: '#',
            dataIndex: 'id'
        }, {
            title: 'Template',
            dataIndex: 'template'
        }, {
            title: 'Periodicidad',
            dataIndex: 'periodicity',
            render: (text, record, index) => {
                return (
                    <Input
                        name={'periodicity'}
                        value={record.periodicity}
                        onChange={(e) => {
                            const { value } = e.target;
                            let templates = this.state.data;
                            templates[index].periodicity = value;
                            this.setState({ templates });
                            // let template = this.state.data.indexOf(record)
                        }}
                    />
                );
            }
        }, {
            title: 'Habilitada',
            dataIndex: 'enabled',
            render: (text, record, index) => {
                return (
                    <Checkbox
                        name={'enabled'}
                        checked={record.enabled}
                        onChange={(e) => {
                            const { checked } = e.target;
                            let templates = this.state.data;
                            templates[index].enabled = checked;
                            this.setState({ templates });
                            // let template = this.state.data.indexOf(record)
                        }}
                    />
                );
            }
        }, {
            title: 'Próxima ejecución',
            dataIndex: 'next_ejecution',
            render: (text, record, index) => {
                let nextExecution = null;
                if(record.periodicity != null) {
                    nextExecution = moment(record.next_execution).format('DD/MM/YYYY [a las] HH:mm');
                }

                return (
                    <p style={{ opacity: record.enabled ? 1 : .4, fontWeight: 500, backgroundColor: 'transparent', color: '#8e519a', padding: 5 }}>{ nextExecution != null ? nextExecution : '-' }</p>
                );
            }
        }]
    }

    onSubmit = (values) => {
        this.setState({ isSaving : true });

        const { data } = this.state;
        Services.AutomatedAlert.updateTemplates({ templates : data }, (response) => {
            this.setState({ isSaving : false });
            message.success('Se modificaron las alertas automáticas');

            this.getTemplates();

            Services.Analytics.trackAudit({ action : `Cambios en alertas`, screen: 'alerts', payload: {} });
        }, (err) => {
            message.error('Hubo un problema al modificar las alertas automáticas.');
            this.setState({ isSaving : false });
        })
    }

    render() {

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Acopios</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <Alert
                                        message={
                                            <p>Genera la periodicidad desde <a href="https://crontab.cronhub.io/" target="_blank">https://crontab.cronhub.io/</a></p>
                                        }
                                        type="info"
                                        showIcon
                                    />

                                    <Table
                                        size={'small'}
                                        style={{ marginTop: 10 }}
                                        columns={this.state.columns}
                                        rowKey={record => record.id}
                                        dataSource={this.state.data}
                                        pagination={this.state.pagination}
                                        loading={this.state.isLoading}
                                        onChange={this.handleTableChange}
                                    />

                                    <Button
                                        loading={this.state.isSaving}
                                        type="primary"
                                        onClick={this.onSubmit}
                                        className="btn-cta"
                                        htmlType="submit">
                                        Guardar
                                    </Button>
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default withContext(Section);