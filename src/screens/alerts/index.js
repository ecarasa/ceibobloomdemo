import React from 'react';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { List, Button, Avatar, Tag, Empty, Spin } from 'antd';
import DEMO from 'constants/demoData';
import QueueAnim from 'rc-queue-anim';

import { observer } from 'mobx-react';
import { CheckOutlined } from '@ant-design/icons';
import * as Services from 'services'
import moment from 'moment';
import withContext from 'context/withContext';

@observer
class Section extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            notifications: [],
            isLoading: true
        };
    }

    componentDidMount() {
        this.getNotifications()
    }

    getNotifications = () => {
        Services.Notification.getMyNotifications((data) => {
            if(data.success) {
                this.setState({ notifications: data.notifications, isLoading: false });
            }
        }, (err) => {
            this.setState({ isLoading: false });
        });
    }

    render() {
        const { notifications } = this.state;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Mis alertas</h2>
                        <div className="row">
                            <div className="col-12">
                                <Spin spinning={this.state.isLoading}>
                                    <ListAlerts
                                        notifications={notifications}
                                        onSeen={(notification, index) => {
                                            notifications[index].loading = true;
                                            this.setState({ notifications });

                                            Services.Notification.markAsRead(notification.id, (data) => {
                                                notifications[index].seen = true;
                                                notifications[index].loading = false;
                                                let user = this.props.stores.user.getUser();
                                                user.unread_notifications -= 1;
                                                this.props.stores.user.setUser(user);
                                                this.setState({ notifications });

                                                Services.Analytics.trackAudit({ action : `Marcar como leida`, screen: 'alerts', payload: {} });
                                            });
                                        }}
                                    />
                                </Spin>
                            </div>
                        </div>
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

const ListAlerts = (props) => {
    if(!props.notifications || !props.notifications.length) {
        return (
            <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={'Aún no tenés alertas'}
            />
        );
    }

    return (
        <div className="box box-default">
            <div className="box-body">
                <List
                    itemLayout="horizontal"
                    dataSource={props.notifications}
                    renderItem={(item, index) => (
                        <List.Item>
                            <div className="list-style-v1">
                                <div className="list-item">
                                    {  <div
                                        className={`icon-btn icon-btn-round mr-3 ${item.seen ? 'bg-info' : 'bg-success'} text-body-reverse`}>
                                        <LegacyIcon type={'mail'} style={{ color: 'white' }} />
                                    </div>  }
                                    <div className="list-item__body">
                                        <div className="list-item__title">{ !item.seen ? 'Nueva notificacion' : 'Notificación' }</div>
                                        <div className="list-item__desc" dangerouslySetInnerHTML={{ __html: item.text_formatted }}></div>
                                        { item.from != null &&
                                            <div className="list-item__datetime">Enviada por { item.from.email } { item.to && item.to.email ? `a ${item.to.email}` : null}</div>
                                        }
                                        <div className="list-item__datetime">
                                            {moment(item.created_at).fromNow()}
                                        </div>
                                    </div>

                                    { !item.seen &&
                                        <div style={{ textAlign: 'center' }}>
                                            <Button
                                                type="primary"
                                                style={{ backgroundColor: '#52c41a', borderColor: '#52c41a', margin: '10px auto' }}
                                                onClick={() => props.onSeen(item, index)}
                                                loading={item.loading}
                                                icon={<CheckOutlined color={'white'} />}>Marcar como leída</Button>
                                        </div>
                                    }
                                </div>
                            </div>
                        </List.Item>
                    )}
                />
            </div>
        </div>
    );
};

export default withContext(Section);