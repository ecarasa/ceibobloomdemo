import { observable, action, computed, intercept } from 'mobx'
import * as Services from 'services/'

let index = 0

class ObservableListStore {
    @observable user = {
        profile: {}
    }

    isGuest() {
        return this.user.id == null;
    }

    setUser(user) {
        this.user = user;
    }

    getUser() {
        return this.user;
    }

    @action
    logout() {
        this.user = {
            profile: {}
        }
    }

    @action
    refresh() {
        Services.User.getMe((data) => {
            if(data.success)
                this.user = data.user;
        }, (err) => {
            console.log("Error while refreshing the user", err);
        });
    }
}


const observableListStore = new ObservableListStore()
export default observableListStore