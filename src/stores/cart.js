import { observable, action, computed, intercept } from 'mobx'
import * as Services from 'services/'

class ObservableListStore {
    @observable cart = {
        products: []
    }

    setCart(cart) {
        this.cart = cart;
    }

    getCart() {
        return this.cart;
    }
}


const observableListStore = new ObservableListStore()
export default observableListStore