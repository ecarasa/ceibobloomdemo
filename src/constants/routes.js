const ROUTES = {
    login: '/auth/login',
    home: '/app/dashboard',
    home_productor: '/dashboard/home',
    home_distributor: '/dashboard/home',
    home_administrator: '/dashboard/admin'
}

export default ROUTES;