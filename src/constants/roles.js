const ROLES = {
    administrator: 1,
    distributor: 2,
    productor: 3,
    administrator_slug: 'administrator',
    distributor_slug: 'distributor',
    productor_slug: 'productor'
}

export default ROLES;