import React, { Component } from 'react';

// Create the context
const Context = React.createContext();

export default Context;