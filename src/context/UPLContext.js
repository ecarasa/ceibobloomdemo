import React, { Component } from "react";
import PropTypes from "prop-types";
import { message as antdMessage } from "antd";
import Context from './Context';

import * as Services from 'services/'

// Stores
import userStore from 'stores/user';
import cartStore from 'stores/cart';

class UPLContext extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null
        };
    }

    render() {
        const {
            props: { children },
        } = this;

        return (
            <Context.Provider
                value={{
                    history: this.props.history,
                    stores: {
                        user: userStore,
                        cart: cartStore
                    }
                }}>
                {children}
            </Context.Provider>
        );
    }
}

UPLContext.propTypes = {
    children: PropTypes.node.isRequired
};

export default UPLContext