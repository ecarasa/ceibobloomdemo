import React from "react";
import Context from "./Context";

export default WrappedComponent => {
    const hocComponent = ({ ...props }) => (
        <Context.Consumer>
            {propsContext => <WrappedComponent {...props} {...propsContext} />}
        </Context.Consumer>
    );

    hocComponent.getInitialProps = async ctx => {

        let props = null;
        if(WrappedComponent.getInitialProps !== undefined) {
            props = await WrappedComponent.getInitialProps(ctx);
        }

        const commonInitialProps = {
            env: {
                BASE_URL: process.env.BASE_URL
            }
        };

        return WrappedComponent.getInitialProps !== undefined
          ? {
                ...props,
                ...commonInitialProps
            }
          : {
            ...commonInitialProps
          };
    };

    return hocComponent;
};