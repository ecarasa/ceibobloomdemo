import { ApiService, normalizePromise } from './BaseService.js';

var AutomatedAlert = {
    getTemplates: (success, error) => {
        let url = 'automated_alerts/templates';
        return normalizePromise(ApiService().get(url), success, error);
    },
    updateTemplates: (data, success, error) => {
        let url = 'automated_alerts/templates';
        return normalizePromise(ApiService().post(url, data), success, error);
    }
};

export default AutomatedAlert;