import { ApiService, normalizePromise } from './BaseService.js';

var Session = {
    updateCurrentAcopio: (acopioId, success, error) => {
        let url = 'session/current_acopio';
        return normalizePromise(ApiService().post(url, { acopio_id: acopioId }), success, error);
    }
};

export default Session;