import { ApiService, normalizePromise } from './BaseService.js';

var Acopio = {
    getAcopios: (filters, success, error) => {
        let url = 'acopios/filter';
        return normalizePromise(ApiService().post(url, filters), success, error);
    },
    create: (data, success, error) => {
        let url = 'acopios';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    getById: (id, success, error) => {
        let url = `acopios/${id}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    updateById: (id, data, success, error) => {
        let url = `acopios/${id}`;
        return normalizePromise(ApiService().post(url, data), success, error);
    }
};

export default Acopio;