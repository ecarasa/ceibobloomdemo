import { ApiService, normalizePromise } from './BaseService.js';

var Auth = {
    isAuthenticated: () => {
        return Auth.user != null
    },
    requestForgotPassword: (data, success, error) => {
        let url = 'auth/forgot_password/request';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    postForgotPassword: (data, success, error) => {
        let url = 'auth/forgot_password/post';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    register: (data, success, error) => {
        let url = 'auth/register';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    login: (data, success, error) => {
        let url = 'auth/login';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    logout: (data, success, error) => {
        let url = 'auth/logout';
        return normalizePromise(ApiService().post(url, data), success, error);
    }
};

export default Auth;