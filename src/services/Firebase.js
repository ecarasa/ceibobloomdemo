import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

var FirebaseService = {
    initialized: false,
    initialize: () => {
        if(!FirebaseService.initialized) {
            const config = {
                apiKey: "AIzaSyD9gjiMoA3a2pv-hJSfR5ftg52N4R8iyPQ",
                authDomain: "ceibo-b3867.firebaseapp.com",
                databaseURL: "https://ceibo-b3867.firebaseio.com",
                projectId: "ceibo-b3867",
                storageBucket: "ceibo-b3867.appspot.com",
                messagingSenderId: "264309446288",
                appId: "1:264309446288:web:7f66430e8f4b16b103aa15"
            };

            firebase.initializeApp(config);

            FirebaseService.initialized = true;
        }
    },
    services: () => {
        return firebase
    },
    media: {
        upload: (file, success, error, options = {}) => {
            // Upload image
            const storage = FirebaseService.services().storage();
            const fileName = new Date().getTime();
            const storageRef = storage.ref(`${options.container}/${fileName}.jpg`)
            const task = storageRef.put(file)
            task.on('state_changed', (snapshot) => {
                // Se lanza durante el progreso de subida
                if(options && options.onProgress)
                    options.onProgress(snapshot);
            }, (err) => {
                if(error)
                    error(err);
            }, (data) => {
                if(success){
                    task.snapshot.ref.getDownloadURL().then((data) => {
                        success({
                            downloadURL: data
                        });
                    });
                }
            });
        }
    }
}

FirebaseService.initialize();

export default FirebaseService;