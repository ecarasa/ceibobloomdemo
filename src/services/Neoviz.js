import { ApiService, normalizePromise } from './BaseService.js';

var Neoviz = {
    getConfig: (data, success, error) => {
        let url = 'home/neoviz/config';
        return normalizePromise(ApiService().post(url, data), success, error);
    }
};

export default Neoviz;