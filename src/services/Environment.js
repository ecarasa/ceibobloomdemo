import { ApiService, normalizePromise } from './BaseService.js';
import SYR from 'strings/syr.json';
import _ from 'lodash';

var Environment = {
    isSYR: () => {
        return window.CONFIG.STR_ENVIRONMENT == 'SYR';
    },
    getString: (string, replaces = []) => {
        // if(Environment.isSYR() && SYR[string] != null)
        let strReplace = string;
        if(Environment.isSYR() && _.some(Object.keys(SYR), (i) => i.toLowerCase() == string.toLowerCase())){
            const key = _.find(Object.keys(SYR), (i) => i.toLowerCase() == string.toLowerCase());
            strReplace = SYR[key];
        }

        // If there are replaces
        if(replaces && replaces.length > 0 && strReplace.indexOf('%s') > -1) {
            strReplace.format(...replaces);
        }

        return strReplace;
    }
};

export default Environment;