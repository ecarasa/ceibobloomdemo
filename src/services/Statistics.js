import { ApiService, normalizePromise } from './BaseService.js';

var Statistics = {
    getHomeIndicators: (success, error) => {
        let url = 'home/indicators';
        return normalizePromise(ApiService().get(url), success, error);
    },
    getHomeIndicatorsWithData: (data, success, error) => {
        let url = 'home/indicators';
        if(data.acopioId != null)
            url += `?acopioId=${data.acopioId}`;
        else if(data.productorId != null)
            url += `?productorId=${data.productorId}`;

        return normalizePromise(ApiService().get(url), success, error);
    },
    getHistoricoReal: (data, success, error) => {
        let url = 'home/historical_vs_real';
        if(data.acopioId != null)
            url += `?acopioId=${data.acopioId}`;
        else if(data.productorId != null)
            url += `?productorId=${data.productorId}`;
        else if(data.merge != null)
            url += `?merge=true&acopio_1=${data.merge.acopio_id_1}&acopio_2=${data.merge.acopio_id_2}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getStackGraphicData: (data, success, error) => {
        let url = 'home/stack_graph_data';
        if(data.acopioId != null)
            url += `?acopioId=${data.acopioId}`;
        return normalizePromise(ApiService().get(url), success, error);
    }
};

export default Statistics;