import { ApiService, normalizePromise } from './BaseService.js';

var Notification = {
    getMyNotifications: (success, error) => {
        let url = 'notifications/me';
        return normalizePromise(ApiService().get(url), success, error);
    },
    getNotificationsCreatedByMe: (success, error) => {
        let url = 'notifications/created_by_me';
        return normalizePromise(ApiService().get(url), success, error);
    },
    create: (data, success, error) => {
        let url = 'notifications';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    markAsRead: (id, success, error) => {
        let url = `notifications/${id}/read`;
        return normalizePromise(ApiService().post(url), success, error);
    }
};

export default Notification;