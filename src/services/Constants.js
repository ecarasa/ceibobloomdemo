const Errors = {
    General: 'Surgió un error al realizar esta acción. Inténtalo nuevamente'
};

export default {
    Errors: Errors
};