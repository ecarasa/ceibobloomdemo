import { ApiService, normalizePromise } from './BaseService.js';

var ContentGeo = {
    create: (data, success, error) => {
        let url = 'content_geo';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    toggleEnable: (id, success, error) => {
        let url = 'content_geo/' + id + '/toggle_enable';
        return normalizePromise(ApiService().post(url), success, error);
    },
    getContentCreatedByUserId: (userId, success, error) => {
        let url = `content_geo/owner/${userId}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getContentTargetedForUserId: (userId, success, error) => {
        let url = `content_geo/target/${userId}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getById: (id, success, error) => {
        let url = `content_geo/${id}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    deleteById: (id, success, error) => {
        let url = `content_geo/${id}`;
        return normalizePromise(ApiService().delete(url), success, error);
    }
};

export default ContentGeo;