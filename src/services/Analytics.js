import { ApiService, normalizePromise } from './BaseService.js';

var Analytics = {
    trackAudit: (data, success, error) => {
        let url = 'audit_logs';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    getAuditLogs: (filter, success, error) => {
        let url = `audit_logs`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getAuditLogsByFilter: (filter, success, error) => {
        let url = `audit_logs/filter`;
        return normalizePromise(ApiService().post(url, filter), success, error);
    },
    getAuditLogsFilters: (success, error) => {
        let url = `audit_logs/filters`;
        return normalizePromise(ApiService().get(url), success, error);
    }
};

export default Analytics;