import { ApiService, normalizePromise } from './BaseService.js';

var Product = {
    create: (data, success, error) => {
        let url = 'products';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    toggleEnable: (id, success, error) => {
        let url = 'products/' + id + '/toggle_enable';
        return normalizePromise(ApiService().post(url), success, error);
    },
    getProductsCreatedByUserId: (userId, success, error) => {
        let url = `products/owner/${userId}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getProductsTargetedForUserId: (userId, success, error) => {
        let url = `products/target/${userId}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getById: (id, success, error) => {
        let url = `products/${id}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    deleteById: (id, success, error) => {
        let url = `products/${id}`;
        return normalizePromise(ApiService().delete(url), success, error);
    }
};

export default Product;