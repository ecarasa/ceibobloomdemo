import { ApiService, normalizePromise } from './BaseService.js';

var Client = {
    getClientsByFilter: (filters, success, error) => {
        let url = 'clients/filter';
        return normalizePromise(ApiService().post(url, filters), success, error);
    },
    getIndicators: (type, success, error) => {
        let url = `clients/indicators?tipo=${type}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getClientById: (id, success, error) => {
        let url = `clients/f12/${id}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    isClientExists: (id, success, error) => {
        let url = `clients/f12/${id}/exists`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getClientByIdAndAcopio: (id, acopioId, success, error) => {
        let url = `clients/f12/${id}?acopioId=${acopioId}`;
        return normalizePromise(ApiService().get(url), success, error);
    }
};

export default Client;