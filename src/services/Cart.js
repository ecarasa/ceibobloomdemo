import { ApiService, normalizePromise } from './BaseService.js';

var Cart = {
    getMyCart: (success, error) => {
        let url = 'carts/user/me';
        return normalizePromise(ApiService().get(url), success, error);
    },
    addItemToCart: (data, success, error) => {
        let url = `carts/${data.id}/items/add`;
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    removeFromCart: (cartId, productId, success, error) => {
        let url = `carts/${cartId}/items/${productId}/remove`;
        return normalizePromise(ApiService().post(url, {}), success, error);
    },
    checkoutCart: (cartId, success, error) => {
        let url = `carts/${cartId}/checkout`;
        return normalizePromise(ApiService().post(url, {}), success, error);
    }
};

export default Cart;