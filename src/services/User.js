import { ApiService, normalizePromise } from './BaseService.js';

var User = {
    getMe: (success, error) => {
        let url = 'users/me';
        return normalizePromise(ApiService().get(url), success, error);
    },
    updateMe: (data, success, error) => {
        let url = 'users/me';
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    getUsersByRoleSlug: (slug, success, error) => {
        let url = `users/role/${slug}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    getUsers: (filters, success, error) => {
        let url = `users/filter`;
        return normalizePromise(ApiService().post(url, filters), success, error);
    },
    getById: (id, success, error) => {
        let url = `users/${id}`;
        return normalizePromise(ApiService().get(url), success, error);
    },
    existsByABSecundario: (abSecundario, success, error) => {
        let url = `users/check_exists`;
        return normalizePromise(ApiService().post(url, { ab_secundario: abSecundario }), success, error);
    },
    updateById: (id, data, success, error) => {
        let url = `users/${id}`;
        return normalizePromise(ApiService().post(url, data), success, error);
    },
    deleteById: (id, success, error) => {
        let url = `users/${id}`;
        return normalizePromise(ApiService().delete(url), success, error);
    }
};

export default User;