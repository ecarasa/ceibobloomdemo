import React from 'react';
import { SearchOutlined } from '@ant-design/icons';
import { Button } from 'antd';

const Box = () => {
  return (
    <div className="box box-default">
      <div className="box-header">Icon</div>
      <div className="box-body">
        <Button type="primary" shape="circle" icon={<SearchOutlined />} />
        <Button type="primary" icon={<SearchOutlined />}>Search</Button>
        <Button shape="circle" icon={<SearchOutlined />} />
        <Button icon={<SearchOutlined />}>Search</Button>
        <br />
        <Button shape="circle" icon={<SearchOutlined />} />
        <Button icon={<SearchOutlined />}>Search</Button>
        <Button type="dashed" shape="circle" icon={<SearchOutlined />} />
        <Button type="dashed" icon={<SearchOutlined />}>Search</Button>
      </div>
    </div>
  );
}

export default Box;