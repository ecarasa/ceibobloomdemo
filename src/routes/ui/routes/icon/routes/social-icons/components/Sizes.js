import React from 'react';
import DEMO from 'constants/demoData';

import {
  FacebookFilled,
  GoogleOutlined,
  InstagramOutlined,
  TwitterOutlined,
  YoutubeOutlined,
} from '@ant-design/icons';

const Section = () => (
  <div className="box box-default">
    <div className="box-header">Sizes</div>
    <div className="box-body text-center">
      <p>
        <a href={DEMO.link} className="icon-btn icon-btn-round mx-1 icon-btn-sm btn-twitter"><TwitterOutlined /></a>
        <a href={DEMO.link} className="icon-btn icon-btn-round mx-1 btn-facebook"><FacebookFilled /></a>
        <a href={DEMO.link} className="icon-btn icon-btn-round mx-1 icon-btn-md btn-google-plus"><GoogleOutlined /></a>
        <a href={DEMO.link} className="icon-btn icon-btn-round mx-1 icon-btn-lg btn-instagram"><InstagramOutlined /></a>
        <a href={DEMO.link} className="icon-btn icon-btn-round mx-1 icon-btn-xl btn-youtube"><YoutubeOutlined /></a>
      </p>  
    </div>
  </div> 
)

export default Section;
