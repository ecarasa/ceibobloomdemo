import React from 'react';
import DEMO from 'constants/demoData';

import {
  BehanceOutlined,
  DribbbleOutlined,
  FacebookFilled,
  GithubOutlined,
  GoogleOutlined,
  InstagramOutlined,
  LinkedinOutlined,
  MediumOutlined,
  SkypeOutlined,
  SlackOutlined,
  TwitterOutlined,
  WechatOutlined,
  YoutubeOutlined,
} from '@ant-design/icons';

const Section = () => (
  <div className="box box-default">
    <div className="box-header">Styles</div>
    <div className="box-body text-center">
      <p>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-twitter"><TwitterOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-facebook"><FacebookFilled /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-google-plus"><GoogleOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-instagram"><InstagramOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-rss"><MediumOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-wechat"><WechatOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-linkedin"><LinkedinOutlined /></a>
      </p>
      <p>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-youtube"><YoutubeOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-skype"><SkypeOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-dribbble"><DribbbleOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-behance"><BehanceOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-social"><SlackOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-sm btn-github"><GithubOutlined /></a>
      </p> 
      <div className="divider my-4"></div>
      <p>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-twitter"><TwitterOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-facebook"><FacebookFilled /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-google-plus"><GoogleOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-instagram"><InstagramOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-rss"><MediumOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-wechat"><WechatOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-linkedin"><LinkedinOutlined /></a>
      </p>
      <p>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-youtube"><YoutubeOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-skype"><SkypeOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-dribbble"><DribbbleOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-behance"><BehanceOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-social"><SlackOutlined /></a>
        <a href={DEMO.link} className="icon-btn mx-1 icon-btn-round icon-btn-sm btn-github"><GithubOutlined /></a>
      </p>     
    </div>
  </div> 
)

export default Section;
