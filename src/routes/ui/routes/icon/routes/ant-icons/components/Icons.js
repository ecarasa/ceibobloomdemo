import React from 'react';

import {
  AlipayCircleOutlined,
  AlipayOutlined,
  AliwangwangOutlined,
  AliyunOutlined,
  AmazonOutlined,
  AndroidOutlined,
  AntDesignOutlined,
  ApiOutlined,
  AppleOutlined,
  AppstoreOutlined,
  AreaChartOutlined,
  ArrowDownOutlined,
  ArrowLeftOutlined,
  ArrowRightOutlined,
  ArrowsAltOutlined,
  ArrowUpOutlined,
  BackwardOutlined,
  BankOutlined,
  BarChartOutlined,
  BarcodeOutlined,
  BarsOutlined,
  BehanceOutlined,
  BehanceSquareOutlined,
  BellOutlined,
  BookOutlined,
  BulbOutlined,
  CalculatorOutlined,
  CalendarOutlined,
  CameraOutlined,
  CaretDownOutlined,
  CaretLeftOutlined,
  CaretRightOutlined,
  CaretUpOutlined,
  CarOutlined,
  CheckCircleOutlined,
  CheckOutlined,
  CheckSquareOutlined,
  ChromeOutlined,
  ClockCircleOutlined,
  CloseCircleOutlined,
  CloseOutlined,
  CloseSquareOutlined,
  CloudDownloadOutlined,
  CloudOutlined,
  CloudUploadOutlined,
  CodeOutlined,
  CodepenCircleOutlined,
  CodepenOutlined,
  CoffeeOutlined,
  CompassOutlined,
  ContactsOutlined,
  CopyOutlined,
  CopyrightOutlined,
  CreditCardOutlined,
  CustomerServiceOutlined,
  DashboardOutlined,
  DatabaseOutlined,
  DeleteOutlined,
  DesktopOutlined,
  DingdingOutlined,
  DisconnectOutlined,
  DislikeOutlined,
  DotChartOutlined,
  DoubleLeftOutlined,
  DoubleRightOutlined,
  DownCircleOutlined,
  DownloadOutlined,
  DownOutlined,
  DownSquareOutlined,
  DribbbleOutlined,
  DribbbleSquareOutlined,
  DropboxOutlined,
  EditOutlined,
  EllipsisOutlined,
  EnterOutlined,
  EnvironmentOutlined,
  ExceptionOutlined,
  ExclamationCircleOutlined,
  ExclamationOutlined,
  ExportOutlined,
  EyeOutlined,
  FacebookFilled,
  FastBackwardOutlined,
  FastForwardOutlined,
  FileAddOutlined,
  FileExcelOutlined,
  FileJpgOutlined,
  FileMarkdownOutlined,
  FileOutlined,
  FilePdfOutlined,
  FilePptOutlined,
  FileTextOutlined,
  FileUnknownOutlined,
  FileWordOutlined,
  FilterOutlined,
  FlagOutlined,
  FolderAddOutlined,
  FolderOpenOutlined,
  FolderOutlined,
  ForkOutlined,
  FormOutlined,
  ForwardOutlined,
  FrownOutlined,
  GiftOutlined,
  GithubOutlined,
  GitlabOutlined,
  GlobalOutlined,
  GoogleOutlined,
  GooglePlusOutlined,
  HddOutlined,
  HeartOutlined,
  HomeOutlined,
  HourglassOutlined,
  Html5Outlined,
  IdcardOutlined,
  IeOutlined,
  InboxOutlined,
  InfoCircleOutlined,
  InfoOutlined,
  InstagramOutlined,
  KeyOutlined,
  LaptopOutlined,
  LayoutOutlined,
  LeftCircleOutlined,
  LeftOutlined,
  LeftSquareOutlined,
  LikeOutlined,
  LineChartOutlined,
  LinkedinOutlined,
  LinkOutlined,
  Loading3QuartersOutlined,
  LockOutlined,
  LoginOutlined,
  LogoutOutlined,
  MailOutlined,
  ManOutlined,
  MedicineBoxOutlined,
  MediumOutlined,
  MediumWorkmarkOutlined,
  MehOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  MessageOutlined,
  MinusCircleOutlined,
  MinusOutlined,
  MinusSquareOutlined,
  MobileOutlined,
  NotificationOutlined,
  PaperClipOutlined,
  PauseCircleOutlined,
  PauseOutlined,
  PayCircleOutlined,
  PhoneOutlined,
  PictureOutlined,
  PieChartOutlined,
  PlayCircleOutlined,
  PlusCircleOutlined,
  PlusOutlined,
  PlusSquareOutlined,
  PoweroffOutlined,
  PrinterOutlined,
  ProfileOutlined,
  PushpinOutlined,
  QqOutlined,
  QrcodeOutlined,
  QuestionCircleOutlined,
  QuestionOutlined,
  RedEnvelopeOutlined,
  ReloadOutlined,
  RetweetOutlined,
  RightCircleOutlined,
  RightOutlined,
  RightSquareOutlined,
  RocketOutlined,
  RollbackOutlined,
  SafetyOutlined,
  SaveOutlined,
  ScanOutlined,
  ScheduleOutlined,
  SearchOutlined,
  SelectOutlined,
  SettingOutlined,
  ShakeOutlined,
  ShareAltOutlined,
  ShopOutlined,
  ShoppingCartOutlined,
  ShrinkOutlined,
  SkinOutlined,
  SkypeOutlined,
  SlackOutlined,
  SlackSquareOutlined,
  SmileOutlined,
  SolutionOutlined,
  SoundOutlined,
  StarOutlined,
  StepBackwardOutlined,
  StepForwardOutlined,
  SwapLeftOutlined,
  SwapOutlined,
  SwapRightOutlined,
  SwitcherOutlined,
  SyncOutlined,
  TableOutlined,
  TabletOutlined,
  TagOutlined,
  TagsOutlined,
  TaobaoCircleOutlined,
  TaobaoOutlined,
  TeamOutlined,
  ToolOutlined,
  ToTopOutlined,
  TrademarkOutlined,
  TrophyOutlined,
  TwitterOutlined,
  UnlockOutlined,
  UpCircleOutlined,
  UploadOutlined,
  UpOutlined,
  UpSquareOutlined,
  UsbOutlined,
  UserAddOutlined,
  UserDeleteOutlined,
  UsergroupAddOutlined,
  UsergroupDeleteOutlined,
  UserOutlined,
  VideoCameraOutlined,
  WalletOutlined,
  WarningOutlined,
  WechatOutlined,
  WeiboCircleOutlined,
  WeiboOutlined,
  WeiboSquareOutlined,
  WifiOutlined,
  WindowsOutlined,
  WomanOutlined,
  YoutubeOutlined,
  YuqueOutlined,
  ZhihuOutlined,
} from '@ant-design/icons';

import { Icon as LegacyIcon } from '@ant-design/compatible';
import './demo.scss';


const Section = () => {
  return (
    <section>

<h3 id="Directional-Icons"><span>Directional Icons</span></h3>
<ul className="anticons-list clearfix icons">
<li className=""><StepBackwardOutlined /><span className="anticon-class"><span className="ant-badge">step-backward</span></span></li>
<li className=""><StepForwardOutlined /><span className="anticon-class"><span className="ant-badge">step-forward</span></span></li>
<li className=""><FastBackwardOutlined /><span className="anticon-class"><span className="ant-badge">fast-backward</span></span></li>
<li className=""><FastForwardOutlined /><span className="anticon-class"><span className="ant-badge">fast-forward</span></span></li>
<li className=""><ShrinkOutlined /><span className="anticon-class"><span className="ant-badge">shrink</span></span></li>
<li className=""><ArrowsAltOutlined /><span className="anticon-class"><span className="ant-badge">arrows-alt</span></span></li>
<li className=""><DownOutlined /><span className="anticon-class"><span className="ant-badge">down</span></span></li>
<li className=""><UpOutlined /><span className="anticon-class"><span className="ant-badge">up</span></span></li>
<li className=""><LeftOutlined /><span className="anticon-class"><span className="ant-badge">left</span></span></li>
<li className=""><RightOutlined /><span className="anticon-class"><span className="ant-badge">right</span></span></li>
<li className=""><CaretUpOutlined /><span className="anticon-class"><span className="ant-badge">caret-up</span></span></li>
<li className=""><CaretDownOutlined /><span className="anticon-class"><span className="ant-badge">caret-down</span></span></li>
<li className=""><CaretLeftOutlined /><span className="anticon-class"><span className="ant-badge">caret-left</span></span></li>
<li className=""><CaretRightOutlined /><span className="anticon-class"><span className="ant-badge">caret-right</span></span></li>
<li className=""><UpCircleOutlined /><span className="anticon-class"><span className="ant-badge">up-circle</span></span></li>
<li className=""><DownCircleOutlined /><span className="anticon-class"><span className="ant-badge">down-circle</span></span></li>
<li className=""><LeftCircleOutlined /><span className="anticon-class"><span className="ant-badge">left-circle</span></span></li>
<li className=""><RightCircleOutlined /><span className="anticon-class"><span className="ant-badge">right-circle</span></span></li>
<li className=""><UpCircleOutlined /><span className="anticon-class"><span className="ant-badge">up-circle-o</span></span></li>
<li className=""><DownCircleOutlined /><span className="anticon-class"><span className="ant-badge">down-circle-o</span></span></li>
<li className=""><RightCircleOutlined /><span className="anticon-class"><span className="ant-badge">right-circle-o</span></span></li>
<li className=""><LeftCircleOutlined /><span className="anticon-class"><span className="ant-badge">left-circle-o</span></span></li>
<li className=""><DoubleRightOutlined /><span className="anticon-class"><span className="ant-badge">double-right</span></span></li>
<li className=""><DoubleLeftOutlined /><span className="anticon-class"><span className="ant-badge">double-left</span></span></li>
<li className=""><LegacyIcon type="verticle-left" /><span className="anticon-class"><span className="ant-badge">verticle-left</span></span></li>
<li className=""><LegacyIcon type="verticle-right" /><span className="anticon-class"><span className="ant-badge">verticle-right</span></span></li>
<li className=""><ForwardOutlined /><span className="anticon-class"><span className="ant-badge">forward</span></span></li>
<li className=""><BackwardOutlined /><span className="anticon-class"><span className="ant-badge">backward</span></span></li>
<li className=""><RollbackOutlined /><span className="anticon-class"><span className="ant-badge">rollback</span></span></li>
<li className=""><EnterOutlined /><span className="anticon-class"><span className="ant-badge">enter</span></span></li>
<li className=""><RetweetOutlined /><span className="anticon-class"><span className="ant-badge">retweet</span></span></li>
<li className=""><SwapOutlined /><span className="anticon-class"><span className="ant-badge">swap</span></span></li>
<li className=""><SwapLeftOutlined /><span className="anticon-class"><span className="ant-badge">swap-left</span></span></li>
<li className=""><SwapRightOutlined /><span className="anticon-class"><span className="ant-badge">swap-right</span></span></li>
<li className=""><ArrowUpOutlined /><span className="anticon-class"><span className="ant-badge">arrow-up</span></span></li>
<li className=""><ArrowDownOutlined /><span className="anticon-class"><span className="ant-badge">arrow-down</span></span></li>
<li className=""><ArrowLeftOutlined /><span className="anticon-class"><span className="ant-badge">arrow-left</span></span></li>
<li className=""><ArrowRightOutlined /><span className="anticon-class"><span className="ant-badge">arrow-right</span></span></li>
<li className=""><PlayCircleOutlined /><span className="anticon-class"><span className="ant-badge">play-circle</span></span></li>
<li className=""><PlayCircleOutlined /><span className="anticon-class"><span className="ant-badge">play-circle-o</span></span></li>
<li className=""><UpSquareOutlined /><span className="anticon-class"><span className="ant-badge">up-square</span></span></li>
<li className=""><DownSquareOutlined /><span className="anticon-class"><span className="ant-badge">down-square</span></span></li>
<li className=""><LeftSquareOutlined /><span className="anticon-class"><span className="ant-badge">left-square</span></span></li>
<li className=""><RightSquareOutlined /><span className="anticon-class"><span className="ant-badge">right-square</span></span></li>
<li className=""><UpSquareOutlined /><span className="anticon-class"><span className="ant-badge">up-square-o</span></span></li>
<li className=""><DownSquareOutlined /><span className="anticon-class"><span className="ant-badge">down-square-o</span></span></li>
<li className=""><LeftSquareOutlined /><span className="anticon-class"><span className="ant-badge">left-square-o</span></span></li>
<li className=""><RightSquareOutlined /><span className="anticon-class"><span className="ant-badge">right-square-o</span></span></li>
<li className=""><LoginOutlined /><span className="anticon-class"><span className="ant-badge">login</span></span></li>
<li className=""><LogoutOutlined /><span className="anticon-class"><span className="ant-badge">logout</span></span></li>
<li className=""><MenuFoldOutlined /><span className="anticon-class"><span className="ant-badge">menu-fold</span></span></li>
<li className=""><MenuUnfoldOutlined /><span className="anticon-class"><span className="ant-badge">menu-unfold</span></span></li>
</ul>

<h3 id="Suggested-Icons"><span>Suggested Icons</span></h3>
<ul className="anticons-list clearfix icons">
<li className=""><QuestionOutlined /><span className="anticon-class"><span className="ant-badge">question</span></span></li>
<li className=""><QuestionCircleOutlined /><span className="anticon-class"><span className="ant-badge">question-circle-o</span></span></li>
<li className=""><QuestionCircleOutlined /><span className="anticon-class"><span className="ant-badge">question-circle</span></span></li>
<li className=""><PlusOutlined /><span className="anticon-class"><span className="ant-badge">plus</span></span></li>
<li className=""><PlusCircleOutlined /><span className="anticon-class"><span className="ant-badge">plus-circle-o</span></span></li>
<li className=""><PlusCircleOutlined /><span className="anticon-class"><span className="ant-badge">plus-circle</span></span></li>
<li className=""><PauseOutlined /><span className="anticon-class"><span className="ant-badge">pause</span></span></li>
<li className=""><PauseCircleOutlined /><span className="anticon-class"><span className="ant-badge">pause-circle-o</span></span></li>
<li className=""><PauseCircleOutlined /><span className="anticon-class"><span className="ant-badge">pause-circle</span></span></li>
<li className=""><MinusOutlined /><span className="anticon-class"><span className="ant-badge">minus</span></span></li>
<li className=""><MinusCircleOutlined /><span className="anticon-class"><span className="ant-badge">minus-circle-o</span></span></li>
<li className=""><MinusCircleOutlined /><span className="anticon-class"><span className="ant-badge">minus-circle</span></span></li>
<li className=""><PlusSquareOutlined /><span className="anticon-class"><span className="ant-badge">plus-square</span></span></li>
<li className=""><PlusSquareOutlined /><span className="anticon-class"><span className="ant-badge">plus-square-o</span></span></li>
<li className=""><MinusSquareOutlined /><span className="anticon-class"><span className="ant-badge">minus-square</span></span></li>
<li className=""><MinusSquareOutlined /><span className="anticon-class"><span className="ant-badge">minus-square-o</span></span></li>
<li className=""><InfoOutlined /><span className="anticon-class"><span className="ant-badge">info</span></span></li>
<li className=""><InfoCircleOutlined /><span className="anticon-class"><span className="ant-badge">info-circle-o</span></span></li>
<li className=""><InfoCircleOutlined /><span className="anticon-class"><span className="ant-badge">info-circle</span></span></li>
<li className=""><ExclamationOutlined /><span className="anticon-class"><span className="ant-badge">exclamation</span></span></li>
<li className=""><ExclamationCircleOutlined /><span className="anticon-class"><span className="ant-badge">exclamation-circle-o</span></span></li>
<li className=""><ExclamationCircleOutlined /><span className="anticon-class"><span className="ant-badge">exclamation-circle</span></span></li>
<li className=""><CloseOutlined /><span className="anticon-class"><span className="ant-badge">close</span></span></li>
<li className=""><CloseCircleOutlined /><span className="anticon-class"><span className="ant-badge">close-circle</span></span></li>
<li className=""><CloseCircleOutlined /><span className="anticon-class"><span className="ant-badge">close-circle-o</span></span></li>
<li className=""><CloseSquareOutlined /><span className="anticon-class"><span className="ant-badge">close-square</span></span></li>
<li className=""><CloseSquareOutlined /><span className="anticon-class"><span className="ant-badge">close-square-o</span></span></li>
<li className=""><CheckOutlined /><span className="anticon-class"><span className="ant-badge">check</span></span></li>
<li className=""><CheckCircleOutlined /><span className="anticon-class"><span className="ant-badge">check-circle</span></span></li>
<li className=""><CheckCircleOutlined /><span className="anticon-class"><span className="ant-badge">check-circle-o</span></span></li>
<li className=""><CheckSquareOutlined /><span className="anticon-class"><span className="ant-badge">check-square</span></span></li>
<li className=""><CheckSquareOutlined /><span className="anticon-class"><span className="ant-badge">check-square-o</span></span></li>
<li className=""><ClockCircleOutlined /><span className="anticon-class"><span className="ant-badge">clock-circle-o</span></span></li>
<li className=""><ClockCircleOutlined /><span className="anticon-class"><span className="ant-badge">clock-circle</span></span></li>
<li className=""><WarningOutlined /><span className="anticon-class"><span className="ant-badge">warning</span></span></li>
</ul>

<h3 id="Application-Icons"><span>Application Icons</span></h3>
<ul className="anticons-list clearfix icons">
<li className=""><LockOutlined /><span className="anticon-class"><span className="ant-badge">lock</span></span></li>
<li className=""><UnlockOutlined /><span className="anticon-class"><span className="ant-badge">unlock</span></span></li>
<li className=""><AreaChartOutlined /><span className="anticon-class"><span className="ant-badge">area-chart</span></span></li>
<li className=""><PieChartOutlined /><span className="anticon-class"><span className="ant-badge">pie-chart</span></span></li>
<li className=""><BarChartOutlined /><span className="anticon-class"><span className="ant-badge">bar-chart</span></span></li>
<li className=""><DotChartOutlined /><span className="anticon-class"><span className="ant-badge">dot-chart</span></span></li>
<li className=""><BarsOutlined /><span className="anticon-class"><span className="ant-badge">bars</span></span></li>
<li className=""><BookOutlined /><span className="anticon-class"><span className="ant-badge">book</span></span></li>
<li className=""><CalendarOutlined /><span className="anticon-class"><span className="ant-badge">calendar</span></span></li>
<li className=""><CloudOutlined /><span className="anticon-class"><span className="ant-badge">cloud</span></span></li>
<li className=""><CloudDownloadOutlined /><span className="anticon-class"><span className="ant-badge">cloud-download</span></span></li>
<li className=""><CodeOutlined /><span className="anticon-class"><span className="ant-badge">code</span></span></li>
<li className=""><CodeOutlined /><span className="anticon-class"><span className="ant-badge">code-o</span></span></li>
<li className=""><CopyOutlined /><span className="anticon-class"><span className="ant-badge">copy</span></span></li>
<li className=""><CreditCardOutlined /><span className="anticon-class"><span className="ant-badge">credit-card</span></span></li>
<li className=""><DeleteOutlined /><span className="anticon-class"><span className="ant-badge">delete</span></span></li>
<li className=""><DesktopOutlined /><span className="anticon-class"><span className="ant-badge">desktop</span></span></li>
<li className=""><DownloadOutlined /><span className="anticon-class"><span className="ant-badge">download</span></span></li>
<li className=""><EditOutlined /><span className="anticon-class"><span className="ant-badge">edit</span></span></li>
<li className=""><EllipsisOutlined /><span className="anticon-class"><span className="ant-badge">ellipsis</span></span></li>
<li className=""><FileOutlined /><span className="anticon-class"><span className="ant-badge">file</span></span></li>
<li className=""><FileTextOutlined /><span className="anticon-class"><span className="ant-badge">file-text</span></span></li>
<li className=""><FileUnknownOutlined /><span className="anticon-class"><span className="ant-badge">file-unknown</span></span></li>
<li className=""><FilePdfOutlined /><span className="anticon-class"><span className="ant-badge">file-pdf</span></span></li>
<li className=""><FileWordOutlined /><span className="anticon-class"><span className="ant-badge">file-word</span></span></li>
<li className=""><FileExcelOutlined /><span className="anticon-class"><span className="ant-badge">file-excel</span></span></li>
<li className=""><FileJpgOutlined /><span className="anticon-class"><span className="ant-badge">file-jpg</span></span></li>
<li className=""><FilePptOutlined /><span className="anticon-class"><span className="ant-badge">file-ppt</span></span></li>
<li className=""><FileMarkdownOutlined /><span className="anticon-class"><span className="ant-badge">file-markdown</span></span></li>
<li className=""><FileAddOutlined /><span className="anticon-class"><span className="ant-badge">file-add</span></span></li>
<li className=""><FolderOutlined /><span className="anticon-class"><span className="ant-badge">folder</span></span></li>
<li className=""><FolderOpenOutlined /><span className="anticon-class"><span className="ant-badge">folder-open</span></span></li>
<li className=""><FolderAddOutlined /><span className="anticon-class"><span className="ant-badge">folder-add</span></span></li>
<li className=""><HddOutlined /><span className="anticon-class"><span className="ant-badge">hdd</span></span></li>
<li className=""><FrownOutlined /><span className="anticon-class"><span className="ant-badge">frown</span></span></li>
<li className=""><FrownOutlined /><span className="anticon-class"><span className="ant-badge">frown-o</span></span></li>
<li className=""><MehOutlined /><span className="anticon-class"><span className="ant-badge">meh</span></span></li>
<li className=""><MehOutlined /><span className="anticon-class"><span className="ant-badge">meh-o</span></span></li>
<li className=""><SmileOutlined /><span className="anticon-class"><span className="ant-badge">smile</span></span></li>
<li className=""><SmileOutlined /><span className="anticon-class"><span className="ant-badge">smile-o</span></span></li>
<li className=""><InboxOutlined /><span className="anticon-class"><span className="ant-badge">inbox</span></span></li>
<li className=""><LaptopOutlined /><span className="anticon-class"><span className="ant-badge">laptop</span></span></li>
<li className=""><AppstoreOutlined /><span className="anticon-class"><span className="ant-badge">appstore-o</span></span></li>
<li className=""><AppstoreOutlined /><span className="anticon-class"><span className="ant-badge">appstore</span></span></li>
<li className=""><LineChartOutlined /><span className="anticon-class"><span className="ant-badge">line-chart</span></span></li>
<li className=""><LinkOutlined /><span className="anticon-class"><span className="ant-badge">link</span></span></li>
<li className=""><MailOutlined /><span className="anticon-class"><span className="ant-badge">mail</span></span></li>
<li className=""><MobileOutlined /><span className="anticon-class"><span className="ant-badge">mobile</span></span></li>
<li className=""><NotificationOutlined /><span className="anticon-class"><span className="ant-badge">notification</span></span></li>
<li className=""><PaperClipOutlined /><span className="anticon-class"><span className="ant-badge">paper-clip</span></span></li>
<li className=""><PictureOutlined /><span className="anticon-class"><span className="ant-badge">picture</span></span></li>
<li className=""><PoweroffOutlined /><span className="anticon-class"><span className="ant-badge">poweroff</span></span></li>
<li className=""><ReloadOutlined /><span className="anticon-class"><span className="ant-badge">reload</span></span></li>
<li className=""><SearchOutlined /><span className="anticon-class"><span className="ant-badge">search</span></span></li>
<li className=""><SettingOutlined /><span className="anticon-class"><span className="ant-badge">setting</span></span></li>
<li className=""><ShareAltOutlined /><span className="anticon-class"><span className="ant-badge">share-alt</span></span></li>
<li className=""><ShoppingCartOutlined /><span className="anticon-class"><span className="ant-badge">shopping-cart</span></span></li>
<li className=""><TabletOutlined /><span className="anticon-class"><span className="ant-badge">tablet</span></span></li>
<li className=""><TagOutlined /><span className="anticon-class"><span className="ant-badge">tag</span></span></li>
<li className=""><TagOutlined /><span className="anticon-class"><span className="ant-badge">tag-o</span></span></li>
<li className=""><TagsOutlined /><span className="anticon-class"><span className="ant-badge">tags</span></span></li>
<li className=""><TagsOutlined /><span className="anticon-class"><span className="ant-badge">tags-o</span></span></li>
<li className=""><ToTopOutlined /><span className="anticon-class"><span className="ant-badge">to-top</span></span></li>
<li className=""><UploadOutlined /><span className="anticon-class"><span className="ant-badge">upload</span></span></li>
<li className=""><UserOutlined /><span className="anticon-class"><span className="ant-badge">user</span></span></li>
<li className=""><VideoCameraOutlined /><span className="anticon-class"><span className="ant-badge">video-camera</span></span></li>
<li className=""><HomeOutlined /><span className="anticon-class"><span className="ant-badge">home</span></span></li>
<li className=""><LegacyIcon type="spin anticon-loading" /><span className="anticon-class"><span className="ant-badge">loading</span></span></li>
<li className=""><Loading3QuartersOutlined /><span className="anticon-class"><span className="ant-badge">loading-3-quarters</span></span></li>
<li className=""><CloudUploadOutlined /><span className="anticon-class"><span className="ant-badge">cloud-upload-o</span></span></li>
<li className=""><CloudDownloadOutlined /><span className="anticon-class"><span className="ant-badge">cloud-download-o</span></span></li>
<li className=""><CloudUploadOutlined /><span className="anticon-class"><span className="ant-badge">cloud-upload</span></span></li>
<li className=""><CloudOutlined /><span className="anticon-class"><span className="ant-badge">cloud-o</span></span></li>
<li className=""><StarOutlined /><span className="anticon-class"><span className="ant-badge">star-o</span></span></li>
<li className=""><StarOutlined /><span className="anticon-class"><span className="ant-badge">star</span></span></li>
<li className=""><HeartOutlined /><span className="anticon-class"><span className="ant-badge">heart-o</span></span></li>
<li className=""><HeartOutlined /><span className="anticon-class"><span className="ant-badge">heart</span></span></li>
<li className=""><EnvironmentOutlined /><span className="anticon-class"><span className="ant-badge">environment</span></span></li>
<li className=""><EnvironmentOutlined /><span className="anticon-class"><span className="ant-badge">environment-o</span></span></li>
<li className=""><EyeOutlined /><span className="anticon-class"><span className="ant-badge">eye</span></span></li>
<li className=""><EyeOutlined /><span className="anticon-class"><span className="ant-badge">eye-o</span></span></li>
<li className=""><CameraOutlined /><span className="anticon-class"><span className="ant-badge">camera</span></span></li>
<li className=""><CameraOutlined /><span className="anticon-class"><span className="ant-badge">camera-o</span></span></li>
<li className=""><SaveOutlined /><span className="anticon-class"><span className="ant-badge">save</span></span></li>
<li className=""><TeamOutlined /><span className="anticon-class"><span className="ant-badge">team</span></span></li>
<li className=""><SolutionOutlined /><span className="anticon-class"><span className="ant-badge">solution</span></span></li>
<li className=""><PhoneOutlined /><span className="anticon-class"><span className="ant-badge">phone</span></span></li>
<li className=""><FilterOutlined /><span className="anticon-class"><span className="ant-badge">filter</span></span></li>
<li className=""><ExceptionOutlined /><span className="anticon-class"><span className="ant-badge">exception</span></span></li>
<li className=""><ExportOutlined /><span className="anticon-class"><span className="ant-badge">export</span></span></li>
<li className=""><CustomerServiceOutlined /><span className="anticon-class"><span className="ant-badge">customer-service</span></span></li>
<li className=""><QrcodeOutlined /><span className="anticon-class"><span className="ant-badge">qrcode</span></span></li>
<li className=""><ScanOutlined /><span className="anticon-class"><span className="ant-badge">scan</span></span></li>
<li className=""><LikeOutlined /><span className="anticon-class"><span className="ant-badge">like</span></span></li>
<li className=""><LikeOutlined /><span className="anticon-class"><span className="ant-badge">like-o</span></span></li>
<li className=""><DislikeOutlined /><span className="anticon-class"><span className="ant-badge">dislike</span></span></li>
<li className=""><DislikeOutlined /><span className="anticon-class"><span className="ant-badge">dislike-o</span></span></li>
<li className=""><MessageOutlined /><span className="anticon-class"><span className="ant-badge">message</span></span></li>
<li className=""><PayCircleOutlined /><span className="anticon-class"><span className="ant-badge">pay-circle</span></span></li>
<li className=""><PayCircleOutlined /><span className="anticon-class"><span className="ant-badge">pay-circle-o</span></span></li>
<li className=""><CalculatorOutlined /><span className="anticon-class"><span className="ant-badge">calculator</span></span></li>
<li className=""><PushpinOutlined /><span className="anticon-class"><span className="ant-badge">pushpin</span></span></li>
<li className=""><PushpinOutlined /><span className="anticon-class"><span className="ant-badge">pushpin-o</span></span></li>
<li className=""><BulbOutlined /><span className="anticon-class"><span className="ant-badge">bulb</span></span></li>
<li className=""><SelectOutlined /><span className="anticon-class"><span className="ant-badge">select</span></span></li>
<li className=""><SwitcherOutlined /><span className="anticon-class"><span className="ant-badge">switcher</span></span></li>
<li className=""><RocketOutlined /><span className="anticon-class"><span className="ant-badge">rocket</span></span></li>
<li className=""><BellOutlined /><span className="anticon-class"><span className="ant-badge">bell</span></span></li>
<li className=""><DisconnectOutlined /><span className="anticon-class"><span className="ant-badge">disconnect</span></span></li>
<li className=""><DatabaseOutlined /><span className="anticon-class"><span className="ant-badge">database</span></span></li>
<li className=""><CompassOutlined /><span className="anticon-class"><span className="ant-badge">compass</span></span></li>
<li className=""><BarcodeOutlined /><span className="anticon-class"><span className="ant-badge">barcode</span></span></li>
<li className=""><HourglassOutlined /><span className="anticon-class"><span className="ant-badge">hourglass</span></span></li>
<li className=""><KeyOutlined /><span className="anticon-class"><span className="ant-badge">key</span></span></li>
<li className=""><FlagOutlined /><span className="anticon-class"><span className="ant-badge">flag</span></span></li>
<li className=""><LayoutOutlined /><span className="anticon-class"><span className="ant-badge">layout</span></span></li>
<li className=""><PrinterOutlined /><span className="anticon-class"><span className="ant-badge">printer</span></span></li>
<li className=""><SoundOutlined /><span className="anticon-class"><span className="ant-badge">sound</span></span></li>
<li className=""><UsbOutlined /><span className="anticon-class"><span className="ant-badge">usb</span></span></li>
<li className=""><SkinOutlined /><span className="anticon-class"><span className="ant-badge">skin</span></span></li>
<li className=""><ToolOutlined /><span className="anticon-class"><span className="ant-badge">tool</span></span></li>
<li className=""><SyncOutlined /><span className="anticon-class"><span className="ant-badge">sync</span></span></li>
<li className=""><WifiOutlined /><span className="anticon-class"><span className="ant-badge">wifi</span></span></li>
<li className=""><CarOutlined /><span className="anticon-class"><span className="ant-badge">car</span></span></li>
<li className=""><ScheduleOutlined /><span className="anticon-class"><span className="ant-badge">schedule</span></span></li>
<li className=""><UserAddOutlined /><span className="anticon-class"><span className="ant-badge">user-add</span></span></li>
<li className=""><UserDeleteOutlined /><span className="anticon-class"><span className="ant-badge">user-delete</span></span></li>
<li className=""><UsergroupAddOutlined /><span className="anticon-class"><span className="ant-badge">usergroup-add</span></span></li>
<li className=""><UsergroupDeleteOutlined /><span className="anticon-class"><span className="ant-badge">usergroup-delete</span></span></li>
<li className=""><ManOutlined /><span className="anticon-class"><span className="ant-badge">man</span></span></li>
<li className=""><WomanOutlined /><span className="anticon-class"><span className="ant-badge">woman</span></span></li>
<li className=""><ShopOutlined /><span className="anticon-class"><span className="ant-badge">shop</span></span></li>
<li className=""><GiftOutlined /><span className="anticon-class"><span className="ant-badge">gift</span></span></li>
<li className=""><IdcardOutlined /><span className="anticon-class"><span className="ant-badge">idcard</span></span></li>
<li className=""><MedicineBoxOutlined /><span className="anticon-class"><span className="ant-badge">medicine-box</span></span></li>
<li className=""><RedEnvelopeOutlined /><span className="anticon-class"><span className="ant-badge">red-envelope</span></span></li>
<li className=""><CoffeeOutlined /><span className="anticon-class"><span className="ant-badge">coffee</span></span></li>
<li className=""><CopyrightOutlined /><span className="anticon-class"><span className="ant-badge">copyright</span></span></li>
<li className=""><TrademarkOutlined /><span className="anticon-class"><span className="ant-badge">trademark</span></span></li>
<li className=""><SafetyOutlined /><span className="anticon-class"><span className="ant-badge">safety</span></span></li>
<li className=""><WalletOutlined /><span className="anticon-class"><span className="ant-badge">wallet</span></span></li>
<li className=""><BankOutlined /><span className="anticon-class"><span className="ant-badge">bank</span></span></li>
<li className=""><TrophyOutlined /><span className="anticon-class"><span className="ant-badge">trophy</span></span></li>
<li className=""><ContactsOutlined /><span className="anticon-class"><span className="ant-badge">contacts</span></span></li>
<li className=""><GlobalOutlined /><span className="anticon-class"><span className="ant-badge">global</span></span></li>
<li className=""><ShakeOutlined /><span className="anticon-class"><span className="ant-badge">shake</span></span></li>
<li className=""><ApiOutlined /><span className="anticon-class"><span className="ant-badge">api</span></span></li>
<li className=""><ForkOutlined /><span className="anticon-class"><span className="ant-badge">fork</span></span></li>
<li className=""><DashboardOutlined /><span className="anticon-class"><span className="ant-badge">dashboard</span></span></li>
<li className=""><FormOutlined /><span className="anticon-class"><span className="ant-badge">form</span></span></li>
<li className=""><TableOutlined /><span className="anticon-class"><span className="ant-badge">table</span></span></li>
<li className=""><ProfileOutlined /><span className="anticon-class"><span className="ant-badge">profile</span></span></li>
</ul>

<h3 id="Brand-and-Logos"><span>Brand and Logos</span></h3>
<ul className="anticons-list clearfix icons">
<li className=""><AndroidOutlined /><span className="anticon-class"><span className="ant-badge">android</span></span></li>
<li className=""><AndroidOutlined /><span className="anticon-class"><span className="ant-badge">android-o</span></span></li>
<li className=""><AppleOutlined /><span className="anticon-class"><span className="ant-badge">apple</span></span></li>
<li className=""><AppleOutlined /><span className="anticon-class"><span className="ant-badge">apple-o</span></span></li>
<li className=""><WindowsOutlined /><span className="anticon-class"><span className="ant-badge">windows</span></span></li>
<li className=""><WindowsOutlined /><span className="anticon-class"><span className="ant-badge">windows-o</span></span></li>
<li className=""><IeOutlined /><span className="anticon-class"><span className="ant-badge">ie</span></span></li>
<li className=""><ChromeOutlined /><span className="anticon-class"><span className="ant-badge">chrome</span></span></li>
<li className=""><GithubOutlined /><span className="anticon-class"><span className="ant-badge">github</span></span></li>
<li className=""><AliwangwangOutlined /><span className="anticon-class"><span className="ant-badge">aliwangwang</span></span></li>
<li className=""><AliwangwangOutlined /><span className="anticon-class"><span className="ant-badge">aliwangwang-o</span></span></li>
<li className=""><DingdingOutlined /><span className="anticon-class"><span className="ant-badge">dingding</span></span></li>
<li className=""><DingdingOutlined /><span className="anticon-class"><span className="ant-badge">dingding-o</span></span></li>
<li className=""><WeiboSquareOutlined /><span className="anticon-class"><span className="ant-badge">weibo-square</span></span></li>
<li className=""><WeiboCircleOutlined /><span className="anticon-class"><span className="ant-badge">weibo-circle</span></span></li>
<li className=""><TaobaoCircleOutlined /><span className="anticon-class"><span className="ant-badge">taobao-circle</span></span></li>
<li className=""><Html5Outlined /><span className="anticon-class"><span className="ant-badge">html5</span></span></li>
<li className=""><WeiboOutlined /><span className="anticon-class"><span className="ant-badge">weibo</span></span></li>
<li className=""><TwitterOutlined /><span className="anticon-class"><span className="ant-badge">twitter</span></span></li>
<li className=""><WechatOutlined /><span className="anticon-class"><span className="ant-badge">wechat</span></span></li>
<li className=""><YoutubeOutlined /><span className="anticon-class"><span className="ant-badge">youtube</span></span></li>
<li className=""><AlipayCircleOutlined /><span className="anticon-class"><span className="ant-badge">alipay-circle</span></span></li>
<li className=""><TaobaoOutlined /><span className="anticon-class"><span className="ant-badge">taobao</span></span></li>
<li className=""><SkypeOutlined /><span className="anticon-class"><span className="ant-badge">skype</span></span></li>
<li className=""><QqOutlined /><span className="anticon-class"><span className="ant-badge">qq</span></span></li>
<li className=""><MediumWorkmarkOutlined /><span className="anticon-class"><span className="ant-badge">medium-workmark</span></span></li>
<li className=""><GitlabOutlined /><span className="anticon-class"><span className="ant-badge">gitlab</span></span></li>
<li className=""><MediumOutlined /><span className="anticon-class"><span className="ant-badge">medium</span></span></li>
<li className=""><LinkedinOutlined /><span className="anticon-class"><span className="ant-badge">linkedin</span></span></li>
<li className=""><GooglePlusOutlined /><span className="anticon-class"><span className="ant-badge">google-plus</span></span></li>
<li className=""><DropboxOutlined /><span className="anticon-class"><span className="ant-badge">dropbox</span></span></li>
<li className=""><FacebookFilled /><span className="anticon-class"><span className="ant-badge">facebook</span></span></li>
<li className=""><CodepenOutlined /><span className="anticon-class"><span className="ant-badge">codepen</span></span></li>
<li className=""><AmazonOutlined /><span className="anticon-class"><span className="ant-badge">amazon</span></span></li>
<li className=""><GoogleOutlined /><span className="anticon-class"><span className="ant-badge">google</span></span></li>
<li className=""><CodepenCircleOutlined /><span className="anticon-class"><span className="ant-badge">codepen-circle</span></span></li>
<li className=""><AlipayOutlined /><span className="anticon-class"><span className="ant-badge">alipay</span></span></li>
<li className=""><AntDesignOutlined /><span className="anticon-class"><span className="ant-badge">ant-design</span></span></li>
<li className=""><AliyunOutlined /><span className="anticon-class"><span className="ant-badge">aliyun</span></span></li>
<li className=""><ZhihuOutlined /><span className="anticon-class"><span className="ant-badge">zhihu</span></span></li>
<li className=""><SlackOutlined /><span className="anticon-class"><span className="ant-badge">slack</span></span></li>
<li className=""><SlackSquareOutlined /><span className="anticon-class"><span className="ant-badge">slack-square</span></span></li>
<li className=""><BehanceOutlined /><span className="anticon-class"><span className="ant-badge">behance</span></span></li>
<li className=""><BehanceSquareOutlined /><span className="anticon-class"><span className="ant-badge">behance-square</span></span></li>
<li className=""><DribbbleOutlined /><span className="anticon-class"><span className="ant-badge">dribbble</span></span></li>
<li className=""><DribbbleSquareOutlined /><span className="anticon-class"><span className="ant-badge">dribbble-square</span></span></li>
<li className=""><InstagramOutlined /><span className="anticon-class"><span className="ant-badge">instagram</span></span></li>
<li className=""><YuqueOutlined /><span className="anticon-class"><span className="ant-badge">yuque</span></span></li>
</ul>

    </section>
  );
}

export default Section;
