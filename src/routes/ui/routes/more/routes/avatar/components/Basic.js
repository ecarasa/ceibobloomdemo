import React from 'react';
import { UserOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';

const Box = () => {
  return (
    <div className="box box-default">
      <div className="box-header">Basic</div>
      <div className="box-body">
        <div>
          <Avatar size="large" icon={<UserOutlined />} />
          <Avatar icon={<UserOutlined />} />
          <Avatar size="small" icon={<UserOutlined />} />
        </div>
        <div>
          <Avatar shape="square" size="large" icon={<UserOutlined />} />
          <Avatar shape="square" icon={<UserOutlined />} />
          <Avatar shape="square" size="small" icon={<UserOutlined />} />
        </div>
      </div>
    </div>
  );
}

export default Box;