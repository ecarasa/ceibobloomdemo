import React from 'react';
import { LaptopOutlined } from '@ant-design/icons';

const Section = () => (
  <article className="article">
    <h2 className="article-title">Icon Styles and Colors</h2>
    <ul className="list-inline">
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-primary"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--outlined border-primary"><LaptopOutlined className="bg-primary" /> </div></li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--bordered border-primary"><LaptopOutlined className="text-primary" /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--rounded bg-primary"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon--plain"><LaptopOutlined className="text-primary" /></div> </li>
      <li className="list-inline-item"> <div className="icon--plain icon--sm"><LaptopOutlined className="text-primary" /></div> </li>
    </ul>
    
    <ul className="list-inline">
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-dark"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--outlined border-dark"><LaptopOutlined className="bg-dark" /> </div></li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--bordered border-dark"><LaptopOutlined className="text-dark" /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--rounded bg-dark"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon--plain"><LaptopOutlined className="text-dark" /></div> </li>
      <li className="list-inline-item"> <div className="icon--plain icon--sm"><LaptopOutlined className="text-dark" /></div> </li>
    </ul>    
    
    <ul className="list-inline">
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-primary"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-info"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-success"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-warning"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-danger"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-dark"><LaptopOutlined /></div> </li>
      <li className="list-inline-item"> <div className="icon-card__icon icon--circle bg-secondary"><LaptopOutlined /></div> </li>
    </ul>      
    
  </article>
);

export default Section;
