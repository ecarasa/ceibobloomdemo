import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Screens
import ViewCreditLimitScreen from 'screens/productor/credit/limit';

class ViewCreditLimit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {

        const { role } = this.props.match.params;

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1" className="container-fluid chapter">
                    <article className="article">
                        <h2 className="article-title">Límite de crédito</h2>
                        <ViewCreditLimitScreen />
                    </article>
                </div>
            </QueueAnim>
        );
    }
}

export default ViewCreditLimit;