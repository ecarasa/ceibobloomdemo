import React from 'react';
import { Route } from 'react-router-dom';

import ViewCreditLimit from './routes/ViewCreditLimit';
import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Route path={`${match.url}/limit`} component={ViewCreditLimit} />
    </div>
);

export default Page;
