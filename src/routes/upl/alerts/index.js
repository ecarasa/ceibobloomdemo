import React from 'react';
import { Route } from 'react-router-dom';
import PrivateRoute from 'routes/upl/PrivateRoute';

import ListAlerts from 'screens/alerts';
import ManagedAlerts from 'screens/distributor/alerts/managed_alerts'
import './styles.scss';

const Page = ({ match }) => (
    <div>
        <PrivateRoute path={`${match.url}/managed`} exact component={ManagedAlerts} />
        <PrivateRoute path={`${match.url}/`} exact component={ListAlerts} />
    </div>
);

export default Page;
