import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PrivateRoute from 'routes/upl/PrivateRoute';

// Screens
import ListContent from 'screens/productor/content_geo/content_list';
import ListContentCreatedByMe from 'screens/distributor/content_geo/my_content_geo';
import CreateOrEditContent from 'screens/distributor/content_geo/create_content_geo';
// import ListContentById from 'screens/productor/content_geo';

import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Switch>
            <PrivateRoute path={`${match.url}/list/me`} component={ListContentCreatedByMe} />
            <PrivateRoute path={`${match.url}/list`} component={ListContent} />
            <PrivateRoute path={`${match.url}/edit/:id`} exact component={CreateOrEditContent} />
            <PrivateRoute path={`${match.url}/new`} exact component={CreateOrEditContent} />
        </Switch>
    </div>
);

export default Page;
