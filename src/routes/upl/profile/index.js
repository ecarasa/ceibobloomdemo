import React from 'react';
import { Route } from 'react-router-dom';
import withContext from 'context/withContext'
import PrivateRoute from 'routes/upl/PrivateRoute';

// Screens
import EditProfileProductorScreen from 'screens/productor/profile/edit';
import EditProfileDistributorScreen from 'screens/distributor/profile/edit';
import './styles.scss';

const Page = (props) => {
    let EditProfileComponent = null;
    let user = props.stores.user.getUser();

    if(user.role_slug == 'distributor') {
        EditProfileComponent = EditProfileDistributorScreen;
    } else if(user.role_slug == 'productor') {
        EditProfileComponent = EditProfileProductorScreen;
    }

    return (
        <div>
            <PrivateRoute path={`${props.match.url}/edit`} component={EditProfileComponent} />
        </div>
    );
};

export default withContext(Page);