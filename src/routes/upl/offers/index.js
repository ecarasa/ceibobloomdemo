import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PrivateRoute from 'routes/upl/PrivateRoute';

import MarketplaceOffers from 'screens/productor/marketplace/offers_list';

import CreateMarketplaceOffer from 'screens/distributor/marketplace/create_offer';
import ListMyOffers from 'screens/distributor/marketplace/my_offers';

import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Switch>
            <PrivateRoute path={`${match.url}/list/me`} component={ListMyOffers} />
            <PrivateRoute path={`${match.url}/list`} component={MarketplaceOffers} />
            <PrivateRoute path={`${match.url}/:id/edit`} component={CreateMarketplaceOffer} />
            <PrivateRoute path={`${match.url}/new`} exact component={CreateMarketplaceOffer} />
        </Switch>
    </div>
);

export default Page;
