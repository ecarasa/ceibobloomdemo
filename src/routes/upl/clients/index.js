import React from 'react';
import { Route, Switch } from 'react-router-dom';
import withContext from 'context/withContext';
import PrivateRoute from 'routes/upl/PrivateRoute';

import ListClients from 'screens/distributor/clients/list';
import './styles.scss';

const Page = (props) => {
    let user = props.stores.user.getUser()

    return (
        <div>
            <Switch>
                <PrivateRoute path={`${props.match.url}/list`} exact component={ListClients} />
            </Switch>
        </div>
    );
};

export default withContext(Page);