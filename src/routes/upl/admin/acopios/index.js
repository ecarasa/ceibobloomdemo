import React from 'react';
import { Route } from 'react-router-dom';

import AdminListAcopios from 'screens/admin/acopios/AdminListAcopios';
import AdminCreateAcopio from 'screens/admin/acopios/AdminCreateAcopio';
import AdminEditAcopio from 'screens/admin/acopios/AdminEditAcopio';
import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Route path={`${match.url}/list`} component={AdminListAcopios} />
        <Route path={`${match.url}/new`} component={AdminCreateAcopio} />
        <Route path={`${match.url}/:id/edit`} component={AdminEditAcopio} />
    </div>
);

export default Page;