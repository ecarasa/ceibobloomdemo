import React from 'react';
import { Route } from 'react-router-dom';

import AdminListTemplatesAutomatedAlerts from 'screens/admin/automated_alerts/templates';
import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Route path={`${match.url}/templates`} component={AdminListTemplatesAutomatedAlerts} />
    </div>
);

export default Page;