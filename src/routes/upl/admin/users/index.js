import React from 'react';
import { Route } from 'react-router-dom';

import AdminListUsers from 'screens/admin/users/AdminListUsers';
import AdminCreateUser from 'screens/admin/users/AdminCreateUser';
import AdminEditUser from 'screens/admin/users/AdminEditUser';
import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Route path={`${match.url}/list`} component={AdminListUsers} />
        <Route path={`${match.url}/new`} component={AdminCreateUser} />
        <Route path={`${match.url}/:id/edit`} component={AdminEditUser} />
    </div>
);

export default Page;