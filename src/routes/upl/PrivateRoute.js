// This is used to determine if a user is authenticated and
// if they are allowed to visit the page they navigated to.

// If they are: they proceed to the page
// If not: they are redirected to the login page.
import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import withContext from 'context/withContext';

const PrivateRoute = ({ stores, component: Component, ...rest}) => {
    // Add your own authentication on the below line.
    const isLoggedIn = stores.user && stores.user.user && stores.user.user.id > 0;

    return (
        <Route
            {...rest}
            render={props => {
                return isLoggedIn ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/auth/login/distributor',
                            state: { from: props.location },
                        }}
                    />
                )
            }}
        />
    );
};

export default withContext(PrivateRoute);