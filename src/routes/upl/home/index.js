import React from 'react';
import { Route, Switch } from 'react-router-dom';
import withContext from 'context/withContext';
import PrivateRoute from 'routes/upl/PrivateRoute';

import HomeProductorScreen from 'screens/productor/home';
import HomeDistributorScreen from 'screens/distributor/home';
import GrafoComponent from 'screens/distributor/home/grafo';
import './styles.scss';

const Page = (props) => {
    let user = props.stores.user.getUser()
    let HomeComponent = HomeProductorScreen;

    if(user.role_slug == 'distributor') {
        HomeComponent = HomeDistributorScreen;
    }

    return (
        <div>
            <Switch>
                <PrivateRoute path={`${props.match.url}/`} exact component={HomeComponent} />
                <PrivateRoute path={`${props.match.url}/grafo`} component={GrafoComponent} />
            </Switch>
        </div>
    );
};

export default withContext(Page);