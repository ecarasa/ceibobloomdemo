import React from 'react';
import { Route } from 'react-router-dom';

import Login from './routes/Login';
import SignUp from './routes/SignUp';
import ForgotPassword from 'screens/auth/ForgotPassword'
import ResetPassword from 'screens/auth/ResetPassword';
import './styles.scss';

const Page = ({ match }) => (
    <div>
        <Route path={`${match.url}/login/:role`} component={Login} />
        <Route path={`${match.url}/sign-up`} component={SignUp} />
        <Route path={`${match.url}/forgot-password`} component={ForgotPassword} />
        <Route path={`${match.url}/reset-password/:token`} component={ResetPassword} />
    </div>
);

export default Page;
