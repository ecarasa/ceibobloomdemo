import React from 'react';
import QueueAnim from 'rc-queue-anim';

// Screens
import LoginFormProductor from 'screens/auth/LoginFormProductor';
import LoginFormDistributor from 'screens/auth/LoginFormDistributor';
import LoginFormAdministrator from 'screens/auth/LoginFormAdministrator'

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {

        const { role } = this.props.match.params;
        let ComponentLogin = null;
        if (role == 'distributor') {
            ComponentLogin = <LoginFormDistributor />
        } else if (role == 'productor') {
            ComponentLogin = <LoginFormProductor />
        } else if (role == 'administrator') {
            ComponentLogin = <LoginFormAdministrator />
        }

        return (
            <QueueAnim type="bottom" className="ui-animate">
                <div key="1">
                    <section className="cover cover-page">
                        <div
                            className="cover-bg-img"
                            style={{backgroundColor: '#cecece'}}
                        ></div>
                        <div className="cover-form-container">
                            <div className="col-12 col-md-8 col-lg-6 col-xl-4"
                                style={{
                                    boxShadow: '1px 1px 1px 1px #00000069',
                                    border: '1px solid #80808000',
                                    padding: '50px',
                                    borderRadius: '10px',
                                    backgroundColor: 'white',
                                }}>
                                {ComponentLogin}
                            </div>
                        </div>
                    </section>
                </div>
            </QueueAnim>
        );
    }
}

export default Login;
