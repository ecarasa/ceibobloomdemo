import React from 'react';
import { Loading3QuartersOutlined, LoadingOutlined, SyncOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

const antIcon1 = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const antIcon2 = <Loading3QuartersOutlined style={{ fontSize: 24 }} spin />;
const antIcon3 = <SyncOutlined style={{ fontSize: 24 }} spin />;

const Box = () => {
  return(
    <div className="box box-default">
      <div className="box-header">Custom spinning indicator</div>
      <div className="box-body">
        <Spin indicator={antIcon1} />
        <span className="mx-3"></span>
        <Spin indicator={antIcon2} />
        <span className="mx-3"></span>
        <Spin indicator={antIcon3} />
      </div>
    </div>
  )
}

export default Box;