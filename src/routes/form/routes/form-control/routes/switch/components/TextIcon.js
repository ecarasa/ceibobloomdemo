import React from 'react';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { Switch } from 'antd';

// Customized

const Box = () => {
  return (
    <div className="box box-default">
      <div className="box-header">Text & icon</div>
      <div className="box-body">
        <Switch checkedChildren="On" unCheckedChildren="Off" defaultChecked />
        <br />
        <Switch checkedChildren="1" unCheckedChildren="0" />
        <br />
        <Switch checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} defaultChecked />
      </div>
    </div>
  );
}

export default Box;