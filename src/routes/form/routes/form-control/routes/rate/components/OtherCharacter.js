import React from 'react';
import { HeartOutlined } from '@ant-design/icons';
import { Rate } from 'antd';

const Box = () => {
  return (
    <div className="box box-default">
      <div className="box-header">Other Character</div>
      <div className="box-body">
        <Rate character={<HeartOutlined />} allowHalf />
        <br />
        <Rate character="A" allowHalf style={{ fontSize: 36 }} />
      </div>
    </div>
  );
}

export default Box;