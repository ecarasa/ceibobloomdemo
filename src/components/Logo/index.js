import React from 'react';

const Logo = (props) => {
    return (
        <img
            style={{ maxWidth: props.maxHeight || 60, maxHeight: props.maxHeight || 60 }}
            src={`${window.CONFIG.LOGO}`}
        />
    );
}

export default Logo;
