import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import ScrollToTop from 'components/ScrollToTop';
import App from './App';
import UPLContext from 'context/UPLContext'

export default class Root extends Component {
    render() {
        const { store, history } = this.props;
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <ScrollToTop>
                        <UPLContext history={history}>
                            <Route path="/" component={App} />
                        </UPLContext>
                    </ScrollToTop>
                </ConnectedRouter>
            </Provider>
        );
    }
}

Root.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};
