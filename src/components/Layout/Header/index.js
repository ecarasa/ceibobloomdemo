import React from 'react';
import { observer } from 'mobx-react';
import { connect } from 'react-redux';

import moment from 'moment';
import _ from 'lodash';
import classnames from 'classnames';
import DEMO from 'constants/demoData';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import * as Services from 'services/'

import {
    BellOutlined,
    InfoCircleOutlined,
    LogoutOutlined,
    SettingOutlined,
    ShopOutlined,
    UserOutlined,
    ShoppingCartOutlined,
    ArrowRightOutlined,
    DeleteOutlined
} from '@ant-design/icons';

import { Layout, Menu, Dropdown, Avatar, Badge, Tooltip, Popover, Divider, List, message } from 'antd';
import Logo from 'components/Logo';
import {
    toggleCollapsedNav,
    toggleOffCanvasMobileNav,
} from 'actions/settingsActions';
import Notifications from 'routes/layout/routes/header/components/Notifications';
import withContext from 'context/withContext';
import './styles.scss';
const { Header } = Layout;

@observer
class AppHeader extends React.Component {
    onToggleCollapsedNav = () => {
        const { handleToggleCollapsedNav, collapsedNav } = this.props;
        handleToggleCollapsedNav(!collapsedNav);
    };

    onToggleOffCanvasMobileNav = () => {
        const {
            handleToggleOffCanvasMobileNav,
            offCanvasMobileNav,
        } = this.props;
        handleToggleOffCanvasMobileNav(!offCanvasMobileNav);
    };

    logout = () => {
        const currentRole = _.defaultTo(this.props.stores.user.user.role_slug, 'productor');
        // Remove token and everything from auth
        Services.Base.RemoveToken();
        this.props.stores.user.logout();
        let goToURL = '/auth/login/productor';
        if(currentRole == 'distributor')
            goToURL = '/auth/login/distributor'
        this.props.history.push(goToURL);
    }

    reloadCart = () => {
        Services.Cart.getMyCart((data) => {
            if(data.success)
                this.props.stores.cart.setCart(data.cart);
        }, (err) => {});
    }

    render() {
        const {
            collapsedNav,
            offCanvasMobileNav,
            colorOption,
            showLogo,
        } = this.props;

        const user = this.props.stores.user.getUser();
        const cart = this.props.stores.cart.getCart();

        return (
            <Header className="app-header">
                <div
                    className={classnames('app-header-inner', {
                        'bg-white':
                            ['11', '12', '13', '14', '15', '16', '21'].indexOf(
                                colorOption
                            ) >= 0,
                        'bg-dark': colorOption === '31',
                        'bg-primary': ['22', '32'].indexOf(colorOption) >= 0,
                        'bg-success': ['23', '33'].indexOf(colorOption) >= 0,
                        'bg-info': ['24', '34'].indexOf(colorOption) >= 0,
                        'bg-warning': ['25', '35'].indexOf(colorOption) >= 0,
                        'bg-danger': ['26', '36'].indexOf(colorOption) >= 0,
                    })}>
                    <div className="header-left">
                        <div className="list-unstyled list-inline">
                            {showLogo && [
                                <Logo
                                    key="logo"
                                    maxHeight={collapsedNav ? 20 : 100}
                                />,
                                <Divider type="vertical" key="line" />,
                            ]}
                            <a
                                href={DEMO.link}
                                className="list-inline-item d-none d-md-inline-block"
                                onClick={this.onToggleCollapsedNav}>
                                <LegacyIcon
                                    type={
                                        collapsedNav
                                            ? 'menu-unfold'
                                            : 'menu-fold'
                                    }
                                    className="list-icon"
                                />
                            </a>
                            <a
                                href={DEMO.link}
                                className="list-inline-item d-md-none"
                                onClick={this.onToggleOffCanvasMobileNav}>
                                <LegacyIcon
                                    type={
                                        offCanvasMobileNav
                                            ? 'menu-unfold'
                                            : 'menu-fold'
                                    }
                                    className="list-icon"
                                />
                            </a>
                            <Tooltip placement="bottom" title="Inicio">
                                <a
                                    href="#/dashboard/home"
                                    className="list-inline-item d-md-inline-block">
                                    <ShopOutlined className="list-icon" />
                                </a>
                            </Tooltip>
                        </div>
                    </div>

                    <div className="header-right">
                        <div className="list-unstyled list-inline">
                            { cart && cart.products && cart.products.length > 0 &&
                                <Popover
                                    placement="bottomRight"
                                    content={
                                        <div className="app-header-notifications">
                                            <List
                                                footer={
                                                    <a
                                                        onClick={() => {
                                                            Services.Cart.checkoutCart(cart.id, (data) => {
                                                                if(data.success)
                                                                    message.success('Se envió una notificación a los distribuidores. Te contactarán a tu número de whatsapp.');
                                                                this.reloadCart();
                                                            }, (err) => {
                                                                // Do nothing
                                                            });
                                                        }}
                                                        className="no-link-style">
                                                        Checkout <ArrowRightOutlined />
                                                    </a>
                                                }
                                                itemLayout="horizontal"
                                                dataSource={cart.products}
                                                renderItem={item => (
                                                    <List.Item>
                                                        <div className="list-style-v1">
                                                            <div className="list-item">
                                                                <div className="list-item__body">
                                                                    <div className="list-item__title">{item.name}</div>
                                                                    <div className="list-item__datetime">
                                                                        Agregado {moment(item.created_at).fromNow()}
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    onClick={() => {
                                                                        Services.Cart.removeFromCart(cart.id, item.id, (data) => {
                                                                            if(data.success) {
                                                                                message.success('El producto fue eliminado del carrito.');
                                                                                this.reloadCart()
                                                                            }
                                                                        }, (err) => {
                                                                            // Do nothing
                                                                        })
                                                                    }}
                                                                    class="icon-btn icon-btn-round mr-3 bg-danger text-body-reverse" style={{ marginLeft: 15 }}>
                                                                    <DeleteOutlined />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </List.Item>
                                                )}
                                            />
                                        </div>
                                    }
                                    trigger="click"
                                    overlayClassName="app-header-popover">
                                    <a
                                        href={'/#/dashboard/alerts'}
                                        className="list-inline-item">
                                        <Badge count={cart.products.length}>
                                            <ShoppingCartOutlined className="list-notification-icon" />
                                        </Badge>
                                    </a>
                                </Popover>
                            }

                            <a
                                href={'/#/dashboard/alerts'}
                                className="list-inline-item">
                                <Badge count={user.unread_notifications}>
                                    <BellOutlined className="list-notification-icon" />
                                </Badge>
                            </a>

                            {/*<Popover
                                placement="bottomRight"
                                content={<Notifications />}
                                trigger="click"
                                overlayClassName="app-header-popover">
                                <a
                                    href={DEMO.link}
                                    className="list-inline-item">
                                    <Badge count={this.props.stores.notification.unreadNotifications}>
                                        <BellOutlined className="list-notification-icon" />
                                    </Badge>
                                </a>
                            </Popover>*/}
                            <Dropdown
                                className="list-inline-item"
                                overlay={() => {
                                    return (
                                        <Menu className="app-header-dropdown">
                                            <Menu.Item
                                                key="4"
                                                className="d-block d-md-none">
                                                <strong>{user.email}</strong>
                                            </Menu.Item>
                                            <Menu.Divider className="d-block d-md-none" />
                                            <Menu.Item key="1">
                                                <a href={'/#/dashboard/profile/edit'}>
                                                    <SettingOutlined />
                                                    Mi perfil
                                                </a>
                                            </Menu.Item>
                                            <Menu.Item key="0" disabled>
                                                {' '}
                                                <a href={DEMO.headerLink.about}>
                                                    <InfoCircleOutlined />
                                                    Ayuda
                                                </a>{' '}
                                            </Menu.Item>
                                            <Menu.Divider />
                                            <Menu.Item key="2">
                                                <a onClick={this.logout}>
                                                    <LogoutOutlined />
                                                    Salir de mi cuenta
                                                </a>
                                            </Menu.Item>
                                        </Menu>
                                    );
                                }}
                                trigger={['click']}
                                placement="bottomRight">
                                <a
                                    className="ant-dropdown-link no-link-style"
                                    href={DEMO.link}>
                                    <UserOutlined className="list-notification-icon" />
                                    {/*<Avatar
                                        src="assets/images-demo/avatars/4.jpg"
                                        size="small"
                                    />*/}
                                    <span className="avatar-text d-none d-md-inline">
                                        {user != null
                                            ? (user.profile && user.profile.name ? user.profile.name : user.email)
                                            : '-'}
                                    </span>
                                </a>
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </Header>
        );
    }
}

const mapStateToProps = state => ({
    offCanvasMobileNav: state.settings.offCanvasMobileNav,
    collapsedNav: state.settings.collapsedNav,
    colorOption: state.settings.colorOption,
});

const mapDispatchToProps = dispatch => ({
    handleToggleCollapsedNav: isCollapsedNav => {
        dispatch(toggleCollapsedNav(isCollapsedNav));
    },
    handleToggleOffCanvasMobileNav: isOffCanvasMobileNav => {
        dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withContext(AppHeader));