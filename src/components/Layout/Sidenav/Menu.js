import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {
    AlertOutlined,
    CreditCardOutlined,
    LineChartOutlined,
    LinkOutlined,
    MoneyCollectOutlined,
    ShopOutlined,
    DiffOutlined,
    ExclamationCircleOutlined
} from '@ant-design/icons';

import { Menu } from 'antd';
import APPCONFIG from 'constants/appConfig';
import DEMO from 'constants/demoData';
import { toggleOffCanvasMobileNav } from 'actions/settingsActions';
import {
    CARDS,
    LAYOUTS,
    UIKIT,
    FORMS,
    FEEDBACKS,
    TABELS,
    CHARTS,
    PAGES,
    ECOMMERCE,
    USER,
    EXCEPTION,

    // UPL
    SIDEBAR_PRODUCTOR_MENU,
    SIDEBAR_DISTRIBUTOR_MENU,
    SIDEBAR_ADMINISTRATOR_MENU
} from 'constants/uiComponents';
import withContext from 'context/withContext';
import { observer } from 'mobx-react';

const SubMenu = Menu.SubMenu;

@observer
class AppMenu extends React.Component {

    // list for AccordionNav
    rootMenuItemKeys = [
        // without submenu
        '/app/dashboard',
        '/app/ui-overview',
        '/app/calendar',
    ];
    rootSubmenuKeys = [
        '/app/card',
        '/app/layout',
        '/app/ui',
        '/app/form',
        '/app/feedback',
        '/app/table',
        '/app/chart',
        '/app/page',
        '/app/ecommerce',
        '/user',
        '/exception',
        '/app/menu',
    ];

    constructor(props) {
        super(props);

        this.state = {
            openKeys: ['/app/dashboard'],
        };
    }

    componentDidMount() {

    }

    onOpenChange = openKeys => {
        // AccordionNav
        // console.log(openKeys)
        const latestOpenKey = openKeys.find(
            key => this.state.openKeys.indexOf(key) === -1
        );
        if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            this.setState({ openKeys });
        } else {
            this.setState({
                openKeys: latestOpenKey ? [latestOpenKey] : [],
            });
        }
    };

    onMenuItemClick = item => {
        // AccordionNav
        const itemKey = item.key;
        if (this.rootMenuItemKeys.indexOf(itemKey) >= 0) {
            this.setState({ openKeys: [itemKey] });
        }

        //
        const { isMobileNav } = this.props;
        if (isMobileNav) {
            this.closeMobileSidenav();
        }
    };

    closeMobileSidenav = () => {
        if (APPCONFIG.AutoCloseMobileNav) {
            const { handleToggleOffCanvasMobileNav } = this.props;
            handleToggleOffCanvasMobileNav(true);
        }
    };

    //
    getSubMenuOrItem = item => {
        if (item.children && item.children.some(child => child.name)) {
            const childrenItems = this.getNavMenuItems(item.children);
            // hide submenu if there's no children items
            if (childrenItems && childrenItems.length > 0) {
                return (
                    <SubMenu title={item.name} key={item.path}>
                        {childrenItems}
                    </SubMenu>
                );
            }
            return null;
        } else {
            return (
                <Menu.Item key={item.path}>
                    <Link to={item.path}>
                        <span>{item.menuName || item.name}</span>
                    </Link>
                </Menu.Item>
            );
        }
    };

    getNavMenuItems = menusData => {
        if (!menusData) {
            return [];
        }
        return menusData
            .filter(item => !item.hideInMenu)
            .map(item => {
                // make dom
                const ItemDom = this.getSubMenuOrItem(item);
                return ItemDom;
            })
            .filter(item => item);
    };

    render() {
        const { collapsedNav, colorOption, location } = this.props;
        // const mode = collapsedNav ? 'vertical' : 'inline';
        const menuTheme =
            ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0
                ? 'light'
                : 'dark';
        const currentPathname = location.pathname;

        const menuProps = collapsedNav
            ? {}
            : {
                  openKeys: this.state.openKeys,
              };

        // Return the new menu
        const user = this.props.stores.user.getUser();

        return (
            <div>
                { user && user.role_slug == 'distributor' && this.renderMenuDistribuitor() }
                { user && user.role_slug == 'productor' && this.renderMenuProductor() }
                { user && user.role_slug == 'asesor_productores' && this.renderMenuAsesorProductores() }
                { user && user.role_slug == 'administrator' && this.renderMenuAdministrator() }
            </div>
        );
    }

    renderMenuDistribuitor = () => {

        const { collapsedNav, colorOption, location } = this.props;
        // const mode = collapsedNav ? 'vertical' : 'inline';
        const menuTheme =
            ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0
                ? 'light'
                : 'dark';
        const currentPathname = location.pathname;

        const menuProps = collapsedNav
            ? {}
            : {
                  openKeys: this.state.openKeys,
              };

        return (
            <Menu
                theme={menuTheme}
                mode="inline"
                // inlineCollapsed={collapsedNav}
                {...menuProps}
                onOpenChange={this.onOpenChange}
                onClick={this.onMenuItemClick}
                selectedKeys={[currentPathname]}>
                {/*<SubMenu
                    key="/app/grafos"
                    title={
                        <span>
                            <CreditCardOutlined />
                            <span className="nav-text">Grafos</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_DISTRIBUTOR_MENU.GRAFOS)}
                </SubMenu>*/}

                <Menu.Item key="/#/dashboard/home">
                    <a href="/#/dashboard/home"
                        rel="noopener noreferrer">
                        <ShopOutlined className="list-icon" />
                        <span className="nav-text">Inicio</span>
                    </a>
                </Menu.Item>

                <SubMenu
                    key="/app/alerts"
                    title={
                        <span>
                            <AlertOutlined />
                            <span className="nav-text">Alertas</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_DISTRIBUTOR_MENU.ALERTS)}
                </SubMenu>

                <SubMenu
                    key="/app/content"
                    title={
                        <span>
                            <LinkOutlined />
                            <span className="nav-text">Contenido Georeferenciado</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_DISTRIBUTOR_MENU.CONTENT_GEO)}
                </SubMenu>

                <SubMenu
                    key="/app/offers"
                    title={
                        <span>
                            <LineChartOutlined />
                            <span className="nav-text">Ofertas</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_DISTRIBUTOR_MENU.OFFERS)}
                </SubMenu>

                <Menu.Item key="/#/dashboard/benchmark">
                    <a href="/#/dashboard/benchmark"
                        rel="noopener noreferrer">
                        <DiffOutlined />
                        <span className="nav-text">Benchmark</span>
                    </a>
                </Menu.Item>
            </Menu>
        );
    }

    renderMenuAsesorProductores = () => {
        const { collapsedNav, colorOption, location } = this.props;
        // const mode = collapsedNav ? 'vertical' : 'inline';
        const menuTheme =
            ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0
                ? 'light'
                : 'dark';
        const currentPathname = location.pathname;

        const menuProps = collapsedNav
            ? {}
            : {
                  openKeys: this.state.openKeys,
              };

        return (
            <Menu
                theme={menuTheme}
                mode="inline"
                // inlineCollapsed={collapsedNav}
                {...menuProps}
                onOpenChange={this.onOpenChange}
                onClick={this.onMenuItemClick}
                selectedKeys={[currentPathname]}>

                <Menu.Item key="/#/dashboard/home">
                    <a href="/#/dashboard/home"
                        rel="noopener noreferrer">
                        <ShopOutlined className="list-icon" />
                        <span className="nav-text">Inicio</span>
                    </a>
                </Menu.Item>

                {/*<Menu.Item key="/#/dashboard/alerts">
                    <a href="/#/dashboard/alerts"
                        rel="noopener noreferrer">
                        <AlertOutlined />
                        <span className="nav-text">Alertas</span>
                    </a>
                </Menu.Item>

                <Menu.Item key="/#/dashboard/offers/list">
                    <a href="/#/dashboard/offers/list"
                        rel="noopener noreferrer">
                        <MoneyCollectOutlined />
                        <span className="nav-text">Ofertas</span>
                    </a>
                </Menu.Item>

                <Menu.Item key="/#/dashboard/content/list">
                    <a href="/#/dashboard/content/list"
                        rel="noopener noreferrer">
                        <LinkOutlined />
                        <span className="nav-text">Contenido Georeferenciado</span>
                    </a>
                </Menu.Item>*/}
            </Menu>
        );
    }

    renderMenuProductor = () => {
        const { collapsedNav, colorOption, location } = this.props;
        // const mode = collapsedNav ? 'vertical' : 'inline';
        const menuTheme =
            ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0
                ? 'light'
                : 'dark';
        const currentPathname = location.pathname;

        const menuProps = collapsedNav
            ? {}
            : {
                  openKeys: this.state.openKeys,
              };

        return (
            <Menu
                theme={menuTheme}
                mode="inline"
                // inlineCollapsed={collapsedNav}
                {...menuProps}
                onOpenChange={this.onOpenChange}
                onClick={this.onMenuItemClick}
                selectedKeys={[currentPathname]}>

                {/*<Menu.Item key="/#/dashboard/credit/limit">
                    <a href="/#/dashboard/credit/limit"
                        rel="noopener noreferrer">
                        <CreditCardOutlined />
                        <span className="nav-text">Límite de crédito</span>
                    </a>
                </Menu.Item>*/}

                <Menu.Item key="/#/dashboard/home">
                    <a href="/#/dashboard/home"
                        rel="noopener noreferrer">
                        <ShopOutlined className="list-icon" />
                        <span className="nav-text">Inicio</span>
                    </a>
                </Menu.Item>

                <Menu.Item key="/#/dashboard/alerts">
                    <a href="/#/dashboard/alerts"
                        rel="noopener noreferrer">
                        <AlertOutlined />
                        <span className="nav-text">Alertas</span>
                    </a>
                </Menu.Item>

                <Menu.Item key="/#/dashboard/offers/list">
                    <a href="/#/dashboard/offers/list"
                        rel="noopener noreferrer">
                        <MoneyCollectOutlined />
                        <span className="nav-text">Ofertas</span>
                    </a>
                </Menu.Item>

                <Menu.Item key="/#/dashboard/content/list">
                    <a href="/#/dashboard/content/list"
                        rel="noopener noreferrer">
                        <LinkOutlined />
                        <span className="nav-text">Contenido Georeferenciado</span>
                    </a>
                </Menu.Item>
            </Menu>
        );
    }

    renderMenuAdministrator = () => {
        const { collapsedNav, colorOption, location } = this.props;
        // const mode = collapsedNav ? 'vertical' : 'inline';
        const menuTheme =
            ['31', '32', '33', '34', '35', '36'].indexOf(colorOption) >= 0
                ? 'light'
                : 'dark';
        const currentPathname = location.pathname;

        const menuProps = collapsedNav
            ? {}
            : {
                  openKeys: this.state.openKeys,
              };

        return (
            <Menu
                theme={menuTheme}
                mode="inline"
                // inlineCollapsed={collapsedNav}
                {...menuProps}
                onOpenChange={this.onOpenChange}
                onClick={this.onMenuItemClick}
                selectedKeys={[currentPathname]}>
                <SubMenu
                    key="/app/grafos"
                    title={
                        <span>
                            <CreditCardOutlined />
                            <span className="nav-text">Usuarios</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_ADMINISTRATOR_MENU.USERS)}
                </SubMenu>

                <SubMenu
                    key="/app/alerts"
                    title={
                        <span>
                            <AlertOutlined />
                            <span className="nav-text">Acopios</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_ADMINISTRATOR_MENU.ACOPIOS)}
                </SubMenu>

                <SubMenu
                    key="/app/content"
                    title={
                        <span>
                            <LinkOutlined />
                            <span className="nav-text">Alertas</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_ADMINISTRATOR_MENU.ALERTS)}
                </SubMenu>

                <SubMenu
                    key="/app/audit"
                    title={
                        <span>
                            <ExclamationCircleOutlined />
                            <span className="nav-text">Logs</span>
                        </span>
                    }>
                    {this.getNavMenuItems(SIDEBAR_ADMINISTRATOR_MENU.LOGS)}
                </SubMenu>
            </Menu>
        );
    }
}

const mapStateToProps = state => {
    // console.log(state);
    return {
        collapsedNav: state.settings.collapsedNav,
        colorOption: state.settings.colorOption,
        location: state.routing.location,
    };
};

const mapDispatchToProps = dispatch => ({
    handleToggleOffCanvasMobileNav: isOffCanvasMobileNav => {
        dispatch(toggleOffCanvasMobileNav(isOffCanvasMobileNav));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(withContext(AppMenu));