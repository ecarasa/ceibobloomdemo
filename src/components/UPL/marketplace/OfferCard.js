import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {
    Button
} from 'antd';
import { ArrowRightOutlined, EyeOutlined } from '@ant-design/icons';

const OfferCard = (props) => {
    const [showProductors, setShowProductors] = React.useState(false);
    const {
        offer,
        index,
        onClick,
        footer
    } = props;

    return (
        <div
            key={index.toString()}
            className="flex-item mb-4 col-xl-4 col-md-12">
            <div className="item-card">
                { offer.image != null ?
                    <div style={{ height: 150, background: `url('${offer.image}')`, backgroundSize: 'cover', width: '100%', opacity: offer.enabled ? 1 : .1 }}></div>
                    :
                    <div style={{ height: 150, background: `url(${window.CONFIG.LOGO_TRANSPARENT_PLACEHOLDER})`, backgroundSize: 'cover', width: '100%', opacity: offer.enabled ? 1 : .1 }}></div>
                }
                <div className="card__body card-white">
                    <div className="card__title">
                        <span>
                            { offer.is_offer ? 'Oferta' : 'Producto' }
                        </span>
                        <a href={offer.link}>
                            {offer.name}
                        </a>
                    </div>
                    <div className="card__price">
                        {/* <span className="type--strikethrough">$699.99</span> */}
                        <span>${ offer.price.toFixed(2) }</span>
                    </div>

                    { offer && offer.__meta__ && offer.__meta__.targeted_to &&
                        <div className="card__meta" style={{ marginTop: 10 }}>
                            <span
                                onClick={() => {
                                    setShowProductors(!showProductors);
                                }}
                                style={{ opacity: 1, color: '#8e519a', fontWeight: 600, cursor: 'pointer' }}>
                                Dirigido a { offer.__meta__.targeted_to } productores <EyeOutlined style={{ marginLeft: 2 }} />
                            </span>

                            { showProductors && offer.targeted.map((e) => {
                                return (
                                    <p style={{ marginTop: 0, marginBottom: 0, fontSize: 12 }}>{ e.user.email }</p>
                                );
                            })}
                        </div>
                    }

                    { footer }
                    {/*<Button
                        type="button"
                        className="ant-btn ant-btn-primary"
                        loading={_.defaultTo(offer.isLoading, false)}
                        onClick={() => { onClick(index) }}
                        style={{ width: '100%', marginTop: 20, height: 40, borderRadius: 3 }}>
                        <span>Agregar al carrito</span>
                    </Button>*/}
                </div>
            </div>
        </div>
    );
}

OfferCard.propTypes = {
    offer: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired
};

OfferCard.defaultProps = {
    //
}

export default OfferCard;