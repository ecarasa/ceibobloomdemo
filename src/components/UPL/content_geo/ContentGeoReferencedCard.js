import React from 'react';
import { Tooltip } from 'antd';
import { ArrowRightOutlined, EyeOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'lodash';
import * as Services from 'services';

const ContentGeoReferencedCard = (props) => {
    const [showProductors, setShowProductors] = React.useState(false);
    const {
        content,
        footer
    } = props;

    if(!content || !content.id)
        return null;

    return (
        <article key={content.id.toString()} className="blog-card flex-item mb-4" style={{ width: '100%' }}>
            { content.image != null &&
                <div
                    onClick={() => {
                        if(content.link_to != null) {
                            Services.Analytics.trackAudit({ action : `Click en contenido`, screen: 'content_geo', payload: {} });
                            window.open(content.link_to, '_blank');
                        }
                    }}
                    style={{ height: 150, background: `url('${content.image}')`, backgroundSize: 'cover', width: '100%', opacity: content.enabled ? 1 : .1 }}></div>
                /*<a href={_.defaultTo(content.link, 'javascript:;')}>
                    <img src={content.image} alt="blog cover" />
                </a>*/
            }
            <div className="blog-card__body" style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
                <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
                    {/*<span className="blog-card__date">{moment(content.created_at).fromNow()}</span>*/}
                    <h4 className="blog-card__title" style={{ fontSize: 18 }}>{content.name}</h4>
                    <p className="blog-card__desc">{content.text}</p>
                    {/*<a
                        href={content.link}
                        className="link-animated-hover link-hover-v1 text-primary">
                        Leer más <ArrowRightOutlined />
                    </a>*/}

                    <div>
                        { content && content.__meta__ && content.__meta__.targeted_to &&
                            <div className="card__meta" style={{ marginTop: 10 }}>
                                <span
                                    onClick={() => {
                                        setShowProductors(!showProductors);
                                    }}
                                    style={{ opacity: 1, color: '#8e519a', fontWeight: 600, cursor: 'pointer' }}>
                                    Dirigido a { content.__meta__.targeted_to } productores <EyeOutlined style={{ marginLeft: 2 }} />
                                </span>

                                { showProductors && content.targeted.map((e) => {
                                    return (
                                        <p style={{ marginTop: 0, marginBottom: 0, fontSize: 12 }}>{ e.user.email }</p>
                                    );
                                })}
                            </div>
                        }
                    </div>

                    <div style={{ display: 'flex', flex: 0 }}>
                        { footer }
                    </div>
                </div>
            </div>
        </article>
    );
};

ContentGeoReferencedCard.propTypes = {
    content: PropTypes.object.isRequired
};

ContentGeoReferencedCard.defaultProps = {
    //
}

export default ContentGeoReferencedCard;