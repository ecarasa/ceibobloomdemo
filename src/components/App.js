import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import loadable from 'react-loadable';
import LoadingComponent from 'components/Loading';
import withContext from 'context/withContext'

// Servcies
import * as Services from 'services/'

// 3rd
import 'styles/antd.less';
import 'styles/bootstrap/bootstrap.scss';
// custom
import 'styles/layout.scss';
import 'styles/theme.scss';
import 'styles/ui.scss';
import 'styles/vendors.scss';

let AsyncAppLayout = loadable({
    loader: () => import('components/Layout/AppLayout/'),
    loading: LoadingComponent,
});

let AsyncException = loadable({
    loader: () => import('routes/exception/'),
    loading: LoadingComponent,
});

let AsyncAuth = loadable({
    loader: () => import('routes/auth/'),
    loading: LoadingComponent
});

let AsyncAccount = loadable({
    loader: () => import('routes/user/'),
    loading: LoadingComponent,
});

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        // Pull the profile every 20s
        this.getMe(true);
        setInterval(() => {
            this.getMe();
        }, 10000);
    }

    getMe = (initialRequest = false) => {
        if(initialRequest || !this.props.stores.user.isGuest()) {
            Services.User.getMe((data) => {
                if(data.success) {
                    this.props.stores.user.setUser(data.user);
                }
            });
        }
    }

    render() {
        const { match, location } = this.props;
        const isRoot = location.pathname === '/' ? true : false;
        if (isRoot) {
            return <Redirect to={'/auth/login/productor'} />;
        }

        return (
            <div id="app">
                <Route path={`${match.url}auth`} component={AsyncAuth} />
                <Route path={`${match.url}dashboard`} component={AsyncAppLayout} />
                <Route path={`${match.url}app`} component={AsyncAppLayout} />

                <Route
                    path={`${match.url}exception`}
                    component={AsyncException}
                />
                <Route path={`${match.url}user`} component={AsyncAccount} />
            </div>
        );
    }
}

export default withContext(App);