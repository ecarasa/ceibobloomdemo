export const formatPrice = (number) => {
    // Create our number formatter.
    var formatter = new Intl.NumberFormat('es-AR', {
        style: 'currency',
        currency: 'USD',
    });

    return formatter.format(number);
}

export const formatTn = (number) => {
    // Create our number formatter.
    var formatter = new Intl.NumberFormat('de-DE');
    return formatter.format(number);
}