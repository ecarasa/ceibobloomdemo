# Sistema AGD - Cliente

A continuación se detallarán los pasos para iniciar el servidor de pruebas y como deployarlo.

## Requerimientos - Infraestructura

- [Instalación de npm](https://www.npmjs.com/get-npm)
- [Instalación de nginx](https://www.digitalocean.com/community/tutorials/como-instalar-nginx-en-ubuntu-18-04-es)

## Instalación

Una vez clonado el proyecto de este repositorio, deberemos pasar a su configuración básica.

- Instalar las dependencias vía npm. Posicionarse sobre el root del proyecto actual, y ejecutar

```
npm install
```

Comprobar que todas las dependencias hayan sido instaladas correctamente.

- Crear un archivo .env sobre la base del proyecto, con el siguiente archivo de configuración.

```
NODE_PATH=src/
```

## Ejecución modo desarrollo

Una vez que configurado el proyecto, deberemos probar si funciona bien y es accesible vía web. Para ello, nos deberemos posicionar sobre esta carpeta (a través de la linea de comandos) y ejecutar:

```
npm run start
```

Demora aproximadamente 1 minuto en iniciar. Una vez iniciado, comprobar que el frontend está funcionando desde la URL:

```http://localhost:3000/#/auth/login/productor```

## Compilación y ejecución modo producción

El proyecto corre con una integración sobre webpack. Esto significa que los archivos generados son estáticos, y solamente necesitan ser apuntados por un servidor como nginx o apache.

Para compilar en modo producción se debe ejecutar el siguiente comando:

```npm run build```

Esto generará una carpeta build/ sobre la carpeta actual. Nginx o Apache debe apuntar a esta carpeta build. Para probar la integración, ingresar a la URL:

```**<URL>**/#/auth/login/productor```

## Compilación y ejecución modo producción

La definición del virtualhost de nginx es útil para enlazar ciertos directorios a ciertos dominios. Luego de apuntar el registro A a la IP del servidor correspondiente, una configuración standard de nginx para referenciar ese dominio a una carpeta puede ser el siguiente:

```
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        # root /var/www/html;
        root /var/projects/Ceibo-Pegasus/client/build;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }
}
```

Luego de hacer ```npm run build```, como comentamos anteriormente tendremos una carpeta llamada build sobre este directorio, que contendrá el fuente listo para ser interpretado por el navegador. La carpeta build está apuntada en el código de ejemplo a ```/var/projects/Ceibo-Pegasus/client/build```. Se deberá modificar esta ruta según la que corresponda.

## Parámetros configurables del sistema

El sistema tiene algunos parámetros de configuración, que podrán ser cambiados en cualquier momento. Para modificar estos parámetros de configuración, se debe editar el archivo ```build/env-config.js```. A continuación se detalla la lista de parámetros:

```FRONTEND_URL```: Este parámetro es la URL del frontend del sistema. En este caso de ejemplo es ```http://sye.ceibo.digital```

```API_URL```: Este parámetro es la URL de la API (o backend) funcionando. En este caso de ejemplo es ```http://sye.ceibo.digital:3333/api/v1/```

```NEOVIZ_URL```: Este parámetro es la configuración de Neoviz. En este caso de ejemplo es ```bolt://sye.ceibo.digital:7687```

```LOGO```: Este parámetro especifica el logo del sistema (en este caso es el de AGD, ubicado en http://sye.ceibo.digital/assets/logo-agd.png). Puede ser una URL externa.

```LOGO_TRANSPARENT_PLACEHOLDER```: Cuando los productos y/o ofertas no tienen una imagen asociada, se usa una como placeholder. Este parámetro especifica ese placeholder (en el ejemplo actual de AGD es http://sye.ceibo.digital/assets/logo-agd-products.png). Puede ser una URL externa.

## License
[MIT](https://choosealicense.com/licenses/mit/)